# symbionix-sample-api

> 

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/symbionix-sample-api; yarn
    ```

3. Initialize database and schema
    ```
    yarn db:init
    ```
    The app will start, but this is to create the initial schema based off the models
    `Ctrl + C` to exit and continue

4. Run migrations (if applicable)
    ```
    yarn db:migrate:latest
    ```

5. Seed new database
    ```
    yarn db:seed:all
    ```

6. Start your app

    ```
    yarn start
    ```

    or in watch mode (watch for code changes)
    ```
    yarn dev
    ```

## Testing

Simply run `yarn test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Docker & docker compose

There are some docker scripts provided for your convenence and to be able to replicate the production environment while testing. The scripts and a more detailed README can be found in the `/infrastructure` folder at the base of this repo.

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2018

Licensed under the [MIT license](LICENSE).
