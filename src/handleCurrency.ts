import Dinero from 'dinero.js';
import { VAT_PERCENT } from './variables';

Dinero.globalLocale = 'en-ZA';
Dinero.defaultCurrency = 'ZAR';

export const withVAT = (amountInCents: number) => {
	const priceWithVAT = Dinero({
		amount: Math.round(Number(amountInCents)),
		precision: 2,
	})
		.multiply(1 + Number(VAT_PERCENT))
		.toObject();

	return priceWithVAT.amount;
};

export const formatCurrency = (amount: number) => {
	return Dinero({
		amount: Math.round(Number(amount)),
	}).toFormat('$0,0.00');
};

export default Dinero;
