import { NotAuthenticated } from '@feathersjs/errors';
import { HookContext } from '@feathersjs/feathers';
import { hooks } from '@feathersjs/authentication';
import * as R from 'ramda';

const verifyIdentity = hooks.authenticate('jwt');

function hasToken(context: HookContext): string | boolean {
	if (R.path(['params', 'headers'], context) === undefined) return false;
	if (R.path(['data', 'accessToken'], context) === undefined) return false;
	if (context.params.headers == null) return false;
	return context.params.headers.authorization || context.data.accessToken;
}

export default async function authenticateUser(context: HookContext) {
	try {
		return await verifyIdentity(context);
	} catch (error) {
		if (error instanceof NotAuthenticated && !hasToken(context)) {
			return context;
		}

		throw error;
	}
}
