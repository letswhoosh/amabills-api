import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import * as Sentry from '@sentry/node';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';

import { IApplication } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import authentication from './authentication';
import sequelize from './sequelize';
// Don't remove this comment. It's needed to format import lines nicely.

const app: IApplication = express(feathers());

app.use(Sentry.Handlers.requestHandler());

// Load app configuration
app.configure(configuration());

if (process.env.NODE_ENV === 'production') {
	Sentry.init({
		dsn: app.get('sentry').url,
	});
}

if (process.env.DATABASE_URL) {
	app.set('postgres', process.env.DATABASE_URL);
}

if (process.env.WEBSOCKET_PATH) {
	app.set('websocketPath', process.env.WEBSOCKET_PATH);
}

if (process.env.UI_DOMAIN) {
	app.set('ui', process.env.UI_DOMAIN);
}

if (process.env.API_URL) {
	app.set('apiUrl', process.env.API_URL);
}

if (process.env.KINEKTEK_API_KEY) {
	app.set('kinektek-api', {
		...app.get('kinektek-api'),
		apikey: process.env.KINEKTEK_API_KEY,
	});
}

if (process.env.KINEKTEK_AUTH_SECRET) {
	app.set('kinektek-api', {
		...app.get('kinektek-api'),
		clientSecret: process.env.KINEKTEK_AUTH_SECRET,
	});
}

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(
	helmet.frameguard({
		action: 'sameorigin',
	}),
);
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));
app.use('/uploads', express.static(app.get('uploads')));

// START SERVE VIEWS

// set the view engine to ejs
app.set('view engine', 'ejs');

// Folder Location
const filePath = ['production', 'staging'].includes(process.env.NODE_ENV || '')
	? '../../'
	: '../';

app.use(
	'/styles',
	express.static(path.join(__dirname, `${filePath}views/styles`)),
);

// In order to load compiled react components
app.use(
	'/components.prod',
	express.static(path.join(__dirname, `${filePath}views/components.prod`)),
);

// END SERVE VIEWS

// Set up Plugins and providers
app.configure(express.rest());
app.configure(
	socketio({
		path: app.get('websocketPath'),
	}),
);

app.configure(sequelize);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

app.use(Sentry.Handlers.errorHandler());

// Configure a middleware for 404s and the error handler
app.use(express.notFound());

app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

export default app;
