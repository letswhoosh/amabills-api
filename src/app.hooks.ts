// Application hooks that run for every service
// Don't remove this comment. It's needed to format import lines nicely.
import { HookContext } from '@feathersjs/feathers';
import { when } from 'feathers-hooks-common';
import * as local from '@feathersjs/authentication-local';
import authenticateUser from './hooks/authenticate';
// import authorize from './hooks/abilities';
import {
	attachData,
	attachMeta,
	returnFullObject,
	disablePagination,
} from './services/utils';
import log from './hooks/log';

const { protect } = local.hooks;

const safePaths = ['/auth', '/uploads', '/authentication', '/cloudinary'];

const whenAuthRequired = (context: HookContext) =>
	context.params.provider != null && !safePaths.includes(`/${context.path}`);

export default {
	before: {
		all: [
			log(),
			// @ts-ignore
			when(whenAuthRequired, authenticateUser) as any,
		],
		find: [disablePagination],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [returnFullObject, log(), protect('password')],
		find: [attachMeta],
		get: [attachData],
		create: [attachData],
		update: [attachData],
		patch: [attachData],
		remove: [attachData],
	},

	error: {
		all: [log()],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
