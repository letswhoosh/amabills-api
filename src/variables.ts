export const MARGIN_PERCENT = 0.08;
export const VAT_PERCENT = 0.15;
export const SERVICE_FEE_PORTION = 0.4;
export const PROCESSING_FEE_PORTION = 0.6;
