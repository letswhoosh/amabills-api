import { Params, ServiceAddons } from '@feathersjs/feathers';
import {
	AuthenticationService,
	JWTStrategy,
	AuthenticationRequest,
} from '@feathersjs/authentication';
import { LocalStrategy } from '@feathersjs/authentication-local';
import {
	expressOauth,
	OAuthStrategy,
	OAuthProfile,
} from '@feathersjs/authentication-oauth';
import axios from 'axios';

import { IApplication } from './declarations';

declare module './declarations' {
	interface ServiceTypes {
		authentication: AuthenticationService & ServiceAddons<any>;
	}
}

class FacebookStrategy extends OAuthStrategy {
	async getProfile(authResult: AuthenticationRequest, _params: Params) {
		// This is the OAuth access token that can be used
		// for Facebook API requests as the Bearer token
		const accessToken = authResult.access_token;

		const { data } = await axios.get('https://graph.facebook.com/me', {
			headers: {
				authorization: `Bearer ${accessToken}`,
			},
			params: {
				fields: 'id,first_name,last_name,email',
			},
		});

		return data;
	}

	async getEntityQuery(profile: OAuthProfile, params: Params) {
		return { email: profile.email };
	}

	async getEntityData(profile: OAuthProfile, existing: any, params: Params) {
		// `profile` is the data returned by getProfile
		const baseData = await super.getEntityData(profile, existing, params);

		return {
			...baseData,
			firstName: profile.first_name,
			lastName: profile.last_name,
			email: profile.email,
			verified: true,
		};
	}
}

class GoogleStrategy extends OAuthStrategy {
	async getEntityQuery(profile: OAuthProfile, params: Params) {
		return { email: profile.email };
	}

	async getEntityData(profile: OAuthProfile, existing: any, params: Params) {
		// this will set 'googleId'
		const baseData = await super.getEntityData(profile, existing, params);
		// this will grab the picture and email address of the Google profile
		return {
			...baseData,
			firstName: profile.given_name,
			lastName: profile.family_name,
			email: profile.email,
			verified: profile.email_verified,
		};
	}
}

export default function (app: IApplication) {
	const authentication = new AuthenticationService(app);

	authentication.register('jwt', new JWTStrategy());
	authentication.register('local', new LocalStrategy());
	authentication.register('facebook', new FacebookStrategy());
	authentication.register('google', new GoogleStrategy());

	app.use('/authentication', authentication);
	app.configure(expressOauth());
}
