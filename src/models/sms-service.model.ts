// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { IApplication } from '../declarations';

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const smsService = sequelizeClient.define(
		'sms_service',
		{
			text: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: any) {
					options.raw = true;
				},
			},
		},
	);

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	(smsService as any).associate = function (models: any): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
	};

	return smsService;
}
