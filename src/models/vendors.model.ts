// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export interface IVendorWrite {
	name: string;
	contactNumber: string;
	email: string;
	address: string;
	coordinates: { type: 'Point'; coordinates: [number, number] };
	city: string;
	discountRateInPercent: number;
}

export interface IVendorRead extends Omit<IVendorWrite, 'coordinates'> {
	id: number;
	coordinates: { coordinates: [] };
	createdAt: Date;
	updatedAt: Date;
}

export type TVendorModel = Model<IVendorRead, IVendorWrite>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const vendors = sequelizeClient.define(
		'vendors',
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
			},

			contactNumber: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: {
					name: 'Duplicate error',
					msg: 'Email address is already in use',
				},
				validate: {
					isEmail: { msg: 'Invalid email address' },
				},
			},
			address: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			coordinates: {
				type: DataTypes.GEOMETRY('POINT'),
				allowNull: false,
			},
			city: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			discountRateInPercent: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
				defaultValue: 0,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TVendorModel>;

	vendors.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		vendors.hasMany(models.meters);
		vendors.hasMany(models.users);
	};

	return vendors;
}
