// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export interface IPermissionWrite {
	name: string;
	risk: number;
	role: string;
	permission: string;
}

export interface IPermissionRead extends IPermissionWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPermissionModel = Model<IPermissionRead, IPermissionWrite>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const permissions = sequelizeClient.define(
		'permissions',
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			permission: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			risk: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			role: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPermissionModel>;

	permissions.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		permissions.belongsToMany(models.users, {
			through: 'user_permissions',
		});
	};

	return permissions;
}
