// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../declarations';
import { TModelStatic } from './models.type.utils';
import { IRentPaymentWrite } from './rent/rent-payments.model';
import { IVasPurchaseWrite } from './vas-purchases.model';

type IScheduleFrequency = 'Daily' | 'Weekly' | 'Monthly';

interface IScheduleInstruction {
	entity: 'vas-purchases' | 'rent/rent-payments';
	scheduleData: IRentPaymentWrite | IVasPurchaseWrite;
}

interface IScheduleWrite {
	scheduleDate: Date;
	nextScheduleDate: Date;
	lastScheduleDate: Date;
	scheduleInstruction: IScheduleInstruction;
	frequency: IScheduleFrequency;
	amount: number;
	productName: string;
}

interface IScheduleRead extends IScheduleWrite {
	id: string;
	createdAt: Date;
	updatedAt: Date;
}

export type TScheduledModel = Model<IScheduleRead, IScheduleWrite>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const scheduled = sequelizeClient.define(
		'scheduled',
		{
			scheduleDate: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			lastScheduleDate: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			nextScheduleDate: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			amount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			frequency: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			productName: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			scheduleInstruction: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TScheduledModel>;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	(scheduled as any).associate = function (models: any): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
	};

	return scheduled;
}
