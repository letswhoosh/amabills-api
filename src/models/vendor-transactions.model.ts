// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type TTransactionTypes = 'credit' | 'debit';

export interface IVendorTransactionWrite {
	type?: TTransactionTypes;
	amountInCents?: number;
	purchaseId?: number;
	prevBalanceInCents?: number;
	currentBalanceInCents?: number;
	vendorId?: number;
}

export interface IVendorTransactionRead
	extends Required<IVendorTransactionWrite> {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TVendorTransactionModel = Model<
	IVendorTransactionRead,
	IVendorTransactionWrite
>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const vendorTransactions = sequelizeClient.define(
		'vendor_transactions',
		{
			type: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			amountInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			purchaseId: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			prevBalanceInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			currentBalanceInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TVendorTransactionModel>;

	vendorTransactions.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		vendorTransactions.belongsTo(models.vendors);
	};

	return vendorTransactions;
}
