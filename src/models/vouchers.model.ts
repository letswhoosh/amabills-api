// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type TVoucherState = 'available' | 'used';

export interface IVoucherWrite {
	voucherId: string;
	expires: Date;
	usedOn: Date;
	state: TVoucherState;
	valueInCents: number;
	issuedTo: number;
	issuedBy: number;
}

export interface IVoucherRead extends IVoucherWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TVoucherModel = Model<IVoucherRead, IVoucherWrite>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const vouchers = sequelizeClient.define(
		'vouchers',
		{
			voucherId: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			expires: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			usedOn: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			// available, used
			state: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: 'available',
			},
			valueInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TVoucherModel>;

	vouchers.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		vouchers.belongsTo(models.users, {
			as: 'issuedTo',
		});
		vouchers.belongsTo(models.users, {
			as: 'issuedBy',
		});
	};

	return vouchers;
}
