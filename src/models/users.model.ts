// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { BuildOptions, DataTypes, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';
import { IPermissionRead } from './permissions.model';

export interface IUserWrite {
	firstName: string;
	lastName: string;
	email: string;
	password: string;
	contactNumber: string;
	picture: string;
	paymentOptions: string[];
	verified: boolean;
	permissionIds: number[];
	vendorId: number;
	verificationCode?: string | null;
	resetToken: string | null;
}

export interface IUserRead extends IUserWrite {
	id: number;
	permissions: IPermissionRead[];
	createdAt: Date;
	updatedAt: Date;
}

export type TUserModel = Model<IUserRead, IUserWrite>;

export type TUserInstance = {
	setPermissions: (permissionIds: number[]) => void;
};

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const users = sequelizeClient.define(
		'users',
		{
			firstName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			lastName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: {
					name: 'Duplicate error',
					msg: 'Email address is already in use',
				},
				validate: {
					isEmail: { msg: 'Invalid email address' },
				},
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			contactNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			resetToken: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			picture: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			verificationCode: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			paymentOptions: {
				type: DataTypes.ARRAY(DataTypes.STRING),
				allowNull: true,
			},
			verified: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
				defaultValue: false,
			},
			facebookId: {
				type: DataTypes.STRING,
				allowNull: true,
				unique: {
					name: 'Duplicate error',
					msg: 'facebookId should be unique',
				},
			},
			googleId: {
				type: DataTypes.STRING,
				allowNull: true,
				unique: {
					name: 'Duplicate error',
					msg: 'googleId should be unique',
				},
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TUserModel>;

	users.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		users.belongsToMany(models.permissions, {
			through: 'user_permissions',
		});
		users.belongsTo(models.vendors);
	};

	return users;
}
