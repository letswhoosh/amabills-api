// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export interface IPreferenceWrite {
	electricityProductCode: string;
	meterNumber: string;
	productName: string;
	electricityProductTypeCode: string;
}

export interface IPreferenceRead extends IPreferenceWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPreferenceModel = Model<IPreferenceRead, IPreferenceWrite>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const preferences = sequelizeClient.define(
		'preferences',
		{
			electricityProductCode: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			meterNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			productName: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			electricityProductTypeCode: {
				type: DataTypes.STRING,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPreferenceModel>;

	preferences.associate = (models: IModels) => {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		preferences.belongsTo(models.users);
	};

	return preferences;
}
