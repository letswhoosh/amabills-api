// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

type PaymentStage =
	| 'lookup'
	| 'lookup-failed'
	| 'payment'
	| 'completed'
	| 'lookup-wait';

export interface TenderDetail {
	amount: number;
	type: string;
}

interface VasPurchaseRequest {
	data: {
		productID: string;
		amount: number;
		clientReference: string;
		tenderDetails: TenderDetail[];
		currency: string;
		slipWidth: number;
	};
	sessionID: string;
}

export interface ILookUpData {
	productID: string;
	reference: string;
}

export interface IVasPurchaseWrite {
	phoneNumber: string;
	email: string;
	amount: number;
	pointsAmount: number;
	voucherAmount: number;
	creditAmount: number;
	isComplete?: boolean;
	stage?: PaymentStage;
	transactionId?: string;
	productName: string;
	lookupRequest?: ILookUpData;
	lookupResponse?: Record<string, unknown>;
	purchaseRequest?: VasPurchaseRequest;
	purchaseResponse?: Record<string, unknown>;

	vasProductId?: number;
	userId: number;
}

export interface IVasPurchaseRead extends IVasPurchaseWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TVasPaymentModel = Model<IVasPurchaseRead, IVasPurchaseWrite>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const vasPurchases = sequelizeClient.define(
		'vas_purchases',
		{
			email: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			phoneNumber: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			amount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			pointsAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			voucherAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			creditAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			isComplete: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false,
			},
			stage: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: 'lookup',
			},
			transactionId: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			productName: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			lookupRequest: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
			lookupResponse: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
			purchaseRequest: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
			purchaseResponse: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TVasPaymentModel>;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	vasPurchases.associate = function (models: IModels) {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
		vasPurchases.belongsTo(models.users);
	};

	return vasPurchases;
}
