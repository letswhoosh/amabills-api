// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, BuildOptions, Model, DataTypes } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export interface IMeterWrite {
	serialNumber: string;
	municipality: string;
	description: string;
	vendorId: number;
}

export interface IMeterRead extends IMeterWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TMeterModel = Model<IMeterRead, IMeterWrite>;

export default function (app: IApplication) {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const meters = sequelizeClient.define(
		'meters',
		{
			serialNumber: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: {
					name: 'Duplicate error',
					msg: 'Serial Number is already in use',
				},
			},
			municipality: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TMeterModel>;

	meters.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		meters.belongsTo(models.vendors);
	};

	return meters;
}
