// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { BuildOptions, DataTypes, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { TModelStatic } from './models.type.utils';

export interface ISubscriberWrite {
	email: string;
}

export interface ISubscriberRead extends ISubscriberWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPermissionModel = Model<ISubscriberRead, ISubscriberWrite>;

export default (app: IApplication) => {
	const sequelizeClient = app.get('sequelizeClient');
	const subscribers = sequelizeClient.define(
		'subscribers',
		{
			email: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPermissionModel>;

	subscribers.associate = () => {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
	};

	return subscribers;
};
