// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../../declarations';
import { IModels, TModelStatic } from '../models.type.utils';

export interface IRepresentative {
	firstName: string;
	lastName: string;
	email: string;
	phoneNumber: string;
	sendNotification?: boolean;
}

export interface IPropertyWrite {
	name: string;
	phoneNumber: string;
	altPhoneNumber: string;
	email: string;
	addressLine1: string;
	addressLine2: string;
	city: string;
	zipCode: string;
	province: string;
	accountNumber: string;
	unitsData: Record<string, number>;
	imageUrl: string;
	description: string;
}

export interface IPropertyRead extends IPropertyWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPropertyModel = Model<IPropertyRead, IPropertyWrite>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const properties = sequelizeClient.define(
		'properties',
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			province: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			phoneNumber: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			altPhoneNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			description: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			city: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			zipCode: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			addressLine1: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			addressLine2: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			accountNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			unitsData: {
				type: DataTypes.JSONB,
				allowNull: false,
			},
			representatives: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPropertyModel>;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	properties.associate = function (models: IModels): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
		properties.hasMany(models.house_units);
		properties.hasMany(models.rent_payments);
		properties.belongsTo(models.users);
	};

	return properties;
}
