// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../../declarations';
import { IModels, TModelStatic } from '../models.type.utils';

type PaymentStage = 'payment' | 'completed';

export interface IRentPaymentWrite {
	phoneNumber: string;
	email: string;
	amount: number;
	account: string;
	pointsAmount: number;
	voucherAmount: number;
	creditAmount: number;
	isComplete: boolean;
	stage: PaymentStage;
	transactionId: string;
	houseUnitId?: number;
	propertyId?: number;

	userId: number;
}

export interface IRentPaymentRead extends IRentPaymentWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TRentPaymentModel = Model<IRentPaymentRead, IRentPaymentWrite>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const rentPayments = sequelizeClient.define(
		'rent_payments',
		{
			email: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			phoneNumber: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			amount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			account: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			pointsAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			voucherAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			creditAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			isComplete: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false,
			},
			stage: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: 'payment',
			},
			transactionId: {
				type: DataTypes.STRING,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TRentPaymentModel>;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	rentPayments.associate = function (models: IModels): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
		rentPayments.belongsTo(models.properties);
		rentPayments.belongsTo(models.house_units);
		rentPayments.belongsTo(models.users);
	};

	return rentPayments;
}
