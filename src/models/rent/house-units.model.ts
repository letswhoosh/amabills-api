// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../../declarations';
import { IModels, TModelStatic } from '../models.type.utils';

export interface IOccupant {
	firstName: string;
	lastName: string;
	email: string;
	phoneNumber: string;
	sendNotification?: boolean;
}

export interface IHouseUnitWrite {
	unitName: string;
	rentAmount: number;
	unitNumber: string;
	referenceNumber: string;
	occupants?: IOccupant[];
	type: string;
	description: string;
	vacant: boolean;
	propertyId: number;
	informationUpdated?: boolean;
}

export interface IHouseUnitRead extends IHouseUnitWrite {
	id: number;
	coordinates: { coordinates: [] };
	createdAt: Date;
	updatedAt: Date;
}

export type THouseUnitModel = Model<IHouseUnitRead, IHouseUnitWrite>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const houseUnits = sequelizeClient.define(
		'house_units',
		{
			unitName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			unitNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			referenceNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			type: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			rentAmount: {
				type: DataTypes.DECIMAL,
				allowNull: false,
			},
			vacant: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: true,
			},
			occupants: {
				type: DataTypes.JSONB,
				allowNull: true,
			},
			informationUpdated: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<THouseUnitModel>;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	houseUnits.associate = function (models: IModels): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
		houseUnits.belongsTo(models.properties);
		houseUnits.belongsTo(models.users);
		houseUnits.hasMany(models.rent_payments);
	};

	return houseUnits;
}
