// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type TPurchaseState = 'prevend' | 'vend' | 'paid';

export interface ISlipPrepaid {
	title: string;
	vatReg: string;
	merchantCode: string;
	orderNumber: string;
	rrn: string;
	meterNumber: string;
	alg: string;
	krn: string;
	sgc: string;
	ti: string;
	tt: string;
	address: string;
	tokenTitle: string;
	receiptNumber: string;
	fbeCred: {
		amount: number | string;
		units: number | string;
		token: string;
	};
	saleCred: {
		amount: number | string;
		units: number | string;
		token: string;
	};
	costs: {
		vat: number | string;
		vpsVat: number | string;
		processingFee: number | string;
		serviceFee: number | string;
		userPaid: number | string;
		total: number | string;
	};
	utility: string;
	printed: string;
	cashier: string;
}

export interface ISlipBill {
	title: string;
	vatReg: string;
	merchantCode: string;
	orderNumber: string;
	accountNumber: string;
	address: string;
	tokenTitle: string;
	receiptNumber: string;
	retailValue: string;
	responseDateTime: string;
	acknowledgmentType: string;
	resultCode: string;
	transactionStatus: string;
	municipalityAccountNumber: string;
	message: string;
	costs: {
		vat: number | string;
		paymentFees: string;
		merchantFee: number;
		amountPaid: string;
		total: number;
		transactionAmount: number;
	};
	utility: string;
	printed: string;
}

export interface IPurchaseWrite {
	transactionRef: string;
	productTypeId: string;
	productCode: string;
	productName: string;
	meterNumber?: string;
	accountNumber?: string;
	amountInCents: number;
	vatInCents?: number;
	vendResponse?: any;
	confirmResponse?: any;
	tokens?: any;
	serviceFeeInCents?: number;
	processingFeeInCents?: number;
	paid?: boolean;
	slip?: string;
	state?: TPurchaseState;
	slipVariables?: ISlipPrepaid | ISlipBill;
	contactNumber?: string;
	email?: string;
	walletBalancePaymentInCents?: number;
	creditCardPaymentInCents?: number;
	voucherPaymentInCents?: number;
	prevendResponse?: any;
	// Relationships
	userId?: number;
	voucherId: number;
}

export interface IPurchaseRead extends IPurchaseWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPurchaseModel = Model<IPurchaseRead, IPurchaseWrite>;

export default function (app: IApplication) {
	const sequelizeClient = app.get('sequelizeClient');
	const purchases = sequelizeClient.define(
		'purchases',
		{
			transactionRef: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			productTypeId: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			productCode: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			meterNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			accountNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			productName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			amountInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			vatInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					isEmail: { msg: 'Invalid email address' },
				},
			},
			contactNumber: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			serviceFeeInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			processingFeeInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			creditCardPaymentInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			voucherPaymentInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			walletBalancePaymentInCents: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			tokens: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			slipVariables: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			slip: {
				type: DataTypes.TEXT,
				allowNull: true,
			},
			prevendResponse: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			vendResponse: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			confirmResponse: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			paid: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false,
			},
			// prevend | vend | paid
			state: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: 'prevend',
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPurchaseModel>;

	purchases.associate = (models: IModels) => {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		purchases.belongsTo(models.users);
		purchases.belongsTo(models.vouchers);
	};

	return purchases;
}
