import { Schema } from 'jsonschema';

export const successActionSchema: Schema = {
	id: '/SuccessActionSchema',
	type: 'object',
	properties: {
		service: {
			type: ['string'],
			enum: ['complete-vas-purchase', 'complete-rent-payment'],
		},
		data: {
			type: 'object',
			required: ['transactionId', 'paid'],
		},
	},
	required: ['service', 'data'],
};

export interface SuccessAction {
	service: 'complete-vas-purchase' | 'complete-rent-payment';
	data: {
		transactionId: string;
		paid: boolean;
	};
}

export const successEntitySchema = {
	type: 'object',
	required: ['entity', 'id'],
	properties: {
		entity: {
			type: ['string'],
			enum: ['purchases', 'vendor-transactions'],
		},
		id: { type: ['string'] },
	},
};

export type TSuccessEntity = {
	entity: 'purchases' | 'vendor-transactions';
	id: string;
};

export type TPaymentComposition = {
	creditCardPaymentInCents: number;
	voucherPaymentInCents: number;
	walletBalancePaymentInCents: number;
};
