// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../../declarations';
import { IModels, TModelStatic } from '../models.type.utils';
import { SuccessAction, TPaymentComposition, TSuccessEntity } from './schemas';

export interface ICompletePayment {
	transactionId: string;
	paid: boolean;
}
export interface ICompletePaymentResponse extends ICompletePayment {
	purchased: boolean;
}

export enum IPaymentCurrency {
	ZAR = 'zar',
}

export enum IPaymentStatus {
	Pending = 'pending', // becomes relevant when dealing with debit-orders
	Processing = 'processing',
	Successful = 'successful',
	Failed = 'failed',
	Cancelled = 'cancelled',
}

export interface IPaymentWrite {
	userId: string;
	checkoutId: string;
	amountInCents: number;
	description: string;
	status: IPaymentStatus;
	currency: IPaymentCurrency;
	reference: string;
	rawResponse?: string;
	externalReference?: string;
	failureReason?: string;
	successAction?: SuccessAction;
	paymentComposition?: TPaymentComposition;
	successActionStatus?: IPaymentStatus;
}

export interface IPaymentRead extends IPaymentWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPaymentModel = Model<IPaymentRead, IPaymentWrite>;

export default (app: IApplication) => {
	const sequelizeClient = app.get('sequelizeClient');
	const payments = sequelizeClient.define(
		'payments',
		{
			checkoutId: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
			},
			amountInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			currency: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					isIn: {
						args: [['zar']],
						msg: `Not a valid currency`,
					},
				},
			},
			description: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			status: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			reference: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			failureReason: {
				type: DataTypes.STRING,
			},
			rawResponse: {
				type: DataTypes.JSONB,
			},
			externalReference: {
				type: DataTypes.STRING,
			},
			successAction: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			successActionStatus: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			paymentComposition: {
				type: DataTypes.JSON,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions) {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPaymentModel>;

	payments.associate = (models: IModels) => {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		payments.belongsTo(models.users);
	};

	return payments;
};
