// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { DataTypes, BuildOptions, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type SourceTypes = 'vps' | 'internal' | 'unclear';

export interface IPurchaseErrorWrite {
	source: SourceTypes;
	description: string;
	error: {
		name: string;
		message: string;
		stack?: string;
	};
	dataProvided: Record<string, any>;
	transactionRef: string;
}

export interface IPurchaseErrorRead extends IPurchaseErrorWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TPurchaseErrorModel = Model<
	IPurchaseErrorRead,
	IPurchaseErrorWrite
>;

export default function (app: IApplication): typeof Model {
	const sequelizeClient = app.get('sequelizeClient');
	const purchaseErrors = sequelizeClient.define(
		'purchase_errors',
		{
			// vps, internal or unclear
			source: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			description: {
				type: DataTypes.TEXT,
				allowNull: false,
			},
			error: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			dataProvided: {
				type: DataTypes.JSON,
				allowNull: true,
			},
			transactionRef: {
				type: DataTypes.STRING,
				allowNull: true,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions): any {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TPurchaseErrorModel>;

	purchaseErrors.associate = function (models: IModels) {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
	};

	return purchaseErrors;
}
