// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type PointTypeUsage = 'credit' | 'debit';

export interface ILoyaltyPointsWrite {
	type: PointTypeUsage;
	amountInCents: number;
	purchaseId?: number;
	prevBalanceInCents: number;
	currentBalanceInCents: number;
	userId?: number;
}

export interface ILoyaltyPointsRead extends ILoyaltyPointsWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TLoyaltyPointsModel = Model<
	ILoyaltyPointsRead,
	ILoyaltyPointsWrite
>;

export default (app: IApplication) => {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const loyaltyPoints = sequelizeClient.define(
		'loyalty_points',
		{
			type: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			amountInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			purchaseId: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			prevBalanceInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			currentBalanceInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: any): any {
					// eslint-disable-next-line no-param-reassign
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TLoyaltyPointsModel>;

	loyaltyPoints.associate = (models: IModels) => {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		loyaltyPoints.belongsTo(models.users);
	};

	return loyaltyPoints;
};
