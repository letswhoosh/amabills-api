// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { IApplication } from '../declarations';
import { IModels, TModelStatic } from './models.type.utils';

export type TPurchaseType = 'prepaid' | 'bills';

export interface IAdminTransactionLogsWrite {
	purchaseType: TPurchaseType;
	valueInCents: number;
	// Relationships
	userId: number;
	purchaseId: number;
}

export interface IAdminTransactionLogsRead extends IAdminTransactionLogsWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type TAdminTransactionLogs = Model<
	IAdminTransactionLogsRead,
	IAdminTransactionLogsWrite
>;

export const PURCHASE_TYPES = ['bills', 'prepaid'] as const;

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const adminTransactionLogs = sequelizeClient.define(
		'admin_transaction_logs',
		{
			valueInCents: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			// bills, prepaid
			purchaseType: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					isIn: {
						args: [[...PURCHASE_TYPES]],
						msg: `Type of purchase is not valid`,
					},
				},
			},
		},
		{
			hooks: {
				beforeCount(options: any): any {
					options.raw = true;
				},
			},
		},
	) as TModelStatic<TAdminTransactionLogs>;

	adminTransactionLogs.associate = function (models: IModels): void {
		// Define associations here
		// See http://docs.sequelizejs.com/en/latest/docs/associations/
		adminTransactionLogs.belongsTo(models.users);
		adminTransactionLogs.belongsTo(models.purchases);
	};

	return adminTransactionLogs;
}
