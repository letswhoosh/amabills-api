import { BuildOptions, Model, ModelCtor } from 'sequelize/types';
import { IPaymentRead } from './payments/model';
import { IPermissionRead } from './permissions.model';
import { ISubscriberRead } from './subscribers.model';
import { IUserRead } from './users.model';
import { ILoyaltyPointsRead } from './loyalty-points.model';
import { IMeterRead } from './meters.model';
import { IPurchaseErrorRead } from './purchase-errors.model';
import { IPurchaseRead } from './purchases.model';
import { IPreferenceRead } from './preferences.model';
import { IVendorTransactionRead } from './vendor-transactions.model';
import { IVendorRead } from './vendors.model';
import { IVoucherRead } from './vouchers.model';
import { IAdminTransactionLogsRead } from './admin-transaction-logs.model';
import { IPropertyRead } from './rent/properties.model';
import { IHouseUnitRead } from './rent/house-units.model';
import { IRentPaymentRead } from './rent/rent-payments.model';
import { IVasPurchaseRead } from './vas-purchases.model';

export type TMCon<T> = ModelCtor<Model<T>>;

export interface IModels {
	users: TMCon<IUserRead>;
	payments: TMCon<IPaymentRead>;
	permissions: TMCon<IPermissionRead>;
	subscribers: TMCon<ISubscriberRead>;
	loyalty_points: TMCon<ILoyaltyPointsRead>;
	meters: TMCon<IMeterRead>;
	purchase_errors: TMCon<IPurchaseErrorRead>;
	purchases: TMCon<IPurchaseRead>;
	preferences: TMCon<IPreferenceRead>;
	vendor_transactions: TMCon<IVendorTransactionRead>;
	vendors: TMCon<IVendorRead>;
	vouchers: TMCon<IVoucherRead>;
	adminTransactionLogs: TMCon<IAdminTransactionLogsRead>;
	properties: TMCon<IPropertyRead>;
	house_units: TMCon<IHouseUnitRead>;
	rent_payments: TMCon<IRentPaymentRead>;
	vas_purchases: TMCon<IVasPurchaseRead>;
}

export type TModelStatic<T> = typeof Model & {
	new (values?: Record<string, unknown>, options?: BuildOptions): T;
} & { associate: (models: IModels) => void };
