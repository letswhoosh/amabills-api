// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model, BuildOptions } from 'sequelize';
import { IApplication } from '../declarations';

export interface IProductWrite {
	productName: string;
	productId: string;
}

export interface IProductRead extends IProductWrite {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export default function (app: IApplication): typeof Model {
	const sequelizeClient: Sequelize = app.get('sequelizeClient');
	const products = sequelizeClient.define(
		'products',
		{
			productName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			productId: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		},
		{
			hooks: {
				beforeCount(options: BuildOptions): any {
					options.raw = true;
				},
			},
		},
	);

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	(products as any).associate = function (models: any): void {
		// Define associations here
		// See https://sequelize.org/master/manual/assocs.html
	};

	return products;
}
