/* eslint-disable func-names */
import { Sequelize } from 'sequelize';
import { IApplication } from './declarations';

export default function (app: IApplication) {
	const connectionString = app.get('postgres');

	const enableSSL = ['production', 'staging'].includes(
		process.env.NODE_ENV || 'default',
	);

	const sequelize = new Sequelize(connectionString, {
		dialect: 'postgres',
		dialectOptions: {
			ssl: enableSSL
				? {
						rejectUnauthorized: false,
				  }
				: false,
		},
		logging: false,
		define: {
			freezeTableName: true,
		},
	});
	const oldSetup = app.setup;

	app.set('sequelizeClient', sequelize);

	// @ts-ignore
	// eslint-disable-next-line no-param-reassign
	app.setup = async function (...args) {
		const result = oldSetup.apply(this, args);

		// Set up data relationships
		const { models } = sequelize;
		Object.keys(models).forEach((name) => {
			if ('associate' in models[name]) {
				(models[name] as any).associate(models);
			}
		});

		// Sync to the database
		await sequelize.sync();

		return result;
	};
}
