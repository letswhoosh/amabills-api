/* eslint-disable @typescript-eslint/no-empty-interface */
import { Application as ExpressFeathers } from '@feathersjs/express';
import {
	Service,
	HookContext as FeathersHookContext,
	ServiceAddons,
} from '@feathersjs/feathers';
import Sequelize from 'sequelize';
// eslint-disable-next-line import/no-extraneous-dependencies
import '@feathersjs/transport-commons';
import config from '@config/default.json';

import { ILoyaltyPointsRead } from './models/loyalty-points.model';
import { IMeterRead } from './models/meters.model';
import { ICompletePayment, ICompletePaymentResponse, IPaymentRead } from './models/payments/model';
import { IPermissionRead } from './models/permissions.model';
import { IPreferenceRead } from './models/preferences.model';
import { IPurchaseErrorRead } from './models/purchase-errors.model';
import { IPurchaseRead } from './models/purchases.model';
import { ISubscriberRead } from './models/subscribers.model';
import { IUserRead } from './models/users.model';
import { IVendorTransactionRead } from './models/vendor-transactions.model';
import { IVendorRead } from './models/vendors.model';
import { IVoucherRead } from './models/vouchers.model';
import { IAdminTransactionLogsRead } from './models/admin-transaction-logs.model';
import { PurchaseStatuResponse } from './services/kinektek-api/actions/getPurchaseStatus';
import {
	IKinekteProduct,
	IKinekteProductDetails,
} from './services/kinektek-api/actions/getProducts';
import { ILookUpResponse } from './services/kinektek-api/actions/lookUpProduct';
import { ConfirmPurchaseData } from './services/kinektek-api/actions/confirmPurchase';
import { IPurchaseRes } from './services/kinektek-api/actions/purchase';
import { IProductRead } from './models/products.model';
import { KinektekApi } from './services/kinektek-api/kinektek-api.class';
import Verify from './services/auth/actions/verify';
import { IPropertyRead } from './models/rent/properties.model';
import { IRentPaymentRead } from './models/rent/rent-payments.model';
import { IHouseUnitRead } from './models/rent/house-units.model';
import { IVasPurchaseRead } from './models/vas-purchases.model';
import { IProductDetails } from './services/vas-purchases/actions/getProducts';

// List of packages that don't have typings yet
declare module 'express-cassandra';
declare module 'feathers-cassandra';
declare module 'mongodb-core';
declare module 'mongodb-core';
declare module 'abstract-blob-store';

// A mapping of service names to types. Will be extended in service files.
export interface IEntityServices {
	//auth
	'auth/verify': Verify;
	'loyalty-points': Service<ILoyaltyPointsRead>;
	meters: Service<IMeterRead>;
	payments: Service<IPaymentRead>;
	permissions: Service<IPermissionRead>;
	preferences: Service<IPreferenceRead>;
	'purchase-errors': Service<IPurchaseErrorRead>;
	purchases: Service<IPurchaseRead>;
	subscribers: Service<ISubscriberRead>;
	users: Service<IUserRead>;
	'vendor-transactions': Service<IVendorTransactionRead>;
	vendors: Service<IVendorRead>;
	vouchers: Service<IVoucherRead>;
	'admin-transaction-logs': Service<IAdminTransactionLogsRead>;
	products: Service<IProductRead>;
	'rent/properties': Service<IPropertyRead>;
	'rent/rent-payments': Service<IRentPaymentRead>;
	'rent/house-units': Service<IHouseUnitRead>;
	'vas-purchases': Service<IVasPurchaseRead>;
	'vas-products': Service<Record<string, IProductDetails[]>>;

	//none entity
	'kinektek-api': KinektekApi & ServiceAddons<any>;
	'kinektek-api/products': Service<
		IKinekteProduct[] | IKinekteProduct | IKinekteProductDetails[]
	>;
	'kinektek-api/lookup': Service<ILookUpResponse>;
	'kinektek-api/session-purchase': Service<IPurchaseRes>;
	'kinektek-api/confirm-purchase': Service<ConfirmPurchaseData>;
	'kinektek-api/purchase-status': Service<PurchaseStatuResponse>;
	'complete-vas-purchase': Service<ICompletePaymentResponse>;
	'complete-rent-payment': Service<ICompletePaymentResponse>;
	'payments/process-card': Service<any>;
	'send-sms': Service<any>;
	uploads: ServiceAddons<{ file: any }>;
}

type TConfig = typeof config;

interface IModifiedConfig extends TConfig {
	sequelizeClient: Sequelize.Sequelize;
	'view engine': 'ejs';
}

export interface ServiceTypes extends IEntityServices {}

type TInitialApplication = ExpressFeathers<ServiceTypes>;

type TAppGet = <T extends keyof IModifiedConfig>(path: T) => IModifiedConfig[T];
type TAppSet = <T extends keyof IModifiedConfig>(
	path: T,
	config: IModifiedConfig[T],
) => TInitialApplication;

// The application instance type that will be used everywhere else
export interface IApplication extends Omit<TInitialApplication, 'get' | 'set'> {
	get: TAppGet;
	set: TAppSet;
}

export type TPaths = keyof ServiceTypes;
export type TEntityPaths = keyof IEntityServices;

export interface IAPIHookContext<T = any> extends FeathersHookContext<T> {
	app: IApplication;
	path: TPaths;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IHookContext<T = any> extends FeathersHookContext<T> {}
