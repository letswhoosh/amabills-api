import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '../../declarations';

export class VasPurchases extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
    super(options);
  }
}
