// Initializes the `vas-purchases` service on path `/vas-purchases`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { VasPurchases } from './vas-purchases.class';
import createModel from '../../models/vas-purchases.model';
import hooks from './vas-purchases.hooks';
import VasActions from './actions';

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/vas-products', new VasActions.GetProducts(app));
	app.use('/complete-vas-purchase', new VasActions.CompleteVasPayment(app));
	app.use('/vas-purchases', new VasPurchases(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vas-purchases');

	service.hooks(hooks);
}
