import { IApplication } from '@app/declarations';
import axios from 'axios';
import { getAuthToken } from '../vas-purchases.utils';

export interface IProductCatologue {
	name: string;
	subCatalogues: Array<IProductSubCatologue>;
}

export interface IProductSubCatologue {
	name: string;
	subCatalogues: Array<IProductCategory | IProduct>;
}

export interface IProductCategory {
	name: string;
	subCatalogues: Array<IProduct>;
}

export interface IProduct {
	name: string;
	products: Array<IProductDetails>;
}

export interface IProductDetails {
	productID: string;
	issuer: string;
	name: string;
	imageName: string;
	price: number;
	dualStep: boolean;
}

interface IGetProductQuery {
	productCatalogue: string;
	filters: string[];
}

class GetProducts {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async create(
		data: IGetProductQuery,
	): Promise<{ data: Record<string, IProductDetails[]> }> {
		const { hostUrl, apikey } = this.app.get('kinektek-api');
		const token = await getAuthToken();
		const response = await axios.get<IProductCatologue>(
			`${hostUrl}/catalogue/products/${data.productCatalogue}`,
			{
				headers: {
					Authorization: `Bearer ${token}`,
					'x-api-key': apikey,
				},
			},
		);
		let products: Record<string, IProductDetails[]> = {};
		let filteredData = response?.data?.subCatalogues;
		if (response.data?.subCatalogues) {
			for (let x = 0; x <= data.filters.length; x++) {
				if (x === data.filters.length) {
					//@ts-ignore
					filteredData.forEach((data: IProduct) => {
						products[data.name] = data.products;
					});
				} else {
					//@ts-ignore
					filteredData = filteredData.filter(
						(subCatalogue) => subCatalogue.name === data.filters[x],
					)[0]?.subCatalogues;
				}
			}
		}
		return { data: products };
	}
}

export default GetProducts;
