import { IApplication } from '@app/declarations';
import { IVasPurchaseRead } from '@app/models/vas-purchases.model';
import { BadRequest } from '@feathersjs/errors';
import { Paginated, Params } from '@feathersjs/feathers';
import { vasGetRequest, vasPostRequest } from '../vas-purchases.utils';

export interface VasPayment {
	transactionId: string;
	paid: boolean;
}

class CompleteVasPayment {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: VasPayment, params?: Params) {
		const response = await this.CompleteVasPayment(data);
		return response;
	}

	async CompleteVasPayment(data: VasPayment) {
		try {
			if (data.paid) {
				const { data: purchaseResults, total } = (await this.app
					.service('vas-purchases')
					.find({
						query: { transactionId: data.transactionId },
					})) as Paginated<IVasPurchaseRead>;
				if (total < 1) {
					throw new BadRequest('Purchase not found');
				}
				if (purchaseResults[0].isComplete) {
					throw new BadRequest('Purchase complete');
				}
				const { response, purchased } = await this.PerformVasPurchase(
					purchaseResults[0],
				);
				if (!purchased) {
					throw new BadRequest('Purchase was not complete');
				}
				await this.app
					.service('vas-purchases')
					.patch(purchaseResults[0].id, {
						isComplete: true,
						purchaseResponse: response,
					});
				return { purchased };
			}
		} catch (error) {
			throw new BadRequest(error);
		}
	}

	async PerformVasPurchase(purchaseData: IVasPurchaseRead) {
		try {
			if (!purchaseData.purchaseRequest.sessionID) {
				throw new BadRequest('Session Id not found in purchase data');
			}
			const purchaseResult = await vasPostRequest(
				purchaseData.purchaseRequest.data,
				`vas/sessions/${purchaseData.purchaseRequest.sessionID}/purchase`,
			);
			console.log(purchaseResult.data, 'LLLLLLLLLLL');

			if (purchaseResult.data) {
				if (purchaseResult.data.status === 'SUCCESS') {
					return { response: purchaseResult.data, purchased: true };
				}
				if (purchaseResult.data.status === 'PENDING') {
					let trials = 0;
					while (trials < 10) {
						const delay = (time: number) =>
							new Promise((resolve) => setTimeout(resolve, time));
						await delay(1000);
						const statusResponse = await vasGetRequest(
							`vas/sessions/${purchaseData.purchaseRequest.sessionID}`,
						);

						if (statusResponse.data.status !== 'PENDING') {
							if (statusResponse.data.status === 'SUCCESS') {
								return {
									response: statusResponse.data,
									purchased: true,
								};
							}
							return {
								response: statusResponse.data,
								purchased: false,
							};
						}
						trials++;
					}
				}
				return { response: purchaseResult, purchased: false };
			}
		} catch (error) {
			console.log(error);
			throw new BadRequest('Failed during purchase');
		}
	}
}

export default CompleteVasPayment;
