import GetProducts from './getProducts';
import CompleteVasPayment from './completeVasPayment';

const actions = {
	GetProducts,
	CompleteVasPayment
};

export default actions;
