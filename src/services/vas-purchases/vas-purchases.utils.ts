/* eslint-disable no-param-reassign */
import app from '@app/app';
import { HookContext } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import qs from 'qs';
import * as Sentry from '@sentry/node';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import {
	IVasPurchaseRead,
	IVasPurchaseWrite,
} from '@app/models/vas-purchases.model';
import { generateTrackId } from '../utils';
import { awardCreditPoints } from '../loyalty-points/loyalty-points.utils';
import { IAPIHookContext } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import moment from 'moment';
import { formatCurrency } from '@app/handleCurrency';

interface Slip {
	units: string;
	meterNumber: string;
	totalAmount: number;
	subTotalAmount: number;
	vat: number;
	tokens: string;
	discount: number;
}

export const getAuthToken = async () => {
	try {
		const { authUrl, clientId, clientSecret } = app.get('kinektek-api');
		var data = qs.stringify({
			grant_type: 'client_credentials',
			client_id: clientId,
			scope: 'users/write',
			client_secret: clientSecret,
		});

		const response = await axios.post(authUrl, data, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		});
		return response.data?.access_token;
	} catch (error) {
		throw new BadRequest(error);
	}
};

export const productLookUp = async (
	context: HookContext<IVasPurchaseWrite>,
) => {
	try {
		const data = context.data as IVasPurchaseWrite;
		if (!data.lookupRequest.productID || !data.lookupRequest.reference) {
			throw new BadRequest('Invalid lookup request');
		}

		const response = await vasPostRequest(data.lookupRequest, 'vas/lookup');

		if (response) {
			context.data.lookupResponse = response.data;
			if (response.data.description === 'ERROR') {
				throw new BadRequest('Invalid  request');
			}
			context.data.purchaseRequest = {
				data: {
					slipWidth: 60,
					productID: data.lookupRequest.productID,
					clientReference: uuidv4(),
					amount: data.amount,
					tenderDetails: [{ type: 'CASH', amount: data.amount }],
					currency: '710',
				},
				sessionID: response.data.sessionID as string,
			};
		}
		context.data = { ...context.data };
		return context;
	} catch (error) {
		throw new BadRequest('Product Lookup Failed');
	}
};

export const addTransactionId = async (context: HookContext) => {
	const result = context.result as IVasPurchaseRead;
	const { app } = context;
	const productSuffix = result.productName.charAt(0).toUpperCase();
	const transactionId = generateTrackId(`AMB${productSuffix}UP${result.id}-`);
	await app.service('vas-purchases').patch(result.id, { transactionId });
	return context;
};

export const vasPostRequest = async <T extends object>(
	data: T,
	apiEndpoint: string,
) => {
	const { hostUrl, apikey } = app.get('kinektek-api');
	const token = await getAuthToken();

	Sentry.captureEvent({
		message: `Capture  Vas request`,
		request: {
			url: apiEndpoint,
			headers: { Authorization: `Bearer ${token}` },
			data: { ...data },
		},
		event_id: uuidv4(),
	});

	const response = await axios.post(
		`${hostUrl}/${apiEndpoint}`,
		{ ...data },
		{
			headers: {
				Authorization: `Bearer ${token}`,
				'x-api-key': apikey,
			},
		},
	);

	return response;
};

export const vasGetRequest = async (apiEndpoint: string) => {
	const { hostUrl, apikey } = app.get('kinektek-api');
	const token = await getAuthToken();

	Sentry.captureEvent({
		message: `Capture  Vas request`,
		request: {
			url: apiEndpoint,
			headers: { Authorization: `Bearer ${token}` },
		},
		event_id: uuidv4(),
	});

	const response = await axios.get(`${hostUrl}/${apiEndpoint}`, {
		headers: {
			Authorization: `Bearer ${token}`,
			'x-api-key': apikey,
		},
	});

	return response;
};

export const getSlipVariables = (vendResponse: {
	slipData: Array<{
		text: string;
		doubleHeight?: boolean;
		doubleWidth?: boolean;
	}>;
}) => {
	try {
		const { slipData: data } = vendResponse;
		const slip = {} as Slip;
		for (let x = 0; x < data?.length; x++) {
			const currentText = data?.[x]?.text?.replace(/ /g, '');
			const checkNumber = Number(currentText);
			if (Number.isInteger(checkNumber) && checkNumber !== 0) {
				slip.tokens = data?.[x]?.text?.trim();
			} else {
				const splited = data?.[x]?.text?.split(':');
				if (data?.[x]?.text?.includes('Units (kWh):')) {
					slip.units = splited?.[1]?.trim();
				}
				if (data?.[x]?.text?.includes('Tot.Elec:')) {
					slip.subTotalAmount = Number(splited?.[1]?.trim());
				}
				if (data?.[x]?.text?.includes('Tot.VAT:')) {
					slip.vat = Number(splited?.[1]?.trim());
				}
				if (data?.[x]?.text?.includes('TOTAL:')) {
					slip.totalAmount = Number(splited?.[1]?.trim());
				}
				if (data?.[x]?.text?.includes('Meter:')) {
					slip.meterNumber = splited?.[1]?.trim();
				}
			}
		}
		return slip;
	} catch (error) {
		const err = error as Error;
		throw err;
	}
};

export const updateLoyaltyPoints = async (context: HookContext) => {
	const { app, params, result } = context as IAPIHookContext<
		{
			addLoyalty: boolean;
		} & IVasPurchaseRead
	>;
	const user =
		params && params.user
			? (params.user as IUserRead)
			: result && result.userId
			? await app.service('users').get(result.userId)
			: null;

	if (result == null) {
		throw new BadRequest('Data was not provided');
	}

	const { phoneNumber, creditAmount } = result;

	try {
		if (result.isComplete) {
			if (user == null) {
				throw new BadRequest('No user could be matched to the payment');
			}

			await awardCreditPoints(user, result.id, creditAmount, phoneNumber);
		}

		return context;
	} catch (err) {
		context.app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not update loyalty points',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: context.result.transactionRef,
		});

		throw err;
	}
};

export const sendElectricityTokens = async (context: HookContext) => {
	const { app, result } = context as IAPIHookContext<IVasPurchaseRead>;
	try {
		if (result.isComplete) {
			if (result.productName === 'Electricity') {
				const { phoneNumber } = result;
				const slipVariables = getSlipVariables(
					result.purchaseResponse as {
						slipData: Array<{
							text: string;
							doubleHeight?: boolean;
							doubleWidth?: boolean;
						}>;
					},
				);
				if (slipVariables == null) {
					throw new BadRequest('Slip data is not defined');
				}
				if (phoneNumber && slipVariables.tokens) {
					const message = `AMABILLS TOKENS
					Units: ${slipVariables.units} kWh
					Token: ${slipVariables.tokens}
					Subtotal: R ${slipVariables.subTotalAmount}
					VAT: ${slipVariables.vat}
					Total: R ${slipVariables.totalAmount}`;
					console.log(message);
					const messages = [
						{
							content: message,
							destination: phoneNumber.replace('+', ''),
						},
					];

					await app.service('send-sms').create({ messages });
				}
			}
		}
	} catch (err) {
		app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not send prepaid sms',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: result.transactionId,
		});

		throw err;
	}
};
