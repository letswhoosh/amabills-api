import { IApplication } from '@app/declarations';
import { Params } from '@feathersjs/feathers';

export interface IPaymentResults {
	merchant: string;
	paid: boolean;
}

class ReceivePaymentResults {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: IPaymentResults, params?: Params) {
		console.log(
			data,
			'PPPPPPPPPPPPRPRPRPRPPRPRPRPRPRPPRRPRPRPPRPRPRPRPRPRRRRPRPRPRPRPPRRPPRPRRPRRPRRPPRRPR',
		);
		return 'success';
	}
}

export default ReceivePaymentResults;
