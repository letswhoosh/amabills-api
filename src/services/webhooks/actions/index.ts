import ReceivePaymentResults from './receivePaymentResults';

const actions = {
	ReceivePaymentResults,
};

export default actions;
