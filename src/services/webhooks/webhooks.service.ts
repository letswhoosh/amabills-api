// Initializes the `webhooks` service on path `/webhooks`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { Webhooks } from './webhooks.class';
import hooks from './webhooks.hooks';
import WebhookActions from './actions';

// Add this service to the service type index
declare module '../../declarations' {
	interface ServiceTypes {
		webhooks: Webhooks & ServiceAddons<any>;
	}
}

export default function (app: IApplication): void {
	const options = {
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/payments-results', new WebhookActions.ReceivePaymentResults(app));
	app.use('/webhooks', new Webhooks(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('webhooks');

	service.hooks(hooks);
}
