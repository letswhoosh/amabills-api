// Initializes the `uploads` service on path `/uploads`
import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import { Storage } from '@google-cloud/storage';
// @ts-ignore Needs TS Typing
import { IApplication } from '@app/declarations';
import hooks from './uploads.hooks';
import { Uploads } from './uploads.class';
import { BadRequest } from '@feathersjs/errors';

const multipartMiddleware = multer({
	storage: multer.memoryStorage(),
	limits: {
		fileSize: 5 * 1024 * 1024, // no larger than 5mb, you can change as needed.
	},
});

const uploadData = (req: Request, res: Response) => {
	console.log(req.file.originalname, 'PPPPPPPPPP≥');

	const storage = new Storage({
		projectId: 'amabills-356213',
	});
	const bucket = storage.bucket('amabills-uploads');
	const file = bucket.file(req.file.originalname);

	const stream = file.createWriteStream({
		metadata: {
			contentType: req.file.mimetype,
		},
	});

	stream.on('error', (err) => {
		throw new BadRequest('Error uploading file');
	});

	const publicUrl = `https://storage.googleapis.com/${bucket.name}/${file.name}`;
	req.feathers.file = publicUrl;

	stream.end(req.file.buffer);
};

// File storage location. Folder must be created before upload.
// Example: './uploads' will be located under feathers app top level.

export default function (app: IApplication) {
	// Initialize our service with any options it requires
	app.use(
		'/uploads',
		// multipartMiddleware.single('file'),
		(req: Request, res: Response, next: NextFunction) => {
			// @ts-ignore feathers is added to req object
			// uploadData(req, res);
			req.feathers.file = 'publicUrl';

			console.log("JJJJJJJJJJJ")
			next();
		},
		new Uploads(app),
	);

	// Get our initialized service so that we can register hooks
	const service = app.service('uploads');

	service.hooks(hooks);
}
