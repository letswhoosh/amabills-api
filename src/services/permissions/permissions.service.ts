// Initializes the `permissions` service on path `/permissions`
import { IApplication } from '@app/declarations';
import { Permissions } from './permissions.class';
import createModel from '@app/models/permissions.model';
import hooks from './permissions.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/permissions', new Permissions(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('permissions');

	service.hooks(hooks);
};
