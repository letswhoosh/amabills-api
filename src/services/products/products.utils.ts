import { IApplication } from '@app/declarations';
import { IProductRead } from '@app/models/products.model';
import { Paginated } from '@feathersjs/feathers';

export const addOrUpdateProduct = async (
	productName: string,
	productId: string,
	app: IApplication,
) => {
	const { total, data } = (await app
		.service('products')
		.find({ query: { productName } })) as Paginated<IProductRead>;

	if (total > 0) {
		return data[0];
	}
	const newProduct = app
		.service('products')
		.create({ productId, productName });
	return newProduct;
};
