import { IProductRead } from '@app/models/products.model';
import { Paginated } from '@feathersjs/feathers';
import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '../../declarations';

export class Products extends Service {
	app: IApplication;

	//eslint-disable-next-line @typescript-eslint/no-unused-vars
	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
		this.app = app;
	}

}
