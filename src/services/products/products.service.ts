// Initializes the `products` service on path `/products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { Products } from './products.class';
import createModel from '../../models/products.model';
import hooks from './products.hooks';

export default function (app: IApplication): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/products', new Products(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('products');

  service.hooks(hooks);
}
