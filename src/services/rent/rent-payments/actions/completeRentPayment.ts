import { IApplication } from '@app/declarations';
import { IRentPaymentRead } from '@app/models/rent/rent-payments.model';
import { BadRequest } from '@feathersjs/errors';
import { Paginated, Params } from '@feathersjs/feathers';

export interface RentPayment {
	transactionId: string;
	paid: boolean;
}

class CompleteRentPayment {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: RentPayment, params?: Params) {
		const response = await this.CompleteRentPayment(data);
		return response;
	}

	async CompleteRentPayment(data: RentPayment) {
		try {
			const { data: payments, total } = (await this.app
				.service('rent/rent-payments')
				.find({
					query: { transactionId: data.transactionId },
				})) as Paginated<IRentPaymentRead>;
			if (data.paid) {
				if (total < 1) {
					throw new BadRequest('Payment not found');
				}
				if (payments[0].isComplete) {
					throw new BadRequest('Payment already complete');
				}
				const result = await this.app
					.service('rent/rent-payments')
					.patch(payments[0].id, { isComplete: true });
				if (result.isComplete) {
					return {
						purchased: true,
					};
				}

				return {
					purchased: false,
				};
			}
			return {
				purchased: false,
			};
		} catch (error) {
			console.log(error)
			throw new BadRequest('Failed during payment processing');
		}
	}
}

export default CompleteRentPayment;
