import { HooksObject } from '@feathersjs/feathers';
import * as authentication from '@feathersjs/authentication';
import {
	addTransactionId,
	updateLoyaltyPoints,
	sendPaymentNotification,
} from './rent-payments.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [addTransactionId],
		update: [updateLoyaltyPoints, sendPaymentNotification],
		patch: [updateLoyaltyPoints, sendPaymentNotification],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
