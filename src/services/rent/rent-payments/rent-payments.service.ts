// Initializes the `rent/rent-payments` service on path `/rent/rent-payments`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../../declarations';
import { RentPayments } from './rent-payments.class';
import createModel from '../../../models/rent/rent-payments.model';
import hooks from './rent-payments.hooks';
import RentPaymentActions from './actions'

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/complete-rent-payment', new RentPaymentActions.CompleteRentPayment(app));

	app.use('/rent/rent-payments', new RentPayments(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('rent/rent-payments');

	service.hooks(hooks);
}
