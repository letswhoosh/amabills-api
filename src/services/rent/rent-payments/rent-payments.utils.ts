/* eslint-disable no-param-reassign */
import { IAPIHookContext } from '@app/declarations';
import { IRentPaymentRead } from '@app/models/rent/rent-payments.model';
import { IUserRead } from '@app/models/users.model';
import { awardCreditPoints } from '@app/services/loyalty-points/loyalty-points.utils';
import { generateTrackId } from '@app/services/utils';
import { BadRequest } from '@feathersjs/errors';
import { HookContext } from '@feathersjs/feathers';

export const addTransactionId = async (context: HookContext) => {
	const result = context.result as IRentPaymentRead;
	const { app } = context;
	const transactionId = generateTrackId(`AMBRP${result.id}-`);
	await app.service('rent/rent-payments').patch(result.id, { transactionId });
	return context;
};

export const updateLoyaltyPoints = async (context: HookContext) => {
	const { app, params, result } = context as IAPIHookContext<
		{
			addLoyalty: boolean;
		} & IRentPaymentRead
	>;
	const user =
		params && params.user
			? (params.user as IUserRead)
			: result && result.userId
			? await app.service('users').get(result.userId)
			: null;

	if (result == null) {
		throw new BadRequest('Data was not provided');
	}

	const { phoneNumber, creditAmount } = result;

	try {
		if (result.isComplete) {
			if (user == null) {
				throw new BadRequest('No user could be matched to the payment');
			}

			await awardCreditPoints(user, result.id, creditAmount, phoneNumber);
		}

		return context;
	} catch (err) {
		context.app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not update loyalty points',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: context.result.transactionRef,
		});

		throw err;
	}
};

export const sendPaymentNotification = async (context: HookContext) => {
	const result = context.result as IRentPaymentRead;
	if (result.isComplete) {
		const messages = [
			{
				content: `Transaction ${result.transactionId} confirmed.Your rent payment has been received.
				 Please go amabills.com for more details. From AMABILLS`,
				destination: result.phoneNumber.replace('+', ''),
			},
		];
		await context.app.service('send-sms').create({
			messages,
		});
	}

	return context;
};
