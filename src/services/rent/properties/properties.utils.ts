/* eslint-disable no-param-reassign */
import { IApplication } from '@app/declarations';
import { IHouseUnitWrite } from '@app/models/rent/house-units.model';
import { IPropertyRead } from '@app/models/rent/properties.model';
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { house_units } = sequelize.models;
	if (context.params?.query?.$loadEager) {
		context.params.sequelize = {
			raw: false,
			include: [
				{
					model: house_units,
				},
			],
		};
	}
	delete context.params?.query?.$loadEager;
	return context;
};

export const addHouseUnits = async (context: HookContext) => {
	const { result, app } = context;
	const unitsData = (result as IPropertyRead).unitsData;
	if (unitsData) {
		let unitNumber = 1;
		const unitsDetails: IHouseUnitWrite[] = [];
		for (let unitType in unitsData) {
			for (let x = 0; x < unitsData[unitType]; x++) {
				const unitData: IHouseUnitWrite = {
					unitName: `Unit ${unitNumber}`,
					rentAmount: 0,
					type: unitType,
					description: unitType,
					vacant: true,
					unitNumber: `${unitNumber}`,
					referenceNumber: `AMBRP${result.id}-UNT${unitNumber}`,
					propertyId: result.id,
				};
				unitsDetails.push(unitData);
				unitNumber++;
			}
		}

		await (app as IApplication)
			.service('rent/house-units')
			.create(unitsDetails);
	}
};
