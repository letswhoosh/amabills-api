// Initializes the `rent/properties` service on path `/rent/properties`
import { IApplication } from '../../../declarations';
import { Properties } from './properties.class';
import createModel from '../../../models/rent/properties.model';
import hooks from './properties.hooks';

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/rent/properties', new Properties(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('rent/properties');

	service.hooks(hooks);
}
