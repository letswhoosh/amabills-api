import { shouldSequelizeModel } from '@app/services/utils';
import * as authentication from '@feathersjs/authentication';
import { addHouseUnits, attachAssociations } from './properties.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [attachAssociations],
		get: [attachAssociations],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [shouldSequelizeModel],
		find: [],
		get: [],
		create: [addHouseUnits],
		update: [],
		patch: [],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
