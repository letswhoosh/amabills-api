import * as authentication from '@feathersjs/authentication';
import {
	attachAssociations,
	notifyHouseOccupant,
} from '@app/services/rent/house-units/house-units.utils';
import { shouldSequelizeModel } from '@app/services/utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [attachAssociations],
		get: [attachAssociations],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [shouldSequelizeModel],
		find: [],
		get: [],
		create: [],
		update: [notifyHouseOccupant],
		patch: [notifyHouseOccupant],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
