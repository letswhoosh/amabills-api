/* eslint-disable no-param-reassign */
import { IHouseUnitRead, IOccupant } from '@app/models/rent/house-units.model';
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { properties } = sequelize.models;

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: properties,
			},
		],
	};

	return context;
};

export const notifyHouseOccupant = async (context: HookContext) => {
	const result = context.result as IHouseUnitRead;
	if (result.occupants) {
		let messages: { content: string; destination }[] = [];
		for (let x = 0; x < result.occupants.length; x++) {
			const occupant = result.occupants[x] as IOccupant;
			if (occupant.sendNotification) {
				messages.push({
					content: `Dear ${occupant.firstName},you have been added as occupant for house ${result.unitName}.
						 Your house reference number is ${result.referenceNumber}.
						 Please go amabills.com for more details. From AMABILLS`,
					destination: occupant.phoneNumber.replace('+', ''),
				});
			}
		}

		if (messages.length) {
			await context.app.service('send-sms').create({
				messages,
			});
		}
	}
	return context;
};

