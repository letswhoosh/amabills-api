// Initializes the `rent/house-units` service on path `/rent/house-units`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../../declarations';
import { HouseUnits } from './house-units.class';
import createModel from '../../../models/rent/house-units.model';
import hooks from './house-units.hooks';


export default function (app: IApplication): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
		multi: true,
  };

  // Initialize our service with any options it requires
  app.use('/rent/house-units', new HouseUnits(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('rent/house-units');

  service.hooks(hooks);
}
