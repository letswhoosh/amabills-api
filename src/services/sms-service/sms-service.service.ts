// Initializes the `sms-service` service on path `/sms-service`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { SmsService } from './sms-service.class';
import createModel from '../../models/sms-service.model';
import hooks from './sms-service.hooks';
import SmsActions from './actions';

// Add this service to the service type index
declare module '../../declarations' {
	interface ServiceTypes {
		'sms-service': SmsService & ServiceAddons<any>;
	}
}

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/send-sms', new SmsActions.SendTextMessage(app));
	app.use('/sms-service', new SmsService(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('sms-service');

	service.hooks(hooks);
}
