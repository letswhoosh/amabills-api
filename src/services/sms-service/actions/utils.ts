import app from '@app/app';
import axios from 'axios';

export const getAuthToken = async () => {
	const { clientId, secret, authUrl } = app.get('sms_portal');
	const apiKey = clientId;
	const apiSecret = secret;
	const accountApiCredentials = apiKey + ':' + apiSecret;

	//@ts-ignore
	const buff = new Buffer.from(accountApiCredentials);
	const base64Credentials = buff.toString('base64');

	const authHeader = 'Basic ' + base64Credentials;
	const config = {
		headers: {
			Authorization: authHeader,
		},
	};
	const result = await axios.get(authUrl, config);
	return result.data?.token;
};
