import app from '@app/app';
import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import axios from 'axios';
import { getAuthToken } from './utils';

class SendTextMessage {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async create(data: { messages: any[] }) {
		const { messages } = data;
		const { smsUrl } = app.get('sms_portal');
		try {
			const token = await getAuthToken();
			let authHeader = 'Bearer ' + token;
			let config = {
				headers: {
					Authorization: authHeader,
					'Content-Type': 'application/json',
				},
			};

			let data = JSON.stringify({
				messages: messages,
			});
			const result = await axios.post(smsUrl, data, config);
			if (result.data) {
				console.log(result.data, "FFFFFFFFFFFFFFFWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
				return { data: { ...result.data } };
			}
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default SendTextMessage;
