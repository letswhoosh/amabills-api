/* eslint-disable no-param-reassign */
import { HookContext } from '@feathersjs/feathers';
import R from 'ramda';
import { BadRequest } from '@feathersjs/errors';
import { IAPIHookContext } from '@app/declarations';
import { IVendorTransactionWrite } from '@app/models/vendor-transactions.model';

export const vendorUpdateBalance = async (
	context: HookContext<IVendorTransactionWrite>,
) => {
	const { app, data } = context as IAPIHookContext<IVendorTransactionWrite>;

	if (data == null) {
		throw new BadRequest('No data was provided');
	}

	const { vendorId, amountInCents } = data;

	if (amountInCents == null) {
		throw new BadRequest(new Error('Amount is required'));
	}

	if (vendorId == null) {
		throw new BadRequest(new Error('Vendor ID is required'));
	}

	const transactions = await app.service('vendor-transactions').find({
		query: {
			vendorId,
			$sort: {
				createdAt: -1,
			},
		},
	});

	const currentBalanceInCents = R.pathOr(
		0,
		['data', 0, 'currentBalanceInCents'],
		transactions,
	) as number;
	const newBalance = Number(currentBalanceInCents) + Number(amountInCents);
	const type = Number(amountInCents) > 0 ? 'credit' : 'debit';
	if (currentBalanceInCents == null) {
		context.data = {
			...context.data,
			prevBalanceInCents: 0,
			currentBalanceInCents: Number(amountInCents),
			type,
		};
	} else {
		context.data = {
			...context.data,
			prevBalanceInCents: Number(currentBalanceInCents),
			currentBalanceInCents: Number(newBalance),
			type,
		};
	}

	return context;
};
