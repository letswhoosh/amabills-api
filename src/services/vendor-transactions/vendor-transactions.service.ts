// Initializes the `vendorTransactions` service on path `/vendor-transactions`
import { IApplication } from '@app/declarations';
import { VendorTransactions } from './vendor-transactions.class';
import createModel from '@app/models/vendor-transactions.model';
import hooks from './vendor-transactions.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/vendor-transactions', new VendorTransactions(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vendor-transactions');

	service.hooks(hooks);
};
