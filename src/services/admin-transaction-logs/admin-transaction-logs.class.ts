import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '../../declarations';

export class AdminTransactionLogs extends Service {
	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
	}
}
