/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';
import * as R from 'ramda';
import { IAPIHookContext } from '@app/declarations';

interface Search {
	like: string;
}

export const attachAssociations = async (context: HookContext) => {
	const { app } = context as IAPIHookContext;
	const sequelize: Sequelize.Sequelize = app.get('sequelizeClient');
	const { users, purchases } = sequelize.models;
	const { Op } = Sequelize;

	const $search = R.path<Search>(['params', 'query', '$search'], context);

	if (context.params.query && $search != null) {
		delete context.params.query.$search;
	}

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: users,
				as: 'user',
			},
			{
				model: purchases,
				as: 'purchase',
			},
		],
	};

	if ($search != null) {
		context.params.sequelize.where = {
			[Op.or]: [
				{
					'$user.email$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					'$user.contactNumber$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					'$purchase.id$': Number($search.like),
				},
			],
		};
	}

	return context;
};
