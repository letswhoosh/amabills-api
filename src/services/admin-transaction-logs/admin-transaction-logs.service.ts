// Initializes the `admin-transaction-logs` service on path `/admin-transaction-logs`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { AdminTransactionLogs } from './admin-transaction-logs.class';
import createModel from '../../models/admin-transaction-logs.model';
import hooks from './admin-transaction-logs.hooks';

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/admin-transaction-logs', new AdminTransactionLogs(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('admin-transaction-logs');

	service.hooks(hooks);
}
