/* eslint-disable no-param-reassign */
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { vendors } = sequelize.models;

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: vendors,
			},
		],
	};

	return context;
};
