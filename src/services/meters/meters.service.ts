// Initializes the `meters` service on path `/meters`
import { IApplication } from '@app/declarations';
import { Meters } from './meters.class';
import createModel from '@app/models/meters.model';
import hooks from './meters.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/meters', new Meters(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('meters');

	service.hooks(hooks);
};
