// Initializes the `kinektek-api` service on path `/kinektek-api`
import { IApplication } from '../../declarations';
import { KinektekApi } from './kinektek-api.class';
import hooks from './kinektek-api.hooks';
import KinekteActions from './actions';

export default function (app: IApplication): void {
	const options = {
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/kinektek-api', new KinektekApi(options, app));
	app.use('/kinektek-api/products', new KinekteActions.GetProducts(app));
	app.use('/kinektek-api/lookup', new KinekteActions.LookUpProduct(app));
	app.use('/kinektek-api/session-purchase', new KinekteActions.PurchaseAfterLookUp(app));
	app.use('/kinektek-api/purchase-status', new KinekteActions.GetPurchaseStatus(app));
	app.use('/kinektek-api/confirm-purchase', new KinekteActions.ComfirmPurchase(app));


	// Get our initialized service so that we can register hooks
	const service = app.service('kinektek-api');

	service.hooks(hooks);
}
