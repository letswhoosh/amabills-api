import { IApplication } from '@app/declarations';
import { Params } from '@feathersjs/feathers';
import {
	IPurchaseTransactionData,
	kinektekPostRequest,
	kinektekPurchaseResponse,
} from '../kinektek-api.utils';

interface ITenderDetails {
	amount: number;
	type: string;
}

export interface IPurchaseData {
	productID: string;
	clientReference: string;
	amount: number;
	currency: string;
	tenderDetails: Array<ITenderDetails>;
	slipWidth: number;
}

export interface IPurchaseReq {
	purchaseData: IPurchaseData;
	sessionID: string;
}

export interface IPurchaseRes extends IPurchaseReq {
	retryAfter: number;
	status: string;
}

class PurchaseAfterLookUp {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: IPurchaseReq, params?: Params) {
		const { purchaseData, sessionID } = data;
		let response = await kinektekPostRequest(
			this.app,
			`vas/sessions/${sessionID}/purchase`,
			purchaseData,
			'purchase after lookup',
		);
		return response.data;
	}
}

export default PurchaseAfterLookUp;
