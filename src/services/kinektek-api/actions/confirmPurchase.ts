import { IApplication } from '@app/declarations';
import { kinektekPostRequest } from '../kinektek-api.utils';

export interface ConfirmPurchaseData {
	sessionID: string;
}

export interface ConfirmPurchaseResponse {
	message: string;
	success: boolean;
	slipData?: { [key: string]: any };
}

class ConfirmPurchase {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: ConfirmPurchaseData) {
		const { sessionID } = data;
		const response = await kinektekPostRequest<ConfirmPurchaseData>(
			this.app,
			`vas/sessions/${sessionID}/confirmPurchase`,
			data,
			'confirm purchase',
		);
		return response.data;
	}
}

export default ConfirmPurchase;
