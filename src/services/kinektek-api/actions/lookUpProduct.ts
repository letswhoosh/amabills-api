import { IApplication } from '@app/declarations';
import { Params } from '@feathersjs/feathers';
import { kinektekPostRequest } from '../kinektek-api.utils';

export interface ILookUpData {
	productID: string;
	reference: string;
}

export interface ILookUpResponse extends ILookUpData{
	sessionID: string;
	reference: string;
	message: string;
}

class LookUpProduct {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: ILookUpData, params?: Params) {
		console.log(data, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
		const response = await kinektekPostRequest<ILookUpData>(
			this.app,
			'vas/lookup',
			data,
			'lookup',
			1,
			{},
			true
		);
		// console.log(response, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

		return response.data;
	}
}

export default LookUpProduct;
