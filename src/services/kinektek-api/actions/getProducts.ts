import { IApplication } from '@app/declarations';
import { Params } from '@feathersjs/feathers';
import { kinektekGetRequest } from '../kinektek-api.utils';

export interface IKinekteCatologue {
	name: string;
	subCatalogues: Array<IKinekteProductGroup>;
}

export interface IKinekteProductGroup {
	name: string;
	subCatalogues: Array<IKinekteProductCategory>;
	products: Array<IKinekteProduct>;
}
export interface IKinekteProductCategory {
	name: string;
	subCatalogues: Array<IKinekteProduct>;
}
export interface IKinekteProduct {
	name: string;
	products: Array<IKinekteProductDetails>;
}

export interface IKinekteProductDetails {
	productID: string;
	issuer: string;
	name: string;
	imageName: string;
	price: number;
	dualStep: boolean;
}

interface IGetProductQuery {
	productGroup: string;
	productName: string;
	productCategory: string;
}

class GetProducts {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async find(params?: Params): Promise<IKinekteProduct[] | IKinekteProduct> {
		const query = params?.query as IGetProductQuery;
		const { productGroup, productName, productCategory } = query;
		const productCategories = (
			await kinektekGetRequest(
				this.app,
				`catalogue/products/${productGroup}`,
				'all products',
			)
		).data as IKinekteCatologue;
		let filteredProducts = {} as IKinekteProductGroup;
		if (productCategory) {
			filteredProducts = productCategories?.subCatalogues.find(
				(category) => category.name === productCategory,
			) as IKinekteProductGroup;
		}
		if (productName) {
			const products =
				filteredProducts?.subCatalogues.find(
					(filtered) => filtered.name === productName,
				);
			//@ts-ignore
			filteredProducts = products?.subCatalogues || products?.products || ([] as IKinekteProduct[])

		}
		//@ts-ignore
		return filteredProducts;
	}
}

export default GetProducts;
