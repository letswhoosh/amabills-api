import { IApplication } from '@app/declarations';
import { kinektekGetRequest } from '../kinektek-api.utils';

export interface PurchaseStatuResponse {
	slipData?: { [key: string]: any };
	status: string;
}
class GetPurchaseStatus {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async get(sessionID: string) {
		let trials = 0;
		let status = 'PENDING';
		while (trials < 10) {
			const delay = (time: number) =>
				new Promise((resolve) => setTimeout(resolve, time));
			await delay(1000);
			const statusResponse = await kinektekGetRequest(
				this.app,
				`vas/sessions/${sessionID}`,
				'purchase after lookup status',
			);
			status = statusResponse.data.status;
			if (statusResponse.data.status !== 'PENDING') {
				return statusResponse.data;
			}
			trials++;
		}
		return {
			status,
		};
	}
}

export default GetPurchaseStatus;
