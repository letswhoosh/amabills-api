import GetProducts from './getProducts';
import LookUpProduct from './lookUpProduct'
import PurchaseAfterLookUp from './purchase'
import ComfirmPurchase from './confirmPurchase'
import GetPurchaseStatus from './getPurchaseStatus'

const actions = {
	GetProducts,
	LookUpProduct,
	PurchaseAfterLookUp,
	ComfirmPurchase,
	GetPurchaseStatus
};

export default actions;
