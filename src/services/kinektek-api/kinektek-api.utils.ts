import { IApplication } from '@app/declarations';
import { IProductRead } from '@app/models/products.model';
import { GeneralError } from '@feathersjs/errors';
import { Paginated } from '@feathersjs/feathers';
import * as Sentry from '@sentry/node';
import axios, { AxiosError } from 'axios';
import qs from 'qs';
import { v4 as uuidv4 } from 'uuid';

export interface IPurchaseTransactionData {
	sessionId: string;
	amount: string;
	type: string;
	paidById: number;
	paidToId: number;
}

export const getAuthToken = async (app: IApplication) => {
try {
	const { authUrl, clientId, clientSecret } = app.get('kinektek-api');
	var data = qs.stringify({
		grant_type: 'client_credentials',
		client_id: clientId,
		scope: 'users/write',
		client_secret: clientSecret,
	});

	const response = await axios.post(authUrl, data, {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
	});
	return response.data?.access_token;
} catch (error) {
	throw new GeneralError('Kinektek Authentication failed')
}
};

export const kinektekGetRequest = async (
	app: IApplication,
	urlSuffix: string,
	methodName: string,
	retry = 1,
	params?: { [key: string]: string },
) => {
	try {
		const { hostUrl, apikey } = app.get('kinektek-api');
		const token = await getAuthToken(app);
		Sentry.captureEvent({
			message: `Capture kinektek ${methodName} request`,
			request: {
				url: ``,
				headers: { Authorization: `Bearer ${token}` },
			},
			event_id: uuidv4(),
		});
		const response = await axios.get(`${hostUrl}/${urlSuffix}`, {
			headers: { Authorization: `Bearer ${token}`, 'x-api-key': apikey },
			params,
		});
		return response;
	} catch (error) {
		if (retry > 0) {
			retry = 0;
			kinektekGetRequest(app, urlSuffix, methodName, 0, params);
		}
		throw new GeneralError(error as Error);
	}
};

export const kinektekPostRequest = async <T>(
	app: IApplication,
	urlSuffix: string,
	data: T,
	methodName: string,
	retry = 1,
	params?: { [key: string]: any },
	returnErrors?: boolean,
) => {
	try {
		const { hostUrl, apikey } = app.get('kinektek-api');
		const token = await getAuthToken(app);
		Sentry.captureEvent({
			message: `Capture kinektek ${methodName} request`,
			request: {
				url: ``,
				headers: { Authorization: `Bearer ${token}` },
			},
			event_id: uuidv4(),
		});
		const response = await axios.post(`${hostUrl}/${urlSuffix}`, data, {
			headers: { Authorization: `Bearer ${token}`, 'x-api-key': apikey },
			params,
		});
		return response;
	} catch (error) {
		console.log(error)
		if (retry > 0) {
			retry = 0;
			kinektekPostRequest(app, urlSuffix, data, methodName, 0, params);
		}
		const err = error as AxiosError;
		const message = err.response?.data.message;
		if (returnErrors) {
			if (err.response) {
				return err.response;
			}
		}
		throw new GeneralError(message || (error as Error));
	}
};

export const kinektekPurchaseResponse = (
	status: string,
	slipData?: { [key: string]: any },
) => {
	if (status === 'SUCCESS') {
		return {
			message: 'Purchase was succesfful',
			success: true,
			slipData,
		};
	} else {
		return {
			message: 'Purchase was not succesfful',
			success: false,
		};
	}
};


