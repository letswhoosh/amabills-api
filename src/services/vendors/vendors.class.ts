import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '@app/declarations';

export class Vendors extends Service {
	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
	}
}
