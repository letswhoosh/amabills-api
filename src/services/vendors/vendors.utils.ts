/* eslint-disable no-param-reassign */
import { HookContext, Paginated } from '@feathersjs/feathers';
import Sequelize from 'sequelize';
import { IPermissionRead } from '@app/models/permissions.model';
import { IUserRead } from '@app/models/users.model';
import { generateCode } from '../utils';
import { IAPIHookContext } from '@app/declarations';
import { IVendorRead, IVendorWrite } from '@app/models/vendors.model';
import { BadRequest } from '@feathersjs/errors';

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { meters } = sequelize.models;

	// eslint-disable-next-line no-param-reassign
	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: meters,
			},
		],
	};

	return context;
};

export const makeVendorUser = async (context: HookContext) => {
	const { app, result } = context as IAPIHookContext<IVendorRead>;

	if (result == null) {
		throw new BadRequest('No result was provided');
	}

	const { name, email, contactNumber } = result;

	const vendorPermissions = (await app.service('permissions').find({
		paginate: false,
		query: {
			name: 'isVendor',
			$select: ['id'],
		},
	})) as IPermissionRead[];

	const vendorPermissionIds = vendorPermissions.map((n) => n.id);

	const { data } = (await app.service('users').find({
		query: {
			email,
		},
	})) as Paginated<IUserRead>;

	let user = data.length > 0 ? data[0] : null;
	let password;

	if (user == null) {
		password = generateCode(6);
		await app.service('users').create(
			{
				firstName: name,
				lastName: '',
				email,
				contactNumber,
				permissionIds: vendorPermissionIds,
				vendorId: context.result.id,
				password,
			},
			{
				sendPassword: true,
			},
		);
	} else {
		const currentPermissionIds = user.permissions.map((n) => n.id);

		const userPermissions = [
			...currentPermissionIds,
			...vendorPermissionIds,
		].filter((v, i, a) => a.indexOf(v) === i);

		user = await app.service('users').patch(user.id, {
			vendorId: context.result.id,
			permissionIds: userPermissions,
		});
	}

	app.service('auth/send-vendor-login').create({
		email: context.result.email,
		presetPassword: password,
	});

	return context;
};

export const formatGeometry = async (context: HookContext<IVendorWrite>) => {
	const { coordinates } = context.data || {};

	const coords = (coordinates as unknown) as [number, number];
	if (coordinates != null) {
		const geoPoint = { type: 'Point', coordinates: coords } as {
			type: 'Point';
			coordinates: [number, number];
		};

		if (context.data == null) {
			throw new BadRequest('Data was not provided');
		}

		context.data.coordinates = geoPoint;
	}
	return context;
};

export const reformatGeometry = async (
	context: HookContext<IVendorRead | Paginated<IVendorRead>>,
) => {
	const event = context.dispatch || context.result;
	const getEvent = event as IVendorRead;
	const findEvent = event as Paginated<IVendorRead>;

	if (event == null) {
		throw new BadRequest('Data was not provided');
	}

	let updatedEvent;
	if (Array.isArray(findEvent.data)) {
		updatedEvent = findEvent.data.map((n: IVendorRead) => {
			if (n.coordinates != null) {
				const { coordinates } = n.coordinates;
				if (coordinates != null) {
					return { ...n, coordinates };
				}
			}
			return n;
		});
	} else {
		const { coordinates } = getEvent.coordinates || { coordinates: null };
		if (coordinates != null) {
			updatedEvent = { ...getEvent, coordinates };
		} else {
			updatedEvent = getEvent;
		}
	}

	if (Array.isArray(findEvent.data)) {
		(context.result as Paginated<
			IVendorRead
		>).data = updatedEvent as IVendorRead[];
	} else {
		(context.result as IVendorRead) = updatedEvent as IVendorRead;
	}

	return context;
};
