// Initializes the `vendors` service on path `/vendors`
import { IApplication } from '@app/declarations';
import { Vendors } from './vendors.class';
import createModel from '@app/models/vendors.model';
import hooks from './vendors.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/vendors', new Vendors(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vendors');

	service.hooks(hooks);
};
