import * as authentication from '@feathersjs/authentication';
import {
	attachAssociations,
	formatGeometry,
	reformatGeometry,
	makeVendorUser,
} from './vendors.utils';
import { shouldSequelizeModel } from '../utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [attachAssociations],
		get: [attachAssociations],
		create: [formatGeometry],
		update: [formatGeometry],
		patch: [formatGeometry],
		remove: [],
	},

	after: {
		all: [shouldSequelizeModel, reformatGeometry],
		find: [],
		get: [],
		create: [makeVendorUser],
		update: [],
		patch: [],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
