import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/send-vendor-login': SendVendorLogin;
	}
}

class SendVendorLogin {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	create(data: { presetPassword: string; email: string }) {
		const { presetPassword, email } = data;
		try {
			return this.utils.sendVendorLoginMail({
				presetPassword,
				email,
			});
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default SendVendorLogin;
