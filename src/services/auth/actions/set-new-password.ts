import { IApplication } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import { BadRequest } from '@feathersjs/errors';
import { Paginated } from '@feathersjs/feathers';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/send-new-password': SetNewPassword;
	}
}

/**
 * The user submits their new password together with the token
 * that was sent to them, if the token is valid then the users
 * password is updated.
 */
class SetNewPassword {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	async create(data: {
		email: string;
		resetToken: string;
		password: string;
	}) {
		const { email, resetToken, password } = data;
		try {
			if (email == null) {
				throw new BadRequest(new Error('Email was not provided'));
			}

			if (resetToken == null) {
				throw new BadRequest(new Error('Reset Token was not provided'));
			}

			if (password == null) {
				throw new BadRequest(new Error('Password was not provided'));
			}

			const users = (await this.app
				.service('users')
				.find({ query: { email } })) as Paginated<IUserRead>;

			if (users.total < 1) {
				return Promise.reject(
					new BadRequest(new Error('Email does not exist')),
				);
			}

			await this.utils.ResetPassword(users.data[0], password, resetToken);

			return {
				data: {
					message: 'Password has been updated.',
				},
				meta: {
					code: 204,
				},
			};
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default SetNewPassword;
