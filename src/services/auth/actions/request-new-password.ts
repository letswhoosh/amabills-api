import { IApplication } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import { BadRequest } from '@feathersjs/errors';
import { Paginated } from '@feathersjs/feathers';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/request-new-password': RequestNewPassword;
	}
}

/**
 * If the user forgot their password, they will fire off this action
 * in order to receive an email that provides them with a token and a link
 * from where they can change their password.
 */
class RequestNewPassword {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	async create(data: { email: string }) {
		const { email } = data;
		try {
			if (email == null) {
				throw new BadRequest(new Error('Email was not provided'));
			}

			const users = (await this.app
				.service('users')
				.find({ query: { email } })) as Paginated<IUserRead>;

			if (users.total < 1) {
				return Promise.reject(
					new BadRequest(new Error('Email does not exist')),
				);
			}

			return this.utils.SendPasswordResetMail(users.data[0]);
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default RequestNewPassword;
