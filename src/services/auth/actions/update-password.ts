import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/update-password': UpdatePassword;
	}
}

/**
 * The user submits their new password, if the old password matched then
 * the password is updated.
 */
class UpdatePassword {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	async create(data: {
		userId: string;
		currentPassword: string;
		newPassword: string;
	}) {
		const { userId, currentPassword, newPassword } = data;
		try {
			if (userId == null) {
				throw new BadRequest(new Error('User ID was not provided'));
			}

			if (currentPassword == null) {
				throw new BadRequest(
					new Error('Current password was not provided'),
				);
			}

			if (newPassword == null) {
				throw new BadRequest(
					new Error('New password was not provided'),
				);
			}

			const user = await this.app.service('users').get(userId);

			if (user == null) {
				throw new BadRequest('User does not exist');
			}

			await this.utils.UpdatePassword(user, newPassword, currentPassword);

			return Promise.resolve({
				data: {
					message: 'Password has been updated.',
				},
				meta: {
					code: 204,
				},
			});
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default UpdatePassword;
