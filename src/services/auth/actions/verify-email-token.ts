import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/verify-email-token': VerifyEmailToken;
	}
}

/**
 * Receive the verification request from the client,
 * if the token is valid and the user can be found
 * then can verify the account is valid.
 */
class VerifyEmailToken {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	async create(data: { token: string }) {
		const { token } = data;
		try {
			if (token == null) {
				throw new BadRequest(new Error('Token cannot be null.'));
			}

			const user = await this.utils.RetrieveUserFromToken(token);

			this.app.service('users').patch(user.id, {
				verified: true,
			});

			return {
				data: {
					message: 'User has been verified successfully',
					user,
				},
				meta: {
					code: 204,
				},
			};
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default VerifyEmailToken;
