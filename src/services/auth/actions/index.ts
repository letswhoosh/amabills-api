import Verify from './verify';
import VerifyEmailToken from './verify-email-token';
import VerifyCode from './verify-code';
import RequestNewPassword from './request-new-password';
import SetNewPassword from './set-new-password';
import UpdatePassword from './update-password';
import SendVendorLogin from './send-vendor-login';

const actions = {
	Verify,
	VerifyEmailToken,
	VerifyCode,
	RequestNewPassword,
	SetNewPassword,
	UpdatePassword,
	SendVendorLogin,
};

export default actions;
