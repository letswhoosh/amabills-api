import { IApplication } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import { BadRequest } from '@feathersjs/errors';
import { Paginated } from '@feathersjs/feathers';
import AuthUtils from '../auth.utils';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'auth/verify-code': VerifyCode;
	}
}

/**
 * This method also verifies a users account but does so by sending
 * an email to the user with a code which the user has to input into the
 * app, which is then finally confirmed here, and if all is good, account
 * gets verified.
 */
class VerifyCode {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	async create(data: { code: string; email: string }) {
		const { code, email } = data;
		try {
			if (code == null) {
				throw new BadRequest(new Error('Code cannot be null.'));
			}

			if (email == null) {
				throw new BadRequest(new Error('Email was not provided'));
			}

			const { data: users, total } = (await this.app
				.service('users')
				.find({ query: { email } })) as Paginated<IUserRead>;

			if (total < 1) {
				return Promise.reject(
					new BadRequest(new Error('Email does not exist')),
				);
			}

			await this.utils.VerifyAccountCode(code, users[0]);

			return {
				data: {
					message: 'User has been verified successfully',
				},
				meta: {
					code: 204,
				},
			};
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default VerifyCode;
