import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import AuthUtils from '../auth.utils';

class Verify {
	app: IApplication;

	utils: AuthUtils;

	constructor(app: IApplication) {
		this.utils = new AuthUtils(app);

		this.app = app;
	}

	create(data: { presetPassword: string; email: string }) {
		const { presetPassword, email } = data;
		try {
			return this.utils.sendVerificationEmail({
				presetPassword,
				email,
			});
		} catch (err) {
			throw new BadRequest(err);
		}
	}
}

export default Verify;
