// Initializes the `auth` service on path `/auth`
import { IApplication } from '@app/declarations';
import AuthActions from '../auth/actions';

export default (app: IApplication) => {
	app.use('/auth/verify', new AuthActions.Verify(app));
	app.use('/auth/verify-email-token', new AuthActions.VerifyEmailToken(app));
	app.use('/auth/verify-code', new AuthActions.VerifyCode(app));
	app.use(
		'/auth/request-new-password',
		new AuthActions.RequestNewPassword(app),
	);
	app.use('/auth/set-new-password', new AuthActions.SetNewPassword(app));
	app.use('/auth/update-password', new AuthActions.UpdatePassword(app));
	app.use('/auth/send-vendor-login', new AuthActions.SendVendorLogin(app));
};
