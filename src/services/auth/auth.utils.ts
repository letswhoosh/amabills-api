import jwt from 'jsonwebtoken';
import errors from '@feathersjs/errors';
import { Paginated } from '@feathersjs/feathers';
import bcrypt from 'bcryptjs';
import { IUserRead } from '@app/models/users.model';
import MailUtils from '../mailer/mailer.utils';
import {
	verifyTemplate,
	resetPassTemplate,
	verifyWithPassTemplate,
	VendorLoginTemplate,
} from '../mailer/templates';
import { IApplication } from '@app/declarations';

export default class AuthUtils {
	app: IApplication;

	mailUtils: MailUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.mailUtils = new MailUtils(app);
	}

	sendVerificationEmail = async (data: {
		email: string;
		presetPassword: string;
	}) => {
		const users = (await this.app.service('users').find({
			query: { email: data.email },
		})) as Paginated<IUserRead>;

		if (users.total < 1) {
			return Promise.reject(
				new errors.BadRequest('Email does not exist'),
			);
		}

		return this.SendVerifyMail(users.data[0], data.presetPassword);
	};

	verifyPassword = (currentPassword: string, storedPassword: string) =>
		new Promise((resolve, reject) => {
			bcrypt.compare(currentPassword, storedPassword, (error, result) => {
				if (error) {
					reject(error);
				} else if (result) {
					resolve(true);
				} else {
					reject(new Error('Wrong Password'));
				}
			});
		});

	RetrieveUserFromToken = async (token: string) => {
		try {
			const decoded = jwt.verify(
				token,
				this.app.get('authentication').secret,
			);
			const userId = (decoded as Record<string, any>)._doc.id;
			const user = await this.app.service('users').get(userId);
			return user;
		} catch (e) {
			throw new errors.Forbidden(new Error('Security token is invalid'));
		}
	};

	SendVerifyMail = async (user: IUserRead, password: string) => {
		const resetToken = jwt.sign(
			{
				_doc: { id: user.id },
			},
			this.app.get('authentication').secret,
			{ expiresIn: '3h' },
		);

		const verificationCode = this.generateCode(6);

		this.app.service('users').patch(user.id, { verificationCode });

		const { noreply } = this.app.get('emails');
		const { name, logo } = this.app.get('brand');
		const url = `${this.app.get('ui')}/verify-account/${resetToken}`;
		const verifyUrl = `${this.app.get('ui')}/verify-account`;

		let template;

		if (password != null) {
			template = verifyWithPassTemplate({
				url,
				name: `${user.firstName} ${user.lastName}`,
				title: 'Account Verification',
				company: name,
				logo,
				password,
				verificationCode,
				verifyUrl,
			});
		} else {
			template = verifyTemplate({
				url,
				name: `${user.firstName} ${user.lastName}`,
				title: 'Account Verification',
				company: name,
				logo,
				verificationCode,
				verifyUrl,
			});
		}

		const config = {
			to: user.email,
			from: {
				name: 'Account Verification',
				address: noreply,
			},
			subject: `${name} Account Verification`,
			html: template,
		};
		return this.mailUtils.RetrieveTransport().sendMail(config);
	};

	SendPasswordResetMail = async (user: IUserRead) => {
		const resetToken = jwt.sign(
			{
				_doc: { _id: user.id, email: user.email },
			},
			this.app.get('authentication').secret,
			{ expiresIn: '60m' },
		);

		this.app.service('users').patch(user.id, { resetToken });

		const { noreply } = this.app.get('emails');
		const { name, logo } = this.app.get('brand');

		const recoverText =
			'To recover your password please use the link below:';
		const url = `${this.app.get('ui')}/password-reset/${resetToken}`;

		const config = {
			to: user.email,
			from: {
				name: 'Password Recovery',
				address: noreply,
			},
			subject: `${name} Password Recovery`,
			html: resetPassTemplate({
				url,
				logo,
				company: name,
				name: `${user.firstName} ${user.lastName}`,
				recoverText,
				title: 'Password Recovery',
				linkText: 'Reset Password',
			}),
		};

		return this.mailUtils.RetrieveTransport().sendMail(config);
	};

	sendVendorLoginMail = async (data: {
		email: string;
		presetPassword: string;
	}) => {
		const users = (await this.app.service('users').find({
			query: { email: data.email },
		})) as Paginated<IUserRead>;

		if (users.total < 1) {
			return Promise.reject(
				new errors.BadRequest('Email does not exist'),
			);
		}

		const user = users.data[0];

		const { noreply } = this.app.get('emails');
		const { name, logo } = this.app.get('brand');
		const url = `${this.app.get('ui')}/signin`;

		let template;

		if (data.presetPassword != null) {
			template = VendorLoginTemplate({
				url,
				name: `${user.firstName} ${user.lastName}`,
				email: user.email,
				title: 'Account Verification',
				company: name,
				logo,
				password: data.presetPassword,
			});
		} else {
			template = VendorLoginTemplate({
				url,
				name: `${user.firstName} ${user.lastName}`,
				email: user.email,
				title: 'Account Verification',
				company: name,
				logo,
			});
		}
		const config = {
			to: user.email,
			from: {
				name: 'Account Verification',
				address: noreply,
			},
			subject: `${name} Account Verification`,
			html: template,
		};
		return this.mailUtils.RetrieveTransport().sendMail(config);
	};

	VerifyAccount = async (verifyToken: string) => {
		try {
			const user = await this.RetrieveUserFromToken(verifyToken);
			return this.app.service('users').patch(user.id, {
				verified: true,
			});
		} catch (e) {
			throw new errors.Forbidden(new Error('Security token is invalid'));
		}
	};

	VerifyAccountCode = async (code: string, user: IUserRead) => {
		if (code !== user.verificationCode) {
			throw new errors.Forbidden(new Error('Code is invalid'));
		}
		const getUser = this.app.service('users').patch(user.id, {
			verified: true,
			verificationCode: null,
		});

		return getUser;
	};

	ResetPassword = async (
		user: IUserRead,
		password: string,
		token: string,
	) => {
		try {
			jwt.verify(token, this.app.get('authentication').secret);
			return await this.app.service('users').patch(user.id, {
				verified: true,
				password,
			});
		} catch (e) {
			throw new errors.Forbidden(
				new Error(
					'Token has expired. You have to reset your password again.',
				),
			);
		}
	};

	UpdatePassword = async (
		user: IUserRead,
		newPassword: string,
		currentPassword: string,
	) => {
		try {
			await this.verifyPassword(currentPassword, user.password);
			return await this.app
				.service('users')
				.patch(user.id, { password: newPassword });
		} catch (e) {
			throw new errors.Forbidden(
				new Error('Current password was entered incorrectly.'),
			);
		}
	};

	generateCode = (n: number) => {
		const add = 1;
		let max = 12 - add;

		if (n > max) {
			return (module.exports.generateCode(max) +
				module.exports.generateCode(n - max)) as string;
		}

		max = Math.pow(10, n + add); // eslint-disable-line no-restricted-properties
		const min = max / 10;
		const number = Math.floor(Math.random() * (max - min + 1)) + min;

		return `${number}`.substring(add) as string;
	};
}
