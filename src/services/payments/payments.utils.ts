import * as Sentry from '@sentry/node';
import errors, { BadRequest } from '@feathersjs/errors';
import https from 'https';
import querystring from 'querystring';
import { IApplication } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import logger from '@app/logger';
import Dinero from '@app/handleCurrency';

interface IRegistration {
	id: string;
}

interface IPaymentRequest {
	entityId: string;
	amount: number | string;
	currency: 'ZAR';
	paymentType: 'DB';
	createRegistration?: boolean;
	testMode?: 'EXTERNAL';
	registrations?: IRegistration[];
}

interface ICheckoutResponse {
	result: {
		code: string;
		description: string;
	};
	buildNumber: string;
	timestamp: Date;
	ndc: string;
	id: string;
}

interface IPaymentStatusResponse {
	id: string;
	registrationId?: string;
	paymentType?: string;
	paymentBrand?: string;
	amount?: number;
	currency?: string;
	descriptor?: string;
	result: {
		code: string;
		description: string;
	};
	resultDetails?: {
		ExtendedDescription: string;
		AcquirerResponse: string;
		ConnectorTxID1: string;
		ConnectorTxID2: string;
	};
	card?: {
		bin: string;
		last4Digits: string;
		holder: string;
		expiryMonth: string;
		expiryYear: string;
	};
	threeDSecure?: {
		eci: string;
	};
	customParameters?: {
		CTPE_DESCRIPTOR_TEMPLATE: string;
	};
	risk?: {
		score: string;
	};
	buildNumber: string;
	timestamp: Date;
	ndc: string;
}

// Success
export const successTest = /^(000\.000\.|000\.100\.1|000\.[36])/;
export const successWithRiskTest = /^(000\.400\.0[^3]|000\.400\.100)/;
// Pending
export const pendingTest = /^(000\.200)/;
export const pendingChangeTest = /^(800\.400\.5|100\.400\.500)/;
// Failed
const rejectedByRiskTest = /^(000\.400\.[1][0-9][1-9]|000\.400\.2)/;
const rejectedByBankTest = /^(800\.[17]00|800\.800\.[123])/;
const rejectedByComErrorTest = /^(900\.[1234]00|000\.400\.030)/;
const rejectedBySystemErrorTest = /^(800\.[56]|999\.|600\.1|800\.800\.8)/;
const rejectedByAsyncWorkflowTest = /^(100\.39[765])/;
const rejectedByExternalRiskTest = /^(100\.400|100\.38|100\.370\.100|100\.370\.11)/;
const rejectedByAddressTest = /^(800\.400\.1)/;
const rejectedBy3DSecureTest = /^(800\.400\.2|100\.380\.4|100\.390)/;
const rejectedByBlacklistTest = /^(100\.100\.701|800\.[32])/;
const rejectedByRiskValidationTest = /^(800\.1[123456]0)/;
const rejectedByConfigValidationTest = /^(600\.[23]|500\.[12]|800\.121)/;
const rejectedByRegValidationTest = /^(100\.[13]50)/;
const rejectedByJobValidationTest = /^(100\.250|100\.360)/;
const rejectedByReferenceValidationTest = /^(700\.[1345][05]0)/;
const rejectedByFormatValidationTest = /^(200\.[123]|100\.[53][07]|800\.900|100\.[69]00\.500)/;
const rejectedByAddressValidationTest = /^(100\.800)/;
const rejectedByContactValidationTest = /^(100\.[97]00)/;
const rejectedByAccountValidationTest = /^(100\.100|100.2[01])/;
const rejectedByAmountValidationTest = /^(100\.55)/;
const rejectedByRiskManagementTest = /^(100\.380\.[23]|100\.380\.101)/;
// Chargeback
export const chargeBackTest = /^(000\.100\.2)/;

export const failChecks = [
	rejectedByRiskTest,
	rejectedByBankTest,
	rejectedByComErrorTest,
	rejectedBySystemErrorTest,
	rejectedByAsyncWorkflowTest,
	rejectedByExternalRiskTest,
	rejectedByAddressTest,
	rejectedBy3DSecureTest,
	rejectedByBlacklistTest,
	rejectedByRiskValidationTest,
	rejectedByConfigValidationTest,
	rejectedByRegValidationTest,
	rejectedByJobValidationTest,
	rejectedByReferenceValidationTest,
	rejectedByFormatValidationTest,
	rejectedByAddressValidationTest,
	rejectedByContactValidationTest,
	rejectedByAccountValidationTest,
	rejectedByAmountValidationTest,
	rejectedByRiskManagementTest,
];

export default class PayUtils {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	getCheckoutId = ({
		callback,
		amountInCents,
		paymentOptions,
	}: {
		callback: (jsonRes: ICheckoutResponse) => void;
		amountInCents: number;
		paymentOptions: string[] | [];
	}) => {
		const path = '/v1/checkouts';
		const coreRequest: IPaymentRequest = {
			entityId: this.app.get('oppwa').entityId,
			amount: Dinero({
				amount: Math.round(Number(amountInCents)),
				precision: 2,
			})
				.toUnit()
				.toFixed(2),
			currency: this.app.get('oppwa').currency as 'ZAR',
			paymentType: 'DB',
			createRegistration: true,
		};

		if (process.env.NODE_ENV === 'development') {
			coreRequest.testMode = 'EXTERNAL';
		}

		if (
			Array.isArray(paymentOptions) === true &&
			paymentOptions.length > 0
		) {
			paymentOptions.forEach((paymentOption, i) => {
				// #TODO
				// @ts-ignore
				coreRequest[`registrations[${i}].id`] = paymentOption;
			});
		}

		// #TODO
		// @ts-ignore
		const data = querystring.stringify(coreRequest);

		const options = {
			port: this.app.get('oppwa').port,
			host: this.app.get('oppwa').host,
			path,
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': data.length,
				Authorization: this.app.get('oppwa').token,
			},
		};

		const postRequest = https.request(options, (res) => {
			res.setEncoding('utf8');

			let dataPostRequest = '';
			res.on('data', (chunk) => {
				if (chunk != null) {
					dataPostRequest += chunk;
				}
			});

			res.on('end', () => {
				try {
					const jsonRes: ICheckoutResponse = JSON.parse(
						dataPostRequest,
					);
					return callback(jsonRes);
				} catch (e) {
					logger.debug('getCheckoutId Err::', data, e);
					throw new BadRequest('Could not get checkout ID');
				}
			});
		});
		postRequest.write(data);
		postRequest.end();
	};

	getStatus = (
		callback: (jsonRes: ICheckoutResponse) => void,
		id: string,
		resourcePath: string,
	) => {
		let path = resourcePath || `/v1/checkouts/${id}/payment`;
		path += `?entityId=${this.app.get('oppwa').entityId}`;

		const options = {
			port: this.app.get('oppwa').port,
			host: this.app.get('oppwa').host,
			path,
			method: 'GET',
			headers: {
				Authorization: this.app.get('oppwa').token,
			},
		};

		const postRequest = https.request(options, (res) => {
			res.setEncoding('utf8');

			let data = '';

			res.on('data', (chunk) => {
				if (chunk != null) {
					data += chunk;
				}
			});

			res.on('end', () => {
				try {
					const jsonRes: IPaymentStatusResponse = JSON.parse(data);
					return callback(jsonRes);
				} catch (e) {
					logger.debug('getStatus Err::', e);
					throw new BadRequest('Could not get payment status');
				}
			});
		});
		postRequest.end();
	};

	getResult = (code: string) => {
		return { success: successTest.test(code) === true };
	};

	goToCheckout = async ({
		amountInCents,
		user,
	}: {
		amountInCents: number;
		user: IUserRead;
	}) => {
		try {
			const response: Promise<ICheckoutResponse> = new Promise(
				(resolve) => {
					this.getCheckoutId({
						callback: (responseData) => {
							resolve(responseData);
						},
						amountInCents,
						paymentOptions: user.paymentOptions,
					});
				},
			);
			return response;
		} catch (e) {
			Sentry.captureException(e);
			throw new errors.Forbidden(
				new Error('Payment page could not be processed'),
			);
		}
	};

	getPaymentStatus = async ({
		id,
		resourcePath,
	}: {
		id: string;
		resourcePath: string;
	}) => {
		try {
			const response: Promise<IPaymentStatusResponse> = new Promise(
				(resolve) => {
					this.getStatus(
						(responseData) => {
							resolve(responseData);
						},
						id,
						resourcePath,
					);
				},
			);
			const { result } = await response;
			if (this.getResult(result.code).success !== true) {
				Sentry.captureException(new Error(JSON.stringify(result)));
			}
			return response;
		} catch (err) {
			Sentry.captureException(err);
			throw new errors.Forbidden(
				new Error('Could not retrieve payment status'),
			);
		}
	};
}
