import { Params, Id } from '@feathersjs/feathers';
import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IPaymentWrite } from '@app/models/payments/model';
import PayUtils from './payments.utils';
import MailUtils from '../mailer/mailer.utils';
import { IApplication } from '@app/declarations';

export class Payments extends Service {
	app: IApplication;

	utils: PayUtils;

	mailUtils: MailUtils;

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	emit: any;

	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
		this.app = app;
		this.utils = new PayUtils(app);
		this.mailUtils = new MailUtils(app);
	}

	async update(id: null | Id, data: IPaymentWrite, params?: Params) {
		return super.update(id as Id, data, params);
	}
}
