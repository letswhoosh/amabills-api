// Initializes the `payments` service on path `/payments`
import { ServiceAddons } from '@feathersjs/feathers';
import { NextFunction, Request, Response } from 'express';
import { IApplication } from '@app/declarations';
import createModel from '@app/models/payments/model';
import { Payments } from './payments.class';
import hooks from './payments.hooks';
import PaymentActions from './actions';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		payments: Payments & ServiceAddons<any>;
	}
}

const updateSecurityPolicy =
	(app: IApplication) =>
	(request: Request, response: Response, next: NextFunction) => {
		response.setHeader('X-Frame-Options', `ALLOW-FROM ${app.get('ui')}`);
		response.setHeader(
			'Content-Security-Policy',
			`'self' ${app.get('ui')}`,
		);
		next();
	};

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	const options = {
		events: ['status'], // custom events
		Model: createModel(app),
		paginate: app.get('paginate'),
	};
	const paymentService = new Payments(options, app);

	// Custom payment service endpoints
	app.use(
		'/payments/payment-form',
		updateSecurityPolicy(app),
		new PaymentActions.GetPaymentForm(app),
		(request: Request, response: Response, next: NextFunction) => {
			try {
				response.render('pay', response.data.data[0]);
			} catch (err) {
				console.log(err);
				next();
			}
		},
	).hooks({
		before: {
			find: [],
		},
	});

	app.use(
		'/payments/send-status-update',
		new PaymentActions.SendStatusUpdate(app),
	);
	// app.use(
	// 	'/payments/alternative-payment',
	// 	new PaymentActions.AlternativePayment(app),
	// );

	app.use(
		'/payments/process-card',
		new PaymentActions.ProcessCardPayment(app),
	);

	app.use('/payments/payment-key', new PaymentActions.GetPaymentKey(app));

	app.use('/payments', paymentService);

	// Get our initialized service so that we can register hooks
	const service = app.service('payments');

	service.hooks(hooks);
};
