import * as authentication from '@feathersjs/authentication';
import { BadRequest } from '@feathersjs/errors';
import { HookContext } from '@feathersjs/feathers';
import { Validator } from 'jsonschema';
import {
	SuccessAction,
	successActionSchema,
	successEntitySchema,
	TSuccessEntity,
} from '../../models/payments/schemas';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export const validateSuccessAction = async (context: HookContext) => {
	const { data } = context as {
		data: { successAction: SuccessAction };
	};

	if (data.successAction !== null) {
		const v = new Validator();
		v.addSchema(successActionSchema);
		const result = v.validate(data.successAction, successActionSchema);
		if (!result.valid) {
			throw new BadRequest('Invalid payment success action');
		}
	}

	return context;
};

export default {
	before: {
		all: [authenticate('jwt')],
		find: [],
		get: [],
		create: [validateSuccessAction],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
