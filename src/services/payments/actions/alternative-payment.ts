// import { IApplication } from '@app/declarations';
// import { BadRequest } from '@feathersjs/errors';
// import { IPurchaseRead } from '@app/models/purchases.model';
// import { Paginated } from '@feathersjs/feathers';
// import { v4 } from 'uuid';
// import Dinero from 'dinero.js';
// import R from 'ramda';
// import logger from '../../../logger';
// import PayUtils from '../payments.utils';
// import {
// 	IPaymentCurrency,
// 	IPaymentRead,
// 	IPaymentStatus,
// 	IPaymentWrite,
// } from '../../../models/payments/model';

// class AlternativePayment {
// 	app: IApplication;

// 	utils: PayUtils;

// 	constructor(app: IApplication) {
// 		this.utils = new PayUtils(app);

// 		this.app = app;
// 	}

// 	async find(params: {
// 		query: {
// 			voucherId: number;
// 			amountInCents: number;
// 			userId: string;
// 			transactionRef: string;
// 			creditCardPaymentInCents: number;
// 			voucherPaymentInCents: number;
// 			walletBalancePaymentInCents: number;
// 			addLoyalty: boolean;
// 			service: 'bills' | 'prepaid';
// 		};
// 	}) {
// 		const {
// 			voucherId,
// 			userId,
// 			walletBalancePaymentInCents,
// 			transactionRef,
// 			creditCardPaymentInCents,
// 			voucherPaymentInCents,
// 			amountInCents,
// 			addLoyalty,
// 			service,
// 		} = params.query;

// 		let result = {};

// 		try {
// 			//Validator
// 			let validateVoucher = true;
// 			let validatePoints = false;

// 			// Validate the voucher if it exist
// 			if (voucherId) {
// 				const { valid } = await this.app
// 					.service('voucher-validate')
// 					.validateVoucher(`${voucherId}`);

// 				validateVoucher = valid;
// 			}

// 			// Fetch user making payment request
// 			if (userId) {
// 				const user = await this.app.service('users').get(userId);

// 				// Create Payment
// 				const payment: IPaymentWrite = {
// 					userId,
// 					checkoutId: v4(),
// 					amountInCents,
// 					description: '',
// 					status: IPaymentStatus.Processing,
// 					currency: IPaymentCurrency.ZAR,
// 					reference: '',
// 				};

// 				const paymentCreated: IPaymentRead = await this.app
// 					.service('payments')
// 					.create(payment);

// 				let transactions = {};
// 				let currentBalanceInCents = 0;

// 				// Validate the existing of points
// 				if (user.vendorId) {
// 					transactions = await this.app
// 						.service('vendor-transactions')
// 						.find({
// 							query: {
// 								vendorId: user.vendorId,
// 								$sort: {
// 									createdAt: -1,
// 								},
// 							},
// 						});

// 					currentBalanceInCents = R.pathOr(
// 						{},
// 						['data', 0, 'currentBalanceInCents'],
// 						transactions,
// 					) as number;
// 				} else {
// 					transactions = await this.app
// 						.service('loyalty-points')
// 						.find({
// 							query: {
// 								userId,
// 								$sort: {
// 									createdAt: -1,
// 								},
// 							},
// 						});
// 					currentBalanceInCents = R.pathOr(
// 						0,
// 						['data', 0, 'currentBalanceInCents'],
// 						transactions,
// 					) as number;
// 				}

// 				validatePoints =
// 					walletBalancePaymentInCents <= currentBalanceInCents;

// 				if (
// 					walletBalancePaymentInCents > 0 &&
// 					currentBalanceInCents < 10000
// 				) {
// 					throw new BadRequest(
// 						new Error('Points are less than the threshold'),
// 					);
// 				}

// 				const totalfromVoucherAndPoints = Dinero({
// 					amount: voucherPaymentInCents,
// 				}).add(
// 					Dinero({
// 						amount: walletBalancePaymentInCents,
// 					}),
// 				);

// 				const success =
// 					validatePoints &&
// 					validateVoucher &&
// 					totalfromVoucherAndPoints.getAmount() >= amountInCents;

// 				// Success validation
// 				if (success) {
// 					const { data: purchases } = (await this.app
// 						.service('purchases')
// 						.find({
// 							query: {
// 								transactionRef,
// 							},
// 						})) as Paginated<IPurchaseRead>;

// 					if (purchases && purchases.length > 0) {
// 						const purchase = purchases[0];

// 						await this.app
// 							.service(
// 								`vps-${service}-vend` as
// 									| 'vps-prepaid-vend'
// 									| 'vps-bills-vend',
// 							)
// 							.create({
// 								transactionRef,
// 								userId: Number(purchase.userId),
// 								sessionID: purchase.prevendResponse.sessionID
// 							});

// 						const data = {
// 							transactionRef,
// 							creditCardPaymentInCents,
// 							voucherPaymentInCents,
// 							walletBalancePaymentInCents,
// 							addLoyalty,
// 						};

// 						result = await this.app
// 							.service('purchases')
// 							.patch(purchase.id, {
// 								...data,
// 								paid: true,
// 								userId: Number(purchase.userId),
// 							});

// 						// Update payment as successful
// 						const updatedPayment: IPaymentRead = {
// 							...paymentCreated,
// 							status: IPaymentStatus.Successful,
// 							successEntity: {
// 								entity: 'purchases',
// 								id: String(purchase.id),
// 							},
// 							paymentComposition: {
// 								creditCardPaymentInCents,
// 								voucherPaymentInCents,
// 								walletBalancePaymentInCents,
// 							},
// 						};

// 						await this.app
// 							.service('payments')
// 							.patch(paymentCreated.id, updatedPayment);
// 					} else {
// 						throw new BadRequest(
// 							'Transaction reference is invalid',
// 						);
// 					}
// 				} else {
// 					// Update payment with failure state & reason
// 					const updatedPayment: IPaymentRead = {
// 						...paymentCreated,
// 						status: IPaymentStatus.Failed,
// 					};

// 					await this.app
// 						.service('payments')
// 						.patch(paymentCreated.id, updatedPayment);

// 					throw new BadRequest(
// 						!validateVoucher
// 							? 'Voucher is invalid'
// 							: !validatePoints
// 							? 'Insufficient Points'
// 							: 'Transaction is invalid',
// 					);
// 				}
// 			}

// 			return result;
// 		} catch (err) {
// 			logger.error('Payment service Error::alternative-payment ', err);
// 			throw new BadRequest(err);
// 		}
// 	}
// }

// export default AlternativePayment;
