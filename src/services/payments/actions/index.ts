import GetPaymentForm from './get-payment-form';
import SendStatusUpdate from './send-status-update';
import ProcessCardPayment from './process-card-payment';
import GetPaymentKey from './get-payment-key';

const actions = {
	GetPaymentForm,
	SendStatusUpdate,
	ProcessCardPayment,
	GetPaymentKey
};

export default actions;
