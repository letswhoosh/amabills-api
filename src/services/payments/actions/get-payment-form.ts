import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import { Validator } from 'jsonschema';
import {
	IPaymentWrite,
	IPaymentStatus,
	IPaymentCurrency,
} from '@app/models/payments/model';
import { successActionSchema } from '@app/models/payments/schemas';
import logger from '@app/logger';
import PayUtils from '../payments.utils';

class GetPaymentForm {
	app: IApplication;

	utils: PayUtils;

	constructor(app: IApplication) {
		this.utils = new PayUtils(app);

		this.app = app;
	}

	async find(params: {
		query: {
			amountInCents: number;
			userId: string;
			reference: string;
			description: string;
			successAction: string;
		};
	}) {
		const paymentService = this.app.service('payments');

		try {
			const schema = {
				id: '/SimplePerson',
				type: 'object',
				required: [
					'amountInCents',
					'userId',
					'reference',
					'description',
				],
				properties: {
					amountInCents: {
						type: ['number', 'string'],
					},
					userId: {
						type: ['string', 'number'],
					},
					reference: {
						type: ['string'],
					},
					description: {
						type: ['string'],
					},
					successAction: { $ref: '/SuccessActionSchema' },
				},
			};

			const v = new Validator();
			v.addSchema(successActionSchema);
			v.addSchema(schema);
			const result = v.validate(
				{
					...params.query,
					successAction: JSON.parse(params.query.successAction),
				},
				schema,
			);

			if (!result.valid) {
				console.log('Validation Error', result.errors);
				throw new BadRequest('Invalid payment');
			}

			const {
				amountInCents,
				userId,
				reference,
				description,
				successAction,
			} = params.query;

			// Create payment and render payment form
			const root = this.app.get('apiUrl');

			paymentService.emit('status', {
				userId,
				status: 'Initializing payment process...',
				step: 1,
			});

			// Fetch user making payment request
			const user = await this.app.service('users').get(userId);

			paymentService.emit('status', {
				userId,
				status: 'Connecting to payment service...',
				step: 2,
			});

			// Fetch the form payment details
			const checkoutResponse = await paymentService.utils.goToCheckout({
				amountInCents,
				user,
			});

			// Create payment
			const payment: IPaymentWrite = {
				userId,
				checkoutId: checkoutResponse.id, // what we use to link back to the payment when processing response
				amountInCents,
				description,
				status: IPaymentStatus.Processing,
				currency: IPaymentCurrency.ZAR,
				reference,
				successAction: JSON.parse(successAction),
			};

			await paymentService.create(payment);

			paymentService.emit('status', {
				userId,
				status: 'Preparing payment form...',
				step: 3,
			});

			return {
				data: [
					{
						paymentUrl: this.app.get('oppwa').host,
						checkoutId: checkoutResponse.id,
						redirectUrl: `${root}/payments/payment-result?userId=${user.id}`,
						reactPath: `${root}/components.prod/payment.js`,
						apiUrl: this.app.get('apiUrl'),
						userId,
					},
				],
			};
		} catch (err) {
			logger.error('Payment service Error::payment-form', err);

			paymentService.emit('status', {
				userId: params.query.userId,
				result: {
					success: false,
					description: err.message,
				},
			});

			throw new BadRequest(err);
		}
	}
}

export default GetPaymentForm;
