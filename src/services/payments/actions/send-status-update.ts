import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import Joi from 'joi';
import logger from '@app/logger';
import PayUtils from '../payments.utils';
import { validateHttpRequest } from '@app/services/utils';

class SendStatusUpdate {
	app: IApplication;

	utils: PayUtils;

	constructor(app: IApplication) {
		this.utils = new PayUtils(app);

		this.app = app;
	}

	async find(params: {
		query: { status: string; step: number; userId: string };
	}) {
		try {
			// Validate request query
			const schema = Joi.object().keys({
				status: Joi.string().required().label('Status'),
				step: Joi.number().required().label('Step'),
			});

			const validationResult = validateHttpRequest({
				schema,
				value: params.query,
				options: {
					allowUnknown: true,
				},
			});

			if (!validationResult.valid) {
				throw new BadRequest(validationResult.errors);
			}

			const { status, step, userId } = params.query;

			this.app.service('payments').emit('status', {
				userId,
				status,
				step,
			});

			return {
				data: [
					{
						message: 'Status update has been sent',
					},
				],
				meta: {
					code: 204,
				},
			};
		} catch (err) {
			logger.error(`Payment service Error::send-status-update ${err}`);
			this.app.service('payments').emit('status', {
				userId: params.query.userId,
				result: {
					success: false,
					description: err.message,
				},
			});
			throw new BadRequest(err);
		}
	}
}

export default SendStatusUpdate;
