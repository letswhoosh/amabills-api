import { Paginated } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { BadRequest } from '@feathersjs/errors';
import logger from '../../../logger';
import { IPaymentStatus, IPaymentRead } from '@app/models/payments/model';
import PayUtils from '../payments.utils';

class ProcessPayment {
	app: IApplication;

	utils: PayUtils;

	constructor(app: IApplication) {
		this.utils = new PayUtils(app);

		this.app = app;
	}

	async create(data: { transactionRef: string; status: IPaymentStatus }) {
		try {
			const { data: payments, total } = (await this.app
				.service('payments')
				.find({
					query: {
						reference: data.transactionRef,
					},
				})) as Paginated<IPaymentRead>;
			if (total < 1) {
				throw new BadRequest('Payment not found');
			}
			if (payments[0].successActionStatus === IPaymentStatus.Successful) {
				return { creditedBefore: true };
			}
			const updatedPayment = (await this.app
				.service('payments')
				.patch(payments[0].id, {
					status: data.status,
					successActionStatus: IPaymentStatus.Processing,
				})) as IPaymentRead;
			if (updatedPayment) {
				if (updatedPayment.status === IPaymentStatus.Successful) {
					const purchaseResult = await this.app
						.service(updatedPayment.successAction.service)
						.create({
							transactionId: updatedPayment.checkoutId,
							paid: true,
						});
					if (purchaseResult.purchased) {
						await this.app
							.service('payments')
							.patch(payments[0].id, {
								successActionStatus: IPaymentStatus.Successful,
							});
						IPaymentStatus.Processing;
						return purchaseResult;
					}
				}
				return {
					purchased: false,
				};
			}
			return {
				purchased: false,
			};
		} catch (err) {
			logger.error('Payment service Error::process-payment-result', err);
			throw new BadRequest(err);
		}
	}
}

export default ProcessPayment;
