import { IApplication } from '@app/declarations';
import axios from 'axios';
import qs from 'qs';
import { BadRequest } from '@feathersjs/errors';
import { Validator } from 'jsonschema';
import logger from '@app/logger';
import PayUtils from '../payments.utils';
import {
	IPaymentCurrency,
	IPaymentStatus,
	IPaymentWrite,
} from '@app/models/payments/model';

class GetPaymentKey {
	app: IApplication;

	utils: PayUtils;

	constructor(app: IApplication) {
		this.utils = new PayUtils(app);

		this.app = app;
	}

	async find(params: {
		query: {
			amount: string;
			transactionId: string;
			userId: string;
			purchaseType: 'vas-purchase' | 'rent-payment';
		};
	}) {
		try {
			const schema = {
				type: 'object',
				required: ['amount', 'transactionId'],
				properties: {
					amount: {
						type: ['number', 'string'],
					},
					transactionId: {
						type: ['string', 'string'],
					},
				},
			};

			const v = new Validator();
			v.addSchema(schema);
			const result = v.validate(
				{
					...params.query,
				},
				schema,
			);

			if (!result.valid) {
				throw new BadRequest('Invalid payment');
			}
			const amabillsUrl = 'https://amabills.co.za';
			const newDate = new Date();
			const merchantReference = `${
				newDate.getFullYear().toString().substring(2) +
				(newDate.getMonth() + 1).toString().slice(-2) +
				newDate.getDate().toString().slice(-2) +
				newDate.getHours().toString().slice(-2) +
				newDate.getMinutes().toString().slice(-2) +
				newDate.getSeconds().toString().slice(-2)
			}_${params.query.userId}_${
				process.env.NODE_ENV === 'production' ? 'prod' : 'test'
			}`;

			const data = {
				merchant_reference: merchantReference,
				amount: params.query.amount,
				success_url: amabillsUrl,
				error_url: amabillsUrl,
				cancel_url: amabillsUrl,
			};
			const payload = qs.stringify(data);
			const url = 'https://gateway.switch.tj/api/v1/payment-key';
			const response = await axios.post(url, payload, {
				auth: {
					username: 'WhooshInnovations',
					password: 'Shifusensei@1234',
				},
			});

			if (response.data) {
				const paymentData: IPaymentWrite = {
					reference: merchantReference,
					checkoutId: params.query.transactionId,
					currency: IPaymentCurrency.ZAR,
					userId: params.query.userId,
					amountInCents: Number(params.query.amount) * 100,
					description: 'card payment',
					status: IPaymentStatus.Processing,
					successAction: {
						service: `complete-${params.query.purchaseType}`,
						data: {
							transactionId: params.query.transactionId,
							paid: true,
						},
					},
					successActionStatus: IPaymentStatus.Pending,
				};
				await this.app.service('payments').create(paymentData);
				return { data: [response.data] };
			}
		} catch (err) {
			console.log(err);
			logger.error('Payment service Error::payment-key', err);
			throw new BadRequest('Error generating payment form');
		}
	}
}

export default GetPaymentKey;
