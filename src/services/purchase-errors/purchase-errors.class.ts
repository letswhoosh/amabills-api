import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '@app/declarations';

export class PurchaseErrors extends Service {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
	}
}
