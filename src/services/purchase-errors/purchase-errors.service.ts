// Initializes the `purchase-errors` service on path `/purchase-errors`
import { IApplication } from '@app/declarations';
import { PurchaseErrors } from './purchase-errors.class';
import createModel from '@app/models/purchase-errors.model';
import hooks from './purchase-errors.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/purchase-errors', new PurchaseErrors(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('purchase-errors');

	service.hooks(hooks);
};
