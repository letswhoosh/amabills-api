// Initializes the `scheduled` service on path `/scheduled`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { Scheduled } from './scheduled.class';
import createModel from '../../models/scheduled.model';
import hooks from './scheduled.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'scheduled': Scheduled & ServiceAddons<any>;
  }
}

export default function (app: IApplication): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/scheduled', new Scheduled(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('scheduled');

  service.hooks(hooks);
}
