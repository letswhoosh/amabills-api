/* eslint-disable no-param-reassign */
// @ts-ignore Need TS Typing
import dehydrate from 'feathers-sequelize/hooks/dehydrate';
import { ValidationErrorItem, ObjectSchema, ValidationOptions } from 'joi';
import { HookContext, Paginated } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import * as Sentry from '@sentry/node';
import { IAPIHookContext, IHookContext } from '@app/declarations';
import logger from '../logger';

export const shouldSequelizeModel = (context: HookContext) => {
	if (context.params.useSequelizeModel) {
		return context;
	}
	// Convert data back to a plain object
	return dehydrate()(context);
};

export const transformJoiErrors = (
	joiErrors: ValidationErrorItem[],
	errorTitle: string,
) => {
	const errorObject = joiErrors.reduce((acc, val) => {
		const newError = { [val.path[0]]: val.message };
		return { ...acc, ...newError };
	}, {});
	const transformedError = new BadRequest(errorTitle, errorObject);
	return transformedError;
};

export const validateHttpRequest = (params: {
	schema: ObjectSchema;
	value: Record<string, unknown>;
	errorTitle?: string;
	options?: ValidationOptions;
}) => {
	const { schema, value, errorTitle, options } = params;

	const validation = schema.validate(value, options);

	if (validation.error) {
		Sentry.captureException(validation.error);
		return {
			valid: false,
			errors: transformJoiErrors(
				validation.error.details,
				errorTitle || 'Validation error',
			),
		};
	}

	return { valid: true };
};

export const disablePagination = async (context: HookContext) => {
	if (context.params.query != null && context.params.query.$limit === -1) {
		context.params.paginate = false;
		delete context.params.query.$limit;
	}
	return context;
};

export const nonEntityServices = [
	'auth',
	'mailer',
	'authentication',
	'uploads',
	'cloudinary',
	'vps-prepaid-prevend',
	'vps-prepaid-vend',
	'vps-bills-prevend',
	'vps-bills-vend',
	'voucher-validate',
	'payments/alternative-payment',
];

export const attachData = async (context: HookContext) => {
	const { path, app, method } = context as IAPIHookContext;
	try {
		if (
			['create', 'update', 'patch', 'remove', 'get'].includes(method) &&
			!nonEntityServices.includes(path)
		) {
			if (
				Object.prototype.hasOwnProperty.call(app.service(path), 'find')
			) {
				// eslint-disable-next-line require-atomic-updates
				context.dispatch =
					context.dispatch != null
						? {
								data: context.dispatch,
						  }
						: {
								data: context.result,
						  };
			}
		}
		return context;
	} catch (e) {
		console.log('attachData:: Error', context.path, e);
		return context;
	}
};

export const attachMeta = async (context: IHookContext) => {
	const { path } = context as IAPIHookContext;
	try {
		if (context.method === 'find' && !nonEntityServices.includes(path)) {
			const { total, limit, skip, data } = context.result;

			// eslint-disable-next-line require-atomic-updates
			context.dispatch =
				context.dispatch != null
					? {
							data,
							meta: { total, limit, skip },
					  }
					: {
							data,
							meta: { total, limit, skip },
					  };
		}

		return context;
	} catch (e) {
		logger.debug('attachMeta:: Error', context.path, e);
		return context;
	}
};

export const returnFullObject = async (context: HookContext) => {
	try {
		if (
			['create', 'update', 'patch'].includes(context.method) &&
			!nonEntityServices.includes(context.path)
		) {
			const { result } = context;
			if (!result.length) {
				// eslint-disable-next-line require-atomic-updates
				context.dispatch = await context.app
					.service(context.path)
					.get(result.id);
			}
		}
	} catch (e) {
		// Do Nothing
	}

	return context;
};

export const generateCode = (n: number) => {
	const add = 1;
	let max = 12 - add;

	if (n > max) {
		return (
			module.exports.generateCode(max) +
			module.exports.generateCode(n - max)
		);
	}

	max = Math.pow(10, n + add); // eslint-disable-line no-restricted-properties
	const min = max / 10;
	const number = Math.floor(Math.random() * (max - min + 1)) + min;

	return `${number}`.substring(add);
};

export const roundToNearst5thCent = (number: number) => {
	return Number((Math.floor(number / 5) * 5).toFixed(2));
};

export const roundToNearst5thDecimal = (number: number) => {
	return Number((Math.floor(number * 20) / 20).toFixed(2));
};

export const generateTrackId = (prefix: string) => {
	const newDate = new Date();
	const batchId =
		prefix +
		newDate.getFullYear().toString().substring(2) +
		('0' + (newDate.getMonth() + 1).toString()).slice(-2) +
		('0' + newDate.getDate().toString()).slice(-2);
	return batchId;
};
