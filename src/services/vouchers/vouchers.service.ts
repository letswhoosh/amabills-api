// Initializes the `vouchers` service on path `/vouchers`
import { IApplication } from '@app/declarations';
import { Vouchers } from './vouchers.class';
import createModel from '@app/models/vouchers.model';
import hooks from './vouchers.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/vouchers', new Vouchers(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vouchers');

	service.hooks(hooks);
};
