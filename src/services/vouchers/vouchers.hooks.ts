import * as feathersAuthentication from '@feathersjs/authentication';
// @ts-ignore Need TS Typing
import hydrate from 'feathers-sequelize/hooks/hydrate';
import { shouldSequelizeModel } from '../utils';
import {
	attachAssociations,
	assignRelationships,
	generateVoucherId,
} from './vouchers.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [attachAssociations],
		get: [attachAssociations],
		create: [generateVoucherId],
		update: [],
		patch: [],
		remove: [],
	},

	after: {
		all: [
			// Make sure the password field is never sent to the client
			// Always must be the last hook
			shouldSequelizeModel,
		],
		find: [],
		get: [],
		create: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
		],
		update: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
		],
		patch: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
		],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
