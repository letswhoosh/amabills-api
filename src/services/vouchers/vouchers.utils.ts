/* eslint-disable no-param-reassign */
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import R from 'ramda';
import { shouldSequelizeModel } from '../utils';
import { IVoucherWrite } from '@app/models/vouchers.model';
import { BadRequest } from '@feathersjs/errors';
import { IAPIHookContext } from '@app/declarations';

interface Search {
	like: string;
}

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { users } = sequelize.models;
	const { Op } = Sequelize;

	// Search Params
	const $search = R.path<Search>(['params', 'query', '$search'], context);

	if (context.params.query && $search != null) {
		delete context.params.query.$search;
	}

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: users,
				as: 'issuedBy',
			},
			{
				model: users,
				as: 'issuedTo',
			},
		],
	};

	if ($search != null) {
		context.params.sequelize.where = {
			[Op.or]: [
				{
					'$issuedBy.firstName$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					'$issuedBy.lastName$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					'$issuedTo.firstName$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					'$issuedTo.lastName$': {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
				{
					voucherId: {
						[Op.iLike]: `%${$search.like}%`,
					},
				},
			],
		};
	}

	return context;
};

export const assignRelationships = async (context: HookContext) => {
	const { data, app, params, id } = context as IAPIHookContext;
	const newVoucher = context.result;
	const { issuedTo } = context.data;

	if (context.method === 'create' && context.params.user) {
		await newVoucher.setIssuedBy(context.params.user.id);
	}

	if (context.method === 'patch' && issuedTo != null) {
		await newVoucher.setIssuedTo(issuedTo);
	}

	return shouldSequelizeModel(context);
};

export const generateVoucherId = async (
	context: HookContext<IVoucherWrite>,
) => {
	if (context.data == null) {
		throw new BadRequest('Data was not provided');
	}

	context.data.voucherId = uuidv4();
	return context;
};
