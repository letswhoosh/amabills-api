import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { IApplication } from '@app/declarations';

export class Vouchers extends Service {
	constructor(options: Partial<SequelizeServiceOptions>, app: IApplication) {
		super(options);
	}
}
