/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import * as R from 'ramda';
import moment from 'moment';
import { IApplication } from '@app/declarations';

export class VoucherValidate implements Partial<ServiceMethods<any>> {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async find(params?: Params): Promise<{ isValid: boolean }> {
		const user = params != null ? params.user : null;
		const voucherId = R.path(['query', 'voucherId'], params) as
			| string
			| null;

		if (user == null) {
			throw new BadRequest(new Error('Valid user is required'));
		}

		if (voucherId == null) {
			throw new BadRequest(new Error('Voucher ID is required'));
		}

		const { valid, message } = await this.validateVoucher(voucherId);

		if (!valid) {
			throw new BadRequest(new Error(message));
		}

		return { isValid: valid };
	}

	validateVoucher = async (voucherId: string) => {
		try {
			const voucher = await this.app.service('vouchers').get(voucherId);

			if (voucher != null) {
				if (moment(voucher.expires).isBefore(new Date())) {
					return {
						valid: false,
						message: 'Voucher has expired',
					};
				}

				if (voucher.state !== 'available') {
					return {
						valid: false,
						message: 'Voucher has already been used',
					};
				}

				return { valid: true };
			}

			return {
				valid: false,
				message: 'Invalid Voucher',
			};
		} catch (err) {
			console.log('err', err);
			throw err;
		}
	};
}
