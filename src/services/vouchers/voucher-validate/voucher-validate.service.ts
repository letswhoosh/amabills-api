// Initializes the `vps-electricity-products` service on path `/vps-electricity-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { VoucherValidate } from './voucher-validate.class';
import hooks from './voucher-validate.hooks';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'voucher-validate': VoucherValidate & ServiceAddons<any>;
	}
}

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	app.use('/voucher-validate', new VoucherValidate(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('voucher-validate');

	service.hooks(hooks);
};
