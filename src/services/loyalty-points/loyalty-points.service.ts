// Initializes the `loyalty-points` service on path `/loyalty-points`
import { IApplication } from '@app/declarations';
import { LoyaltyPoints } from './loyalty-points.class';
import createModel from '@app/models/loyalty-points.model';
import hooks from './loyalty-points.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/loyalty-points', new LoyaltyPoints(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('loyalty-points');

	service.hooks(hooks);
};
