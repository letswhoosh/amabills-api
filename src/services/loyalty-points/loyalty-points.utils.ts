/* eslint-disable no-param-reassign */
import R from 'ramda';
import { BadRequest } from '@feathersjs/errors';
import {
	ILoyaltyPointsRead,
	ILoyaltyPointsWrite,
} from '@app/models/loyalty-points.model';
import { IAPIHookContext, IHookContext } from '@app/declarations';
import { Paginated } from '@feathersjs/feathers';
import {
	MARGIN_PERCENT,
	PROCESSING_FEE_PORTION,
	SERVICE_FEE_PORTION,
} from '@app/variables';
import app from '@app/app';
import { IUserRead } from '@app/models/users.model';
import Dinero from 'dinero.js';

const PREPAID_BONUS_PERCENTAGE = 5;

export const updateLoyaltyPoints = async (
	context: IHookContext<ILoyaltyPointsWrite>,
) => {
	const { app, data } = context as IAPIHookContext<ILoyaltyPointsWrite>;

	if (data == null) {
		throw new BadRequest('No data was provided');
	}

	const { userId, amountInCents } = data;

	if (amountInCents == null) {
		throw new BadRequest(new Error('Amount is required'));
	}

	const transactions = (await app.service('loyalty-points').find({
		query: {
			userId,
			$sort: {
				createdAt: -1,
			},
		},
	})) as Paginated<ILoyaltyPointsRead>;

	const { currentBalanceInCents } = R.pathOr(
		{},
		['data', 0],
		transactions,
	) as ILoyaltyPointsRead;

	const newBalance = Number(currentBalanceInCents) + Number(amountInCents);
	const type = Number(amountInCents) > 0 ? 'credit' : 'debit';
	if (currentBalanceInCents == null) {
		context.data = {
			...context.data,
			amountInCents,
			prevBalanceInCents: 0,
			currentBalanceInCents: Number(amountInCents),
			type,
		};
	} else {
		context.data = {
			...context.data,
			amountInCents,
			prevBalanceInCents: Number(currentBalanceInCents),
			currentBalanceInCents: Number(newBalance),
			type,
		};
	}

	return context;
};

export const awardCreditPoints = async (
	user: IUserRead,
	purchaseId: number,
	creditAmount: number,
	phoneNumber: string,
) => {
	const amountToPurchase = Dinero({
		amount: Math.round(creditAmount || 0),
	}).divide(1 + MARGIN_PERCENT);

	const fee = Dinero({
		amount: Math.round(creditAmount || 0),
	}).subtract(amountToPurchase);

	const serviceFeeInCents = fee.multiply(SERVICE_FEE_PORTION);
	const processingFeeInCents = fee.multiply(PROCESSING_FEE_PORTION);

	const margin = serviceFeeInCents.add(processingFeeInCents);

	const amountToCredit = margin.multiply(PREPAID_BONUS_PERCENTAGE / 100);

	if (amountToCredit.getAmount() > 0 && user.vendorId == null) {
		const newPoints = await app.service('loyalty-points').create({
			amountInCents: amountToCredit.getAmount(),
			purchaseId: purchaseId,
			userId: user.id,
		});
		if (newPoints) {
			const awardedAmount =
				Math.round(newPoints.amountInCents * 100) / 10000;
			const newPointsBalance =
				Math.round(newPoints.currentBalanceInCents * 100) / 10000;

			const messages = [
				{
					content: `Dear ${user.firstName}.You have been awarded R ${awardedAmount}
					 Your new amabills points balance is R ${newPointsBalance}
					 Please go amabills.com for more details. From AMABILLS`,
					destination: phoneNumber.replace('+', ''),
				},
			];

			await app.service('send-sms').create({
				messages,
			});
		}
	}
};
