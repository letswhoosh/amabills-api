/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods } from '@feathersjs/feathers';
import * as soap from 'soap';
import * as R from 'ramda';
import { IApplication } from '@app/declarations';
import VPSUtils from '../vps-utils';

export class VpsProducts implements Partial<ServiceMethods<any>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async find(params?: Params): Promise<any> {
		const user = params != null ? params.user : null;
		const productTypeCode = R.path(
			['query', 'productTypeCode'],
			params,
		) as string;

		const client = await this.vpsUtils.login();
		const productList: any = await this.getProductList(
			client,
			productTypeCode,
			user ? user.id : null,
		);

		return { data: productList };
	}

	getProductList = async (
		client: soap.Client,
		productTypeCode: string,
		userId: number | null,
	) => {
		return new Promise((resolve, reject) => {
			client.GetProductListByType(
				{
					productTypeCode,
				},
				(err: Error, result: any) => {
					if (err) {
						// eslint-disable-next-line no-console
						console.log('getProductList Error::', err);

						if (userId) {
							this.vpsUtils.sendVPSErrorEmail({
								message: err.message as string,
								userId,
							});
						}

						this.app.service('purchase-errors').create({
							source: 'vps',
							description:
								'Could not retrieve VPS municipalities',
							dataProvided: { productTypeCode },
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
						});

						reject(err);
					} else {
						const list = R.pathOr<any>(
							[],
							[
								'GetProductListByTypeResult',
								'ProductList',
								'ProductList',
							],
							result,
						);
						resolve(list);
					}
				},
			);
		});
	};
}
