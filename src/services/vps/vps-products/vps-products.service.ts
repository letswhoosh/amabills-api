// Initializes the `vps-products` service on path `/vps-electricity-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { VpsProducts } from './vps-products.class';
import hooks from './vps-products.hooks';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'vps-products': VpsProducts & ServiceAddons<any>;
	}
}

export default function (app: IApplication): void {
	// Initialize our service with any options it requires
	app.use('/vps-products', new VpsProducts(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vps-products');

	service.hooks(hooks);
}
