/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods } from '@feathersjs/feathers';
import * as R from 'ramda';
import { IApplication } from '../../../declarations';
import VPSUtils from '../vps-utils';

export class VpsProductTypes implements Partial<ServiceMethods<{}>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async find(params?: Params): Promise<{ data: any[] }> {
		const client = await this.vpsUtils.login();
		const user = params != null ? params.user : null;
		const productTypes = (await this.getProductTypes(
			client,
			user ? user.id : null,
		)) as any[];

		return { data: productTypes };
	}

	getProductTypes = async (client: any, userId: number | null) => {
		return new Promise((resolve, reject) => {
			client.GetProductTypeList({}, (err: Error, result: any) => {
				if (err) {
					// eslint-disable-next-line no-console
					console.log('getProductTypes Error::', err);
					this.app.service('purchase-errors').create({
						source: 'vps',
						description: 'Could not fetch list of VPS products',
						error: {
							name: err.name,
							message: err.message,
							stack: err.stack,
						},
					});

					if (userId) {
						this.vpsUtils.sendVPSErrorEmail({
							message: err.message as string,
							userId,
						});
					}

					reject(err);
				} else {
					const products = R.pathOr<any>(
						[],
						[
							'GetProductTypeListResult',
							'ProductTypeList',
							'ProductTypeList',
						],
						result,
					);
					resolve(products);
				}
			});
		});
	};
}
