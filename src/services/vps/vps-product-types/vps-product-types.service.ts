// Initializes the `vps-product-types` service on path `/vps-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../../declarations';
import { VpsProductTypes } from './vps-product-types.class';
import hooks from './vps-product-types.hooks';

// Add this service to the service type index
declare module '../../../declarations' {
	interface ServiceTypes {
		'vps-product-types': VpsProductTypes & ServiceAddons<any>;
	}
}

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	app.use('/vps-product-types', new VpsProductTypes(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vps-product-types');

	service.hooks(hooks);
};
