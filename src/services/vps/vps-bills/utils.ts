/* eslint-disable class-methods-use-this */
import { BadRequest } from '@feathersjs/errors';
import * as soap from 'soap';
import { IApplication } from '@app/declarations';
import VPSUtils from '../vps-utils';

export default class VpsBills {
	app: IApplication;

	vpsUtils: VPSUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
	}

	getPreVend = (
		client: soap.Client,
		data: {
			transactionRef: string;
			amount: number;
			productCode: string;
			accountNumber: string;
			userId?: number;
		},
	) => {
		return new Promise((resolve, reject) => {
			console.log('PREVEND', {
				transactionRef: data.transactionRef,
				productCode: data.productCode,
				accountNumber: data.accountNumber,
				amount: data.amount,
			});
			client.BillPaymentPreVend(
				{
					transactionRef: data.transactionRef,
					productCode: data.productCode,
					accountNumber: data.accountNumber,
					amount: data.amount,
				},
				(err: Error, result: any) => {
					console.log('Last Request', client.lastRequest);
					if (err) {
						this.app.service('purchase-errors').create({
							source: 'vps',
							description: 'Could not perform bills prevend',
							dataProvided: data,
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
							transactionRef: data.transactionRef,
						});
						if (err.message === 'ESOCKETTIMEDOUT') {
							reject(new Error('Bills client could not connect'));
						} else {
							reject(err);
						}
					} else {
						console.log('getPreVend Result::', result);
						const res = result.BillPaymentPreVendResult;
						if (
							Number(res.ResponseCode) === 0 &&
							res.ResponseMessage === 'Success'
						) {
							resolve(res);
						} else {
							const vpsError = this.vpsUtils.constructVpsError(
								res,
							);

							if (data.userId) {
								this.vpsUtils.sendVPSErrorEmail({
									message: vpsError.message as string,
									userId: data.userId,
								});
							}

							this.app.service('purchase-errors').create({
								source: 'vps',
								description:
									'Could not perform bills prevend due to error indicative response code from VPS',
								dataProvided: res,
								error: {
									name: 'getPreVend Result',
									message: vpsError.message,
								},
								transactionRef: data.transactionRef,
							});
							reject(new BadRequest(vpsError.message));
						}
					}
				},
				{ timeout: 50000 },
			);
		});
	};

	getVend = (
		client: soap.Client,
		data: {
			transactionRef: string;
			amount: number;
			productCode: string;
			accountNumber: string;
			userId: number;
			receiptNumber: string;
			acknowledgementType: string;
		},
	) => {
		return new Promise((resolve, reject) => {
			console.log('VEND', {
				transactionRef: data.transactionRef,
				productCode: data.productCode,
				accountNumber: data.accountNumber,
				amountPaid: data.amount,
				receiptNumber: data.receiptNumber,
				acknowledgementType: data.acknowledgementType,
			});
			client.BillPaymentVend(
				{
					transactionRef: data.transactionRef,
					productCode: data.productCode,
					accountNumber: data.accountNumber,
					amountPaid: data.amount,
					receiptNumber: data.receiptNumber,
					acknowledgementType: data.acknowledgementType,
				},
				(err: Error, result: any) => {
					if (err) {
						this.vpsUtils.sendVPSErrorEmail({
							message: err.message as string,
							userId: data.userId,
						});
						this.app.service('purchase-errors').create({
							source: 'vps',
							description: 'Could not perform bills vend',
							dataProvided: data,
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
							transactionRef: data.transactionRef,
						});
						if (err.message === 'ESOCKETTIMEDOUT') {
							reject(new Error('Bills client could not connect'));
						} else {
							reject(err);
						}
					} else {
						console.log('getVend Result::', result);
						const res = result.BillPaymentVendResult;
						if (Number(res.ResponseCode) === 0) {
							resolve({
								success: true,
								response: res,
							});
						} else {
							const vpsError = this.vpsUtils.constructVpsError(
								res,
							);
							this.vpsUtils.sendVPSErrorEmail({
								message: vpsError.message as string,
								userId: data.userId,
							});
							this.app.service('purchase-errors').create({
								source: 'vps',
								description:
									'Could not perform bills vend due to error indicative response code from VPS',
								dataProvided: res,
								error: {
									name: 'getPreVend Result',
									message: vpsError.message,
								},
								transactionRef: data.transactionRef,
							});
							reject(new BadRequest(vpsError.message));
						}
					}
				},
				{ timeout: 14000 },
			);
		});
	};

	getVendConfirm = (
		client: soap.Client,
		data: {
			transactionRef: string;
			userId: number;
			productCode: string;
		},
	) => {
		return new Promise((resolve, reject) => {
			client.BillPaymentVendConfirm(
				{
					transactionRef: data.transactionRef,
					productCode: data.productCode,
				},
				(err: Error, result: any) => {
					if (err) {
						this.vpsUtils.sendVPSErrorEmail({
							message: err.message as string,
							userId: data.userId,
						});
						this.app.service('purchase-errors').create({
							source: 'vps',
							description:
								'Could not perform bill payment vend confirm',
							dataProvided: data,
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
							transactionRef: data.transactionRef,
						});
						if (err.message === 'ESOCKETTIMEDOUT') {
							reject(
								new Error('Prepaid client could not connect'),
							);
						} else {
							reject(err);
						}
					} else {
						console.log('getVendConfirm Result::', result);
						const res = result.BillPaymentVendConfirmResult;
						if (Number(res.ResponseCode) === 0) {
							resolve({
								success: true,
								response: res,
							});
						} else {
							const vpsError = this.vpsUtils.constructVpsError(
								res,
							);
							this.vpsUtils.sendVPSErrorEmail({
								message: vpsError.message as string,
								userId: data.userId,
							});
							this.app.service('purchase-errors').create({
								source: 'vps',
								description:
									'Could not perform bill payment vend confirm due to error indicative response code from VPS',
								dataProvided: res,
								error: {
									name: 'getVendConfirm Result',
									message: vpsError.message,
								},
								transactionRef: data.transactionRef,
							});
							reject(new BadRequest(vpsError.message));
						}
					}
				},
				{ timeout: 14000 },
			);
		});
	};
}
