/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods, Paginated } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import { IApplication } from '@app/declarations';
import { IPurchaseRead } from '@app/models/purchases.model';
import { IUserRead } from '@app/models/users.model';
import Dinero from '@app/handleCurrency';
import { VAT_PERCENT } from '@app/variables';
import { roundToNearst5thDecimal } from '../../../utils';
import VPSUtils from '../../vps-utils';
import BillsUtils from '../utils';
import VendUtils from './perform-vend.utils';

interface Data {
	transactionRef: string;
	userId?: number;
}

export class PerformVend implements Partial<ServiceMethods<any>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	BillsUtils: BillsUtils;

	vendUtils: VendUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
		this.BillsUtils = new BillsUtils(app);
		this.vendUtils = new VendUtils(app);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: Data, params?: Params) {
		const user =
			params && params.user
				? (params.user as IUserRead)
				: data.userId != null
				? await this.app.service('users').get(data.userId)
				: null;

		if (user == null) {
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'No authenticated user was identifed to access the VPS bills vend API endpoint',
			});
			throw new BadRequest(new Error('Valid user is required'));
		}

		const result = this.purchaseMunicipalBillToken({
			...data,
			userId: user.id,
		});

		return result;
	}

	addPoints = async ({
		amountInCents,
		userId,
		purchaseId,
	}: {
		amountInCents: number;
		userId: number;
		purchaseId: number;
	}) => {
		const user = await this.app.service('users').get(userId);
		if (user.vendorId) {
			this.app.service('vendor-transactions').create({
				amountInCents,
				purchaseId,
				vendorId: user.vendorId,
			});
		} else {
			this.app.service('loyalty-points').create({
				amountInCents,
				purchaseId,
				userId,
			});
		}
	};

	purchaseMunicipalBillToken = async (data: Data & { userId: number }) => {
		let vendResponse;

		const purchase = await this.getTransaction(data.transactionRef);

		if (
			purchase.processingFeeInCents == null ||
			purchase.serviceFeeInCents == null
		) {
			throw new BadRequest('Purchase is missing Service fees');
		}

		const paidForBillsInCents = Dinero({
			amount: purchase.amountInCents,
		})
			.subtract(
				Dinero({ amount: Math.round(purchase.processingFeeInCents) }),
			)
			.subtract(
				Dinero({ amount: Math.round(purchase.serviceFeeInCents) }),
			);

		if (purchase.state === 'prevend') {
			const client = await this.vpsUtils.login();

			try {
				const { response }: any = await this.BillsUtils.getVend(
					client,
					{
						transactionRef: data.transactionRef,
						amount: roundToNearst5thDecimal(
							paidForBillsInCents.convertPrecision(2).toUnit(),
						),
						productCode: purchase.productCode,
						accountNumber: purchase.accountNumber as string,
						receiptNumber: purchase.prevendResponse.receiptNumber,
						acknowledgementType:
							purchase.prevendResponse.acknowledgementType,
						userId: data.userId,
					},
				);

				if (response.ResponseCode === 0) {
					await this.BillsUtils.getVendConfirm(client, {
						transactionRef: data.transactionRef,
						productCode: purchase.productCode,
						userId: data.userId,
					});
				}

				vendResponse = response;
			} catch (err) {
				throw err;
			}
		} else {
			throw new BadRequest(
				new Error('Transaction has already been paid'),
			);
		}

		const voucher = purchase.voucherId
			? await this.app.service('vouchers').get(purchase.voucherId)
			: null;

		const slipVariables = this.vendUtils.getSlipVariables(vendResponse);

		const vatInCents = paidForBillsInCents
			.multiply(VAT_PERCENT)
			.getAmount();

		try {
			const updatedPurchase = await this.app
				.service('purchases')
				.patch(purchase.id, {
					tokens: vendResponse.Tokens,
					vatInCents,
					vendResponse,
					state: 'vend',
					voucherId: voucher != null ? voucher.id : undefined,
					slipVariables: {
						title: 'amaBills Bills',
						vatReg: slipVariables.vatReg,
						merchantCode: slipVariables.merchantCode,
						orderNumber: slipVariables.orderNumber,
						accountNumber: slipVariables.accountNumber,
						address: slipVariables.address,
						tokenTitle: 'Bill Tokens',
						receiptNumber: slipVariables.receiptNumber,
						retailValue: slipVariables.retailValue,
						responseDateTime: slipVariables.responseDateTime,
						acknowledgmentType: slipVariables.acknowledgmentType,
						resultCode: slipVariables.resultCode,
						transactionStatus: slipVariables.transactionStatus,
						municipalityAccountNumber:
							slipVariables.municipalityAccountNumber,
						message: slipVariables.message,
						costs: {
							vat: slipVariables.vat,
							paymentFees: slipVariables.paymentFees,
							merchantFee: slipVariables.merchantFee,
							amountPaid: slipVariables.amountPaid,
							total: slipVariables.totalAmount,
							transactionAmount: slipVariables.transactionAmount,
						},
						utility: slipVariables.vatReg,
						printed: slipVariables.vatReg,
					},
				});

			return updatedPurchase;
		} catch (err) {
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'Could not update purchase following the VPS bills vend request',
				dataProvided: data,
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
				transactionRef: data.transactionRef,
			});

			throw err;
		}
	};

	getTransaction = async (transactionRef: string) => {
		const { data: purchases } = (await this.app
			.service('purchases')
			.find({ query: { transactionRef } })) as Paginated<IPurchaseRead>;

		if (purchases.length < 1) {
			throw new BadRequest(
				new Error('Transaction ref does not match any purchase'),
			);
		}

		return purchases[0] as IPurchaseRead;
	};
}
