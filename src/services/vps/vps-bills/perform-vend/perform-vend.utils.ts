/* eslint-disable class-methods-use-this */
import * as R from 'ramda';
import * as Sentry from '@sentry/node';
import { IApplication } from '@app/declarations';
import Dinero from '../../../../handleCurrency';

interface Net1Other {
	Description: string;
	Amount: number;
}

interface Slip {
	accountNumber: string;
	vatReg: string;
	merchantCode: string;
	orderNumber: string;
	retailValue: string;
	totalAmount: number;
	amountPaid: string;
	paymentFees: string;
	responseDateTime: string;
	acknowledgmentType: string;
	resultCode: string;
	transactionStatus: string;
	municipalityAccountNumber: string;
	address: string;
	message: string;
	receiptNumber: string;
	vat: number;
	utility: string;
	printed: string;
	merchantBalance: number;
	merchantFee: number;
	transactionAmount: number;
}

const vpsDictionary = {
	accountNumber: ['AccountNo'],
	vatReg: ['VATNumber'],
	merchantCode: ['VendorName', 'Utility'],
	orderNumber: ['OrderNumber'],
	retailValue: ['RetailValue'],
	totalAmount: ['TotalAmount'],
	amountPaid: ['AmountPaid'],
	paymentFees: ['PaymentFees'],
	responseDateTime: ['ResponseDateTime'],
	acknowledgmentType: ['AcknowledgmentType'],
	resultCode: ['ResultCode'],
	transactionStatus: ['TransactionStatus'],
	municipalityAccountNumber: ['MunicipalityAccountNumber'],
	address: ['Address'],
	message: ['Message'],
	receiptNumber: ['ReceiptNo', 'Token_Receipt_0', 'ReceiptNumber'],
	vat: ['Token_Tax_0', 'VATAmount'],
	utility: ['VendorName'],
	printed: ['DateTime', 'ServerDateTime', 'Token_Date_0'],
	merchantBalance: ['MerchantBalance'],
	merchantFee: ['MerchantFee'],
	transactionAmount: ['TransactionAmount'],
};

export default class VendUtils {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	returnInCents = (amount: number) => {
		return Dinero({
			amount: Math.floor(amount * 100),
		}).getAmount();
	};

	getSlipVariables = (vendResponse: any) => {
		try {
			const slip = Object.entries(vpsDictionary).reduce(
				(acc, [key, options]) => {
					let value = null as string | number | null;

					if (vendResponse && vendResponse.mResponse) {
						vendResponse = {
							...vendResponse.mResponse,
							...vendResponse,
						};
					}

					options.forEach((option) => {
						const option1 = vendResponse[option] as string;

						if (
							option1 != null &&
							option1 !== '' &&
							Number(option1) !== 0
						) {
							value = option1;
						}
					});

					type AmountKeys =
						| 'vat'
						| 'totalAmount'
						| 'merchantBalance'
						| 'merchantFee'
						| 'transactionAmount';

					const amountKeys = [
						'vat',
						'totalAmount',
						'merchantBalance',
						'merchantFee',
						'transactionAmount',
					] as AmountKeys[];

					if (amountKeys.includes(key as AmountKeys)) {
						acc[
							key as
								| 'vat'
								| 'totalAmount'
								| 'merchantBalance'
								| 'merchantFee'
								| 'transactionAmount'
						] =
							value === '' || value == null
								? 0
								: (value as number);
					} else {
						acc[
							key as keyof Omit<Slip, AmountKeys>
						] = value as string;
					}

					return acc;
				},
				{} as Slip,
			);

			slip.vat = this.returnInCents(Number(slip.vat));
			slip.totalAmount = this.returnInCents(Number(slip.totalAmount));
			slip.merchantBalance = this.returnInCents(
				Number(slip.merchantBalance),
			);
			slip.merchantFee = this.returnInCents(Number(slip.merchantFee));
			slip.transactionAmount = this.returnInCents(
				Number(slip.transactionAmount),
			);
			return slip;
		} catch (err) {
			this.app.service('purchase-errors').create({
				source: 'unclear',
				description: 'Could not construct VPS Bills slip',
				dataProvided: vendResponse,
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
			});

			throw err;
		}
	};
}
