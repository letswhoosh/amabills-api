/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import { v4 as uuidv4 } from 'uuid';
import { IApplication } from '@app/declarations';
import { IPurchaseRead, IPurchaseWrite } from '@app/models/purchases.model';
import Dinero from '@app/handleCurrency';
import {
	MARGIN_PERCENT,
	PROCESSING_FEE_PORTION,
	SERVICE_FEE_PORTION,
} from '@app/variables';
import VPSUtils from '../../vps-utils';
import {
	roundToNearst5thCent,
	roundToNearst5thDecimal,
} from '@app/services/utils';
import PrepaidUtils from '../utils';
import { IProductRead } from '@app/models/products.model';
import { ILookUpResponse } from '@app/services/kinektek-api/actions/lookUpProduct';

interface Data {
	productName: string;
	amountInCents: number;
	productCode: string;
	meterNumber: string;
	voucherId: number;
	productTypeId: string;
	contactNumber: string;
	email: string;
}

export class PerformPrevend implements Partial<ServiceMethods<any>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	prepaidUtils: PrepaidUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
		this.prepaidUtils = new PrepaidUtils(app);
	}

	async create(data: Data, params?: Params): Promise<IPurchaseRead> {
		const user = params != null ? params.user : null;
		if (user == null) {
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'No authenticated user was identifed to access the VPS prepaid prevend API endpoint',
			});
			throw new BadRequest(new Error('Valid user is required'));
		}

		const result = await this.validatePrepaidElectricityTokenPurchase({
			...data,
			userId: user.id,
		});
		console.log(result, "BBBBBBBBBBBBBBBBBBBBBB")

		return result;
	}

	validatePrepaidElectricityTokenPurchase = async (
		data: Data & { userId: number },
	) => {
		const transactionRef = uuidv4();

		const {
			amountForPurchaseInCents,
			serviceFeeInCents,
			processingFeeInCents,
		} = this.calculateAmount(data.amountInCents);
		const prevendResponse = (await this.prepaidUtils.getPreVend({
			transactionRef,
			amount: roundToNearst5thDecimal(
				Dinero({
					amount: Math.round(amountForPurchaseInCents),
					precision: 2,
				}).toUnit(),
			),
			meterNumber: data.meterNumber,
			userId: data.userId,
		})) as ILookUpResponse;

		const purchaseToCreate = {
			...data,
			productCode: prevendResponse.productID,
			amountInCents: roundToNearst5thCent(data.amountInCents),
			transactionRef,
			prevendResponse,
			serviceFeeInCents,
			processingFeeInCents,
			userId: data.userId,
		} as IPurchaseWrite;

		try {
			const newPurchase = await this.app
				.service('purchases')
				.create(purchaseToCreate);

			return newPurchase;
		} catch (error) {
			const err = error as Error;
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'Could not create new purchase following the VPS prepaid prevend request',
				dataProvided: purchaseToCreate,
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
				transactionRef,
			});

			console.log(err);
			throw err;
		}
	};

	calculateAmount = (totalPaidByUser: number) => {
		const amountForPurchase = Dinero({
			amount: Math.round(totalPaidByUser),
		}).divide(1 + MARGIN_PERCENT);
		const margin = Dinero({ amount: Math.round(totalPaidByUser) }).subtract(
			amountForPurchase,
		);
		const serviceFeeInCents = margin.multiply(SERVICE_FEE_PORTION);
		const processingFeeInCents = margin.multiply(PROCESSING_FEE_PORTION);

		return {
			totalPaidByUser,
			serviceFeeInCents: serviceFeeInCents.getAmount(),
			processingFeeInCents: processingFeeInCents.getAmount(),
			marginInCents: margin.getAmount(),
			amountForPurchaseInCents: roundToNearst5thCent(
				amountForPurchase.getAmount(),
			),
		};
	};
}
