// Initializes the `vps-electricity-products` service on path `/vps-electricity-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { PerformPrevend } from './perform-prevend.class';
import hooks from './perform-prevend.hooks';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'vps-prepaid-prevend': PerformPrevend & ServiceAddons<any>;
	}
}

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	app.use('/vps-prepaid-prevend', new PerformPrevend(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vps-prepaid-prevend');

	service.hooks(hooks);
};
