/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods, Paginated } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import { IApplication } from '@app/declarations';
import { IPurchaseRead } from '@app/models/purchases.model';
import { IUserRead } from '@app/models/users.model';
import { VAT_PERCENT } from '@app/variables';
import Dinero from '@app/handleCurrency';
import { roundToNearst5thDecimal } from '../../../utils';
import VPSUtils from '../../vps-utils';
import PrepaidUtils from '../utils';
import VendUtils from './perform-vend.utils';

interface Data {
	sessionID: string;
	transactionRef: string;
	userId?: number;
}

export class PerformPrevend implements Partial<ServiceMethods<any>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	prepaidUtils: PrepaidUtils;

	vendUtils: VendUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
		this.prepaidUtils = new PrepaidUtils(app);
		this.vendUtils = new VendUtils(app);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: Data, params?: Params) {
		const user =
			params && params.user
				? (params.user as IUserRead)
				: data.userId != null
				? await this.app.service('users').get(data.userId)
				: null;

		if (user == null) {
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'No authenticated user was identifed to access the VPS prepaid vend API endpoint',
			});
			throw new BadRequest(new Error('Valid user is required'));
		}

		const result = this.purchasePrepaidElectricityToken({
			...data,
			userId: user.id,
		});

		return result;
	}

	addPoints = async ({
		amountInCents,
		userId,
		purchaseId,
	}: {
		amountInCents: number;
		userId: number;
		purchaseId: number;
	}) => {
		const user = await this.app.service('users').get(userId);
		if (user.vendorId) {
			this.app.service('vendor-transactions').create({
				amountInCents,
				purchaseId,
				vendorId: user.vendorId,
			});
		} else {
			this.app.service('loyalty-points').create({
				amountInCents,
				purchaseId,
				userId,
			});
		}
	};

	purchasePrepaidElectricityToken = async (
		data: Data & { userId: number },
	) => {
		let vendResponse;

		const purchase = await this.getTransaction(data.transactionRef);

		if (
			purchase.processingFeeInCents == null ||
			purchase.serviceFeeInCents == null
		) {
			throw new BadRequest('Purchase is missing Service fees');
		}

		const paidForElectricityInCents = Dinero({
			amount: purchase.amountInCents,
		})
			.subtract(
				Dinero({ amount: Math.round(purchase.processingFeeInCents) }),
			)
			.subtract(
				Dinero({ amount: Math.round(purchase.serviceFeeInCents) }),
			);

		if (purchase.state === 'prevend') {
			try {
				const response = (await this.prepaidUtils.getVend({
					transactionRef: data.transactionRef,
					amount: purchase.amountInCents,
					productId: purchase.productCode,
					meterNumber: purchase.meterNumber as string,
					userId: data.userId,
					sessionID: data.sessionID,
				})) as { status: string };

				if (
					response.status === 'SUCCESS' ||
					response.status === 'PENDING'
				) {
					const responseb = await this.prepaidUtils.getVendConfirm({
						transactionRef: data.transactionRef,
						productCode: purchase.productCode,
						userId: data.userId,
						sessionID: data.sessionID,
					});
					vendResponse = responseb;
				}
			} catch (err) {
				throw err;
			}
		} else {
			throw new BadRequest(
				new Error('Transaction has already been paid'),
			);
		}

		const voucher = purchase.voucherId
			? await this.app.service('vouchers').get(purchase.voucherId)
			: null;

		const slipVariables = this.vendUtils.getSlipVariables(
			vendResponse as any,
		);

		const vatInCents = paidForElectricityInCents
			.multiply(VAT_PERCENT)
			.getAmount();

		try {
			const updatedPurchase = await this.app
				.service('purchases')
				.patch(purchase.id, {
					tokens: slipVariables.fbeCredToken,
					vatInCents,
					vendResponse,
					state: 'vend',
					voucherId: voucher != null ? voucher.id : undefined,
					slipVariables: {
						title: 'amaBills Prepaid',
						vatReg: slipVariables.vatReg,
						merchantCode: slipVariables.merchantCode,
						orderNumber: slipVariables.orderNumber,
						rrn: slipVariables.rrn,
						meterNumber: slipVariables.meterNumber,
						alg: slipVariables.alg,
						krn: slipVariables.krn,
						sgc: slipVariables.sgc,
						ti: slipVariables.ti,
						tt: slipVariables.tt,
						address: slipVariables.address,
						tokenTitle: 'Electricity Tokens',
						receiptNumber: slipVariables.receiptNumber,
						fbeCred: {
							amount: slipVariables.fbeCredAmount,
							units: slipVariables.fbeCredUnits,
							token: slipVariables.fbeCredToken,
						},
						saleCred: {
							amount: slipVariables.saleCredAmount,
							units: slipVariables.saleCredUnits,
							token: slipVariables.saleCredToken,
						},
						costs: {
							vat: vatInCents,
							vpsVat: Number(slipVariables.vat),
							processingFee: purchase.processingFeeInCents,
							serviceFee: purchase.serviceFeeInCents,
							userPaid: Number(purchase.amountInCents),
							total: paidForElectricityInCents.getAmount(),
						},
						utility: slipVariables.utility,
						printed: slipVariables.printed,
						cashier: '',
					},
				});

			return updatedPurchase;
		} catch (error) {
			const err = error as Error;
			this.app.service('purchase-errors').create({
				source: 'internal',
				description:
					'Could not update purchase following the VPS prepaid vend request',
				dataProvided: data,
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
				transactionRef: data.transactionRef,
			});

			throw err;
		}
	};

	getTransaction = async (transactionRef: string) => {
		const { data: purchases } = (await this.app
			.service('purchases')
			.find({ query: { transactionRef } })) as Paginated<IPurchaseRead>;

		if (purchases.length < 1) {
			throw new BadRequest(
				new Error('Transaction ref does not match any purchase'),
			);
		}
		return purchases[0] as IPurchaseRead;
	};
}
