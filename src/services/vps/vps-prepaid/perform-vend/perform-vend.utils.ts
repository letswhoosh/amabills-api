/* eslint-disable class-methods-use-this */
import * as R from 'ramda';
import * as Sentry from '@sentry/node';
import { IApplication } from '@app/declarations';
import Dinero from '@app/handleCurrency';

interface CustomerEntry {
	CustomerEntryName: string;
	CustomerEntryValue: string;
}

interface Slip {
	vatReg: string;
	merchantCode: string;
	orderNumber: string;
	rrn: string;
	meterNumber: string;
	alg: string;
	krn: string;
	sgc: string;
	ti: string;
	tt: string;
	address: string;
	receiptNumber: string;
	fbeCredAmount: number;
	fbeCredUnits: string;
	fbeCredToken: string;
	saleCredAmount: number;
	saleCredUnits: string;
	saleCredToken: string;
	vat: number;
	utility: string;
	printed: string;
}

const vpsDictionary = {
	vatReg: ['VATNumber'],
	merchantCode: ['VendorName', 'Utility'],
	orderNumber: ['OrderNumber'],
	rrn: ['ReferenceNum'],
	meterNumber: ['Meter'],
	alg: ['Alg'],
	krn: ['Krn'],
	sgc: ['Sgc'],
	ti: ['Ti'],
	tt: ['Tt'],
	address: ['Address'],
	receiptNumber: ['ReceiptNo', 'Token_Receipt_0', 'ReceiptNumber'],
	fbeCredAmount: ['Token_Amount_1'],
	fbeCredUnits: ['Token_Units_1'],
	fbeCredToken: ['Token_Number_1'],
	saleCredAmount: ['Token_Amount_0'],
	saleCredUnits: ['Token_Units_0'],
	saleCredToken: ['Token_Number_0'],
	vat: ['Token_Tax_0', 'VATAmount'],
	utility: ['VendorName'],
	printed: ['DateTime', 'ServerDateTime', 'Token_Date_0'],
};

export default class VendUtils {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	returnInCents = (amount: number) => {
		return Dinero({
			amount: Math.round(amount * 100),
		}).getAmount();
	};

	getSlipVariables = (vendResponse: {
		slipData: Array<{
			text: string;
			doubleHeight?: boolean;
			doubleWidth?: boolean;
		}>;
	}) => {
		try {
			const {slipData: data} = vendResponse
			console.log(0, JSON.stringify(vendResponse));
			const slip = {} as Slip;
			for (let x = 0; x < data?.length; x++) {
				const currentText = data?.[x]?.text?.replace(/ /g, '');
				const checkNumber = Number(currentText);
				if (Number.isInteger(checkNumber) && checkNumber !== 0) {
					slip.saleCredToken = data?.[x]?.text?.trim();
					slip.fbeCredToken = data?.[x]?.text?.trim();
				} else {
					const splited = data?.[x]?.text?.split(':');
					if (data?.[x]?.text?.includes('Units (kWh):')) {
						slip.saleCredUnits = splited?.[1]?.trim();
						slip.fbeCredUnits = splited?.[1]?.trim();
					}
					if (data?.[x]?.text?.includes('Tot.Elec:')) {
						slip.fbeCredAmount = parseInt(splited?.[1]?.trim());
					}
					if (data?.[x]?.text?.includes('Tot.VAT:')) {
						slip.vat = parseInt(splited?.[1]?.trim());
					}
					if (data?.[x]?.text?.includes('TOTAL:')) {
						slip.saleCredAmount = parseInt(splited?.[1]?.trim());
					}
					if (data?.[x]?.text?.includes('Meter:')) {
						slip.meterNumber = splited?.[1]?.trim();
					}
				}
			}
			console.log(3, slip);
			return slip;
		} catch (error) {
			const err = error as Error
			this.app.service('purchase-errors').create({
				source: 'unclear',
				description: 'Could not construct VPS prepaid slip',
				dataProvided: vendResponse,
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
			});

			throw err;
		}
	};

	extractTokens = (vendResponse: any) => {
		const tokens = R.pathOr([], ['Tokens'], vendResponse) as any;
		const tokenKeys = Object.keys(tokens);
		let saleCred;
		let fbeCred;
		tokenKeys.forEach((n) => {
			const types = tokens[n];
			types.forEach((x: any) => {
				const type = R.path(['Description'], x);
				if (
					type === 'SaleCred' ||
					type === 'Normal Sale' ||
					type === 'Electricity Credit'
				) {
					saleCred = x;
				} else if (type === 'FbeCred') {
					fbeCred = x;
				}
			});
		});
		if (saleCred == null) {
			Sentry.captureException(new Error(JSON.stringify(vendResponse)));
		}
		return { saleCred, fbeCred };
	};

	findCustomerEntryValue = (name: string, vendResponse: any) => {
		const customerEntries = R.path(
			['CustomerEntries', 'CustomerEntry'],
			vendResponse,
		) as CustomerEntry[];
		if (customerEntries != null) {
			const entry = customerEntries.find(
				(n) => n.CustomerEntryName === name,
			);
			if (entry != null) {
				return entry.CustomerEntryValue;
			}
		}
		return '';
	};
}
