// Initializes the `vps-electricity-products` service on path `/vps-electricity-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { PerformPrevend } from './perform-vend.class';
import hooks from './perform-vend.hooks';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'vps-prepaid-vend': PerformPrevend & ServiceAddons<any>;
	}
}

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	app.use('/vps-prepaid-vend', new PerformPrevend(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vps-prepaid-vend');

	service.hooks(hooks);
};
