/* eslint-disable class-methods-use-this */
import { Params, ServiceMethods } from '@feathersjs/feathers';
import { v4 as uuidv4 } from 'uuid';
import { IApplication } from '@app/declarations';
import Dinero from '@app/handleCurrency';
import VPSUtils from '../../vps-utils';
import PrepaidUtils from '../utils';

interface Data {
	productCode: string;
	meterNumber: string;
}

export class PerformVerify implements Partial<ServiceMethods<any>> {
	app: IApplication;

	vpsUtils: VPSUtils;

	prepaidUtils: PrepaidUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
		this.prepaidUtils = new PrepaidUtils(app);
	}

	async create(data: Data, params?: Params): Promise<any> {
		const result = await this.verifyMeterTransaction(data);
		console.log({ result });
		return result;
	}

	verifyMeterTransaction = async (data: Data) => {
		const client = await this.vpsUtils.login();
		const transactionRef = uuidv4();

		try {
			return await this.prepaidUtils.getPreVend({
				transactionRef,
				amount: Dinero({
					amount: 1000,
					precision: 2,
				}).toUnit(),
				meterNumber: data.meterNumber,
			});
		} catch (err) {
			throw err;
		}
	};
}
