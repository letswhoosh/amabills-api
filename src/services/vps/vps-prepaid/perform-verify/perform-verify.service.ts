// Initializes the `vps-electricity-products` service on path `/vps-electricity-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '@app/declarations';
import { PerformVerify } from './perform-verify.class';
import hooks from './perform-verify.hooks';

// Add this service to the service type index
declare module '@app/declarations' {
	interface ServiceTypes {
		'vps-prepaid-verify': PerformVerify & ServiceAddons<any>;
	}
}

export default (app: IApplication) => {
	// Initialize our service with any options it requires
	app.use('/vps-prepaid-verify', new PerformVerify(app));

	// Get our initialized service so that we can register hooks
	const service = app.service('vps-prepaid-verify');

	service.hooks(hooks);
};
