/* eslint-disable class-methods-use-this */
import { BadRequest } from '@feathersjs/errors';
import * as soap from 'soap';
import { IApplication } from '@app/declarations';
import VPSUtils from '../vps-utils';
import { IProductRead } from '@app/models/products.model';
import { Paginated } from '@feathersjs/feathers';
import { ILookUpResponse } from '@app/services/kinektek-api/actions/lookUpProduct';
import { IKinekteProductDetails } from '@app/services/kinektek-api/actions/getProducts';
import { addOrUpdateProduct } from '@app/services/products/products.utils';

const kinektekFormatError =
	'Request validation failed - probable formatting error';
export default class VpsPrepaid {
	app: IApplication;

	vpsUtils: VPSUtils;
	constructor(app: IApplication) {
		this.app = app;
		this.vpsUtils = new VPSUtils(app);
	}

	getPreVend = (data: {
		transactionRef: string;
		amount: number;
		meterNumber: string;
		userId?: number;
	}) => {
		return new Promise(async (resolve, reject) => {
			try {
				const { meterNumber } = data;
				const productId = await this.getPrepaidProductId(true);
				const lookupResult = (await this.app
					.service('kinektek-api/lookup')
					.create({
						productID: productId,
						reference: meterNumber,
					})) as ILookUpResponse;
				if (lookupResult.message !== kinektekFormatError) {
					resolve({
						...lookupResult,
						productID: productId,
					});
				}
				const latestProductId = await this.getPrepaidProductId(false);
				const retryLookup = (await this.app
					.service('kinektek-api/lookup')
					.create({
						productID: productId,
						reference: meterNumber,
					})) as ILookUpResponse;
				//Add some code here to cater for other errors returned in response
				resolve({
					...retryLookup,
					productID: latestProductId,
				});
			} catch (error) {
				const err = error as Error;
				this.app.service('purchase-errors').create({
					source: 'vps',
					description: 'Could not perform prepaid prevend',
					dataProvided: data,
					error: {
						name: err.name,
						message: err.message,
						stack: err.stack,
					},
					transactionRef: data.transactionRef,
				});
				reject(err);
			}
		});
	};

	getVend = (data: {
		transactionRef: string;
		amount: number;
		productId: string;
		meterNumber: string;
		userId: number;
		sessionID: string;
	}) => {
		return new Promise(async (resolve, reject) => {
			console.log(data.amount, 'HHHHHHHHHHHHH');
			try {
				const purchaseData = {
					productID: data.productId,
					amount: data.amount,
					clientReference: data.transactionRef,
					tenderDetails: [
						{
							amount: data.amount,
							type: 'CASH',
						},
					],
					currency: '710',
					slipWidth: 60,
				};
				const response = (await this.app
					.service('kinektek-api/session-purchase')
					.create({
						sessionID: data.sessionID,
						purchaseData,
					})) as { status: string };
				if (
					response.status === 'PENDING' ||
					response.status === 'SUCCESS'
				) {
					resolve(response);
					return;
				}
				const message =
					'Could not perform prepaid vend due to error indicative response code from VAS';
				this.vpsUtils.sendVPSErrorEmail({
					message: message,
					userId: data.userId,
				});
				this.app.service('purchase-errors').create({
					source: 'vps',
					description: message,
					dataProvided: response,
					error: {
						name: 'getPreVend Result',
						message: message,
					},
					transactionRef: data.transactionRef,
				});
				reject(new BadRequest(message));
			} catch (error) {
				const err = error as Error;
				this.app.service('purchase-errors').create({
					source: 'vps',
					description: 'Could not perform prepaid vend',
					dataProvided: data,
					error: {
						name: err.name,
						message: err.message,
						stack: err.stack,
					},
					transactionRef: data.transactionRef,
				});
				this.vpsUtils.sendVPSErrorEmail({
					message: err.message as string,
					userId: data.userId,
				});
				if (err.message === 'ESOCKETTIMEDOUT') {
					reject(new Error('Prepaid client could not connect'));
					return;
				}
				reject(err);
			}
		});
	};
	getVendConfirm = (data: {
		transactionRef: string;
		userId: number;
		productCode: string;
		sessionID: string;
	}) => {
		return new Promise(async (resolve, reject) => {
			try {
				const response = (await this.app
					.service('kinektek-api/purchase-status')
					.get(data.sessionID)) as { status: string };
				if (response.status === 'SUCCESS') {
					this.app
						.service('kinektek-api/confirm-purchase')
						.create({ sessionID: data.sessionID });
					resolve(response);

					return;
				}
				const message =
					'Could not perform prepaid vend confirm due to error indicative response code from VAS';
				this.vpsUtils.sendVPSErrorEmail({
					message: message,
					userId: data.userId,
				});
				this.app.service('purchase-errors').create({
					source: 'vps',
					description: message,
					dataProvided: response,
					error: {
						name: 'getPreVend Result',
						message: message,
					},
					transactionRef: data.transactionRef,
				});
				reject(new BadRequest(message));
			} catch (error) {
				const err = error as Error;
				this.app.service('purchase-errors').create({
					source: 'vps',
					description: 'Could not perform prepaid vend confirm',
					dataProvided: data,
					error: {
						name: err.name,
						message: err.message,
						stack: err.stack,
					},
					transactionRef: data.transactionRef,
				});
				this.vpsUtils.sendVPSErrorEmail({
					message: err.message as string,
					userId: data.userId,
				});
				if (err.message === 'ESOCKETTIMEDOUT') {
					reject(new Error('Prepaid client could not connect'));
					return;
				}
				reject(err);
			}
		});
	};

	getPrepaidProductId = async (checkFromLocal: boolean) => {
		const productName = `prepaid_electricity`;
		if (checkFromLocal) {
			const { data: products, total } = (await this.app
				.service('products')
				.find({
					query: { productName: productName },
				})) as Paginated<IProductRead>;
			if (total > 0) {
				return products[0].productId;
			}
		}
		const prepaidElectricity = (await this.app
			.service('kinektek-api/products')
			.find({
				query: {
					productGroup: 'UTILITIES',
					productCategory: 'Electricity',
					productName: 'Prepaid',
				},
			})) as IKinekteProductDetails[];
		const electricityProduct = prepaidElectricity.find(
			(product) => product.name === 'Electricity',
		);
		if (electricityProduct) {
			addOrUpdateProduct(
				productName,
				electricityProduct.productID,
				this.app,
			);
		}
		return electricityProduct?.productID;
	};
}
