import * as soap from 'soap';
import * as R from 'ramda';
import { IApplication } from '@app/declarations';
import MailUtils from '../mailer/mailer.utils';
import { PaymentErrorTemplate } from '../mailer/templates';

export default class VPSUtils {
	app: IApplication;

	mailUtils: MailUtils;

	constructor(app: IApplication) {
		this.app = app;
		this.mailUtils = new MailUtils(app);
	}

	constructVpsError = (response: any) => {
		return {
			error: true,
			message: response.ResponseMessage,
		};
	};

	setupSoapClient = async (): Promise<soap.Client> => {
		const { url } = this.app.get('vps');
		return new Promise((resolve, reject) => {
			soap.createClient(
				url,
				{
					forceSoap12Headers: false,
					preserveWhitespace: true,
					escapeXML: false,
				},
				(err, client) => {
					if (err) {
						// eslint-disable-next-line no-console
						console.log('createClient Error', err);
						reject(err);
						this.app.service('purchase-errors').create({
							source: 'unclear',
							description: 'Could not setup VPS soap client',
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
						});
					} else {
						resolve(client);
					}
				},
			);
		});
	};

	login = async (): Promise<soap.Client> => {
		const client = await this.setupSoapClient();
		const { password, username } = this.app.get('vps');
		return new Promise((resolve, reject) => {
			client.Login(
				{
					userName: username,
					password,
				},
				(err: Error) => {
					if (err) {
						console.log('Login Error', err);
						this.app.service('purchase-errors').create({
							source: 'vps',
							description: 'Could not login to VPS',
							error: {
								name: err.name,
								message: err.message,
								stack: err.stack,
							},
						});
						reject(err);
					} else {
						const clientCookie = R.path(
							['lastResponseHeaders', 'set-cookie', '0'],
							client,
						) as string;
						if (clientCookie) {
							client.addHttpHeader('Cookie', clientCookie);
							resolve(client);
						} else {
							this.app.service('purchase-errors').create({
								source: 'unclear',
								description:
									'Could not retrieve authentication cookie for VPS login',
							});
							reject(new Error('No authentication cookie'));
						}
					}
				},
			);
		});
	};

	sendVPSErrorEmail = async ({
		message,
		userId,
	}: {
		message: string;
		userId: number;
	}) => {
		const user = await this.app.service('users').get(userId);
		const { noreply, payments } = this.app.get('emails');
		const { name: company, logo } = this.app.get('brand');
		const config = {
			to: payments,
			from: {
				name: 'amaBills VPS Payments',
				address: noreply,
			},
			subject: `${company} VPS Purchase Error`,
			html: PaymentErrorTemplate({
				company,
				logo,
				name: `${user.firstName} ${user.lastName}`,
				message,
				contactNumber: user.contactNumber,
				email: user.email,
				title: 'VPS Purchase Error',
			}),
		};
		this.mailUtils.RetrieveTransport().sendMail(config);
	};
}
