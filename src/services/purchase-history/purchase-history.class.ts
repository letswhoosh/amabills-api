import { IRentPaymentRead } from '@app/models/rent/rent-payments.model';
import { IVasPurchaseRead } from '@app/models/vas-purchases.model';
import {
	Id,
	NullableId,
	Paginated,
	Params,
	ServiceMethods,
} from '@feathersjs/feathers';
import { IApplication } from '../../declarations';

interface Data {
	id: Id;
	amount: number;
	transactionId: string;
	date: Date;
	accountNumber: string;
	productName: string;
}

interface ServiceOptions {}

export class PurchaseHistory implements ServiceMethods<Data> {
	app: IApplication;
	options: ServiceOptions;

	constructor(options: ServiceOptions = {}, app: IApplication) {
		this.options = options;
		this.app = app;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async find(params?: Params): Promise<Paginated<Data>> {
		const { productName } = params.query;
		if (productName === 'Rent') {
			const { total, data: purchases } = (await this.app
				.service('rent/rent-payments')
				.find({})) as Paginated<IRentPaymentRead>;
			if (total > 0) {
				const data = purchases.map((purchase) => {
					return {
						id: purchase.id,
						productName: 'Rent',
						date: purchase.createdAt,
						amount: purchase.amount,
						accountNumber: purchase.account,
						transactionId: purchase.transactionId,
					};
				});
				return { data: data, total: 1, limit: 0, skip: 0 };
			}
		} else {
			const { total, data: purchases } = (await this.app
				.service('vas-purchases')
				.find({})) as Paginated<IVasPurchaseRead>;
			if (total > 0) {
				const data = purchases.map((purchase) => {
					return {
						id: purchase.id,
						productName: purchase.productName,
						date: purchase.createdAt,
						amount: purchase.amount,
						accountNumber: purchase.lookupRequest.reference,
						transactionId: purchase.transactionId,
					};
				});
				return { data: data, total: 1, limit: 0, skip: 0 };
			}
		}
		return { data: [], total: 1, limit: 0, skip: 0 };
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async get(id: Id, params?: Params): Promise<Data> {
		throw new Error('Not allowed');
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async create(data: Data, params?: Params): Promise<Data> {
		throw new Error('Not allowed');
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
		throw new Error('Not allowed');
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
		throw new Error('Not allowed');
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async remove(id: NullableId, params?: Params): Promise<Data> {
		throw new Error('Not allowed');
	}
}
