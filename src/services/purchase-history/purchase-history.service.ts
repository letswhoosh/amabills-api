// Initializes the `purchase-history` service on path `/purchase-history`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { PurchaseHistory } from './purchase-history.class';
import hooks from './purchase-history.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'purchase-history': PurchaseHistory & ServiceAddons<any>;
  }
}

export default function (app: IApplication): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/purchase-history', new PurchaseHistory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('purchase-history');

  service.hooks(hooks);
}
