/* eslint-disable import/prefer-default-export */
/* eslint-disable no-param-reassign */
import { IHookContext } from '@app/declarations';
import { shouldSequelizeModel } from '../utils';

export const assignRelationship = async (context: IHookContext) => {
	const newSub = context.result;
	if (context.params.user) {
		newSub.setUser(context.params.user.id);
	}
	return shouldSequelizeModel(context);
};
