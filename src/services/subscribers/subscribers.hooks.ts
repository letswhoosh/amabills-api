import * as authentication from '@feathersjs/authentication';
// @ts-ignore
import hydrate from 'feathers-sequelize/hooks/hydrate';
import { shouldSequelizeModel } from '../utils';
import { assignRelationship } from './subscribers.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [authenticate('jwt')],
		patch: [authenticate('jwt')],
		remove: [authenticate('jwt')],
	},

	after: {
		all: [shouldSequelizeModel],
		find: [],
		get: [],
		create: [
			// Access sequelize model
			hydrate(),
			assignRelationship,
		],
		update: [],
		patch: [],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
