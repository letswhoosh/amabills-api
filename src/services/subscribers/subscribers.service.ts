// Initializes the `subscribers` service on path `/subscribers`
import { IApplication } from '@app/declarations';
import { Subscribers } from './subscribers.class';
import createModel from '@app/models/subscribers.model';
import hooks from './subscribers.hooks';

export default (app: IApplication) => {
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		Model,
		paginate,
	};

	// Initialize our service with any options it requires
	app.use('/subscribers', new Subscribers(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('subscribers');

	service.hooks(hooks);
};
