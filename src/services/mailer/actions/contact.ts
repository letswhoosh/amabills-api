// @ts-nocheck
import { IApplication } from '@app/declarations';
import { BadRequest, GeneralError } from '@feathersjs/errors';
import Joi from 'joi';
import logger from '@app/logger';
import MailUtils from '../mailer.utils';
import { contactTemplate, inTouchTemplate } from '../templates';

class Contact {
	app: IApplication;

	utils: MailUtils;

	constructor(app: IApplication) {
		this.utils = new MailUtils(app);

		this.app = app;
	}

	async create(data: {
		email: string;
		contactNumber: string;
		message: string;
		lastName: string;
		firstName: string;
	}) {
		const { email, contactNumber, message, lastName, firstName } = data;
		const smtpTransport = this.utils.RetrieveTransport();
		const { name: company, logo } = this.app.get('brand');
		const { noreply, admin } = this.app.get('emails');

		const schema = {
			email: Joi.string().required().email().label('Contact email'),
			message: Joi.string().required().label('Contact message'),
			contactNumber: Joi.string().required().label('Contact number'),
			firstName: Joi.string().required().label('First Name'),
			lastName: Joi.string().required().label('Last name'),
		};

		Joi.object(data).validate(schema);

		try {
			const res = await smtpTransport.sendMail({
				to: admin,
				from: {
					name: 'New Contact',
					address: email,
				},
				subject: `${company} - Contact Message`,
				html: contactTemplate({
					title: `${company} - Contact Email`,
					message,
					sentBy: `Email was sent by: ${email}`,
					contactNumber,
					name: `${firstName} ${lastName}`,
					company,
					logo,
				}),
			});

			smtpTransport.sendMail({
				to: email,
				from: {
					name: 'Getting in Touch',
					address: noreply,
				},
				subject: `${company} - We have recieved your message`,
				html: inTouchTemplate({
					company,
					logo,
					title: `${company} - Contact Email`,
					name: `${firstName} ${lastName}`,
					message:
						'Thank you for getting in contact with us. We mean it! At the moment we are a small team, but we will aim to respond to you within a week.',
				}),
			});

			if (res.id != null) {
				return {
					message: 'Email has been sent successfully',
					meta: {
						code: 204,
					},
				};
			}

			return Promise.resolve(
				new GeneralError('Email could not be sent.'),
			);
		} catch (err) {
			logger.debug('ERR::', err);
			throw new BadRequest(err);
		}
	}
}

export default Contact;
