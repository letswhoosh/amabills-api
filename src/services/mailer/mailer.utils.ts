/* eslint-disable @typescript-eslint/camelcase */
// @ts-nocheck
import mg from 'nodemailer-mailgun-transport';
import nodemailer from 'nodemailer';
import errors from '@feathersjs/errors';
import { IApplication } from '@app/declarations';
import logger from '@app/logger';

export default class MailUtils {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	RetrieveTransport = () => {
		try {
			const { key, domain } = this.app.get('mailgun');

			const auth = {
				auth: {
					api_key: key,
					domain,
				},
			};

			const smtpTransport = nodemailer.createTransport(mg(auth));

			return smtpTransport;
		} catch (e) {
			logger.debug('Mail sending error', e);
			throw new errors.BadRequest(e.message);
		}
	};

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	SendMail = (mailConfig: any) => {
		const smtpTransport = this.RetrieveTransport();
		return new Promise((resolve, reject) =>
			smtpTransport.verify((error) => {
				if (error) {
					logger.debug('Mail Verification Error', error);
					reject(new errors.BadRequest(error.message));
				} else {
					smtpTransport.sendMail(mailConfig, (sendError, info) => {
						if (sendError) {
							logger.debug('Mail Sending Error', error);
							reject(new errors.BadRequest(sendError.message));
						} else {
							logger.debug('Mail Sending Success', info);
							resolve(info);
						}
					});
				}
			}),
		);
	};
}
