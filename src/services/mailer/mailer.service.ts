// Initializes the `mailer` service on path `/mailer`
import { IApplication } from '@app/declarations';
import MailerActions from './actions';

export default (app: IApplication) => {
	app.use('/mailer/contact', new MailerActions.Contact(app));
};
