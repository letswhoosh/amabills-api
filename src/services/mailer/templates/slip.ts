import moment from 'moment';
import mainLayout from './layouts/main';
import { IPurchaseRead } from '@app/models/purchases.model';
import { formatCurrency } from '@app/handleCurrency';

interface TemplateInputs {
	logo: string;
	name: string;
	title: string;
	company: string;
	slip: any;
	admin: boolean;
	purchase: IPurchaseRead;
}

const PaymentSlip = (context: TemplateInputs) => {
	let listItems = '';
	const { slip } = context;

	if (slip.title) {
		listItems += `<li style="margin: 0.85em 0;"><b>${slip.title}</b></li>`;
	}

	if (slip.vatReg) {
		listItems += `<li style="margin: 0.7em 0;">VAT Reg: ${slip.vatReg}</li>`;
	}

	if (slip.orderNumber) {
		listItems += `<li style="margin: 0.7em 0;">Order Number: ${slip.orderNumber}</li>`;
	}

	if (slip.rrn) {
		listItems += `<li style="margin: 0.7em 0;">RRN: ${slip.rrn}</li>`;
	}

	if (slip.meterNumber) {
		listItems += `<li style="margin: 0.7em 0;">Meter Number: ${slip.meterNumber}</li>`;
	}

	if (slip.alg) {
		listItems += `<li style="margin: 0.7em 0;">Alg: ${slip.alg}</li>`;
	}

	if (slip.krn) {
		listItems += `<li style="margin: 0.7em 0;">KRN: ${slip.krn}</li>`;
	}

	if (slip.sgc) {
		listItems += `<li style="margin: 0.7em 0;">SGC: ${slip.sgc}</li>`;
	}

	if (slip.ti) {
		listItems += `<li style="margin: 0.7em 0;">Ti: ${slip.ti}</li>`;
	}

	if (slip.tt) {
		listItems += `<li style="margin: 0.7em 0;">Tt: ${slip.tt}</li>`;
	}

	if (slip.address) {
		listItems += `<li style="margin: 0.7em 0;">Address: ${slip.address}</li>`;
	}

	if (slip.receiptNumber) {
		listItems += `<li style="margin: 0.7em 0;">Receipt Number: ${slip.receiptNumber}</li>`;
	}

	if (slip.fbeCred && slip.fbeCred.amount) {
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		listItems +=
			'<li style="margin: 0.7em 0;"><b>Free Basic Electricity<b/></li>';
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		listItems += `<li style="margin: 0.7em 0;">Units  ${slip.fbeCred.units} kWh</li>`;
		listItems += `<li style="margin: 0.7em 0;">Token: ${slip.fbeCred.token}</li>`;
	}

	if (slip.saleCred && slip.saleCred.amount) {
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		listItems +=
			'<li style="margin: 0.7em 0;"><b>Purchased Electricity</b></li>';
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		listItems += `<li style="margin: 0.7em 0;">Units: ${slip.saleCred.units} kWh</li>`;
		listItems += `<li style="margin: 0.7em 0;">Token: ${slip.saleCred.token}</li>`;
	}

	if (slip.costs) {
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		listItems += '<li style="margin: 0.7em 0;"><b>Costs<b/></li>';
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
		if (
			context.admin === true &&
			slip.costs.processingFee &&
			slip.costs.serviceFee
		) {
			listItems += `<li style="margin: 0.7em 0;">Processing Fee: ${formatCurrency(
				slip.costs.processingFee,
			)}</li>`;
			listItems += `<li style="margin: 0.7em 0;">Service Fee: ${formatCurrency(
				slip.costs.serviceFee,
			)}</li>`;
		}
		if (context.admin === true) {
			listItems += `<li style="margin: 0.7em 0;">VAT: ${slip.costs.vat}</li>`;
		}
		listItems += `<li style="margin: 0.7em 0;">Total: ${formatCurrency(
			context.admin === true
				? slip.costs.total
				: context.purchase.amountInCents,
		)}</li>`;
		listItems +=
			'<li style="margin: 0.5em 0; border-top: black solid 1px"></li>';
	}

	if (slip.utility) {
		listItems += `<li style="margin: 0.7em 0;">Utility: ${slip.utility}</li>`;
	}

	if (context.purchase.createdAt) {
		listItems += `<li style="margin: 0.7em 0;">Printed: ${moment(
			context.purchase.createdAt,
		).format('Do MMMM YYYY, HH:mm')}</li>`;
	}

	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="70%">
        <mj-text font-family="Lato, Helvetica, Arial, sans-serif">
          <div style="box-shadow: 0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12); padding: 25px">
            <ul style="list-style: none; margin-left: 0px; padding-left: 0px">
              ${listItems}
            </ul>
          </div>
        </mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
      <mj-column></mj-column>
      <mj-column>
      </mj-column>
    </mj-section>
    </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default PaymentSlip;
