import mainLayout from './layouts/main';
import config from './config';

interface TemplateInputs {
	url: string;
	name: string;
	title: string;
	company: string;
	logo: string;
	password?: string;
	email: string;
}

const vendorLogin = (context: TemplateInputs) => {
	const passwordText =
		context.password != null
			? `
  <mj-section>
  <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px">
  <strong>Your password has been set to:</strong></mj-text>
  <mj-spacer height="10px" />
        <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px">${context.password}</mj-text>
      </mj-section>`
			: `<mj-section>
  <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px">
  <strong>Login in using your existing account</strong></mj-text>
      </mj-section>`;

	const updatePasswordText =
		context.password != null
			? `<mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>Once logged in please update your password.</strong></mj-text>`
			: '';

	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="100%">
        <mj-text align="center" color=${config.secondaryColor} font-size="25px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px"><strong>Hey ${context.name}, congratulations on becoming an amaBills vendor
          <br />
            </strong></mj-text>
          <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>Your email to use for login:</strong></mj-text>
          <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>${context.email}</strong></mj-text>
          ${passwordText}
            <br />
  ${updatePasswordText}
  <mj-spacer height="10px" />
        <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>Click the button below to sign into your account</strong></mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
      <mj-column></mj-column>
      <mj-column>
      <mj-button href="${context.url}" font-family="Helvetica" background-color=${config.primaryColor} color="white">
           Proceed to sign in page
        </mj-button>
      </mj-column>
      <mj-column>
      </mj-column>
    </mj-section>
  </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default vendorLogin;
