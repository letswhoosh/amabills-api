import { Application } from '@feathersjs/feathers';
import mainLayout from './layouts/main';
import config from './config';

interface TemplateInputs {
	url: string;
	name: string;
	title: string;
	company: string;
	logo: string;
	verificationCode: string;
	verifyUrl: string;
}

const verifyAccount = (context: TemplateInputs) => {
	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="100%">
        <mj-text align="center" color=${config.secondaryColor} font-size="25px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px"><strong>Hey ${context.name}
            <br />
            </strong></mj-text>
        <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>Please click the button below to activate your account</strong></mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
      <mj-column></mj-column>
      <mj-column>
       <mj-button href="${context.url}" font-family="Helvetica" background-color=${config.primaryColor} color="white">
          Verify Account
         </mj-button>
      </mj-column>
      <mj-column>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
        <mj-column>
          <mj-text align="center" font-size="15px" color=${config.greyText} font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px">
            If that doesn't work, follow
            <a font-family="Helvetica" class="link-nostyle" text-decoration="none" href="${context.verifyUrl}" color="white !important">
              this link
            </a>

            and paste in the following verification code:
            <br />
          </mj-text>


          <mj-text align="center" color="black" font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px">
            ${context.verificationCode}
            <br />
          </mj-text>
        </mj-column>
      </mj-section>
    </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default verifyAccount;
