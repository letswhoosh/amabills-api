const config = {
	primaryColor: '#1e88e5',
	secondaryColor: '#263238',
	backgroundColor: '#cfd8dc',
	defaultTextColor: '#fff',
	greyText: '#555555',
};

export default config;
