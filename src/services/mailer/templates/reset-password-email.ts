import { Application } from '@feathersjs/feathers';
import mainLayout from './layouts/main';
import config from './config';

interface TemplateInputs {
	url: string;
	logo: string;
	name: string;
	recoverText: string;
	title: string;
	linkText: string;
	company: string;
}

const resetPassword = (context: TemplateInputs) => {
	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="100%">
        <mj-text align="center" color=${config.secondaryColor} font-size="25px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px"><strong>Hey ${context.name}
            <br />
            </strong></mj-text>
        <mj-text align="center" color=${config.primaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>Please click the button below to to recover your password</strong></mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
      <mj-column></mj-column>
      <mj-column>
       <mj-button href="${context.url}" font-family="Helvetica" background-color=${config.primaryColor} color=${config.defaultTextColor}>
          Recover Password
         </mj-button>
      </mj-column>
      <mj-column>
      </mj-column>
    </mj-section>
    </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default resetPassword;
