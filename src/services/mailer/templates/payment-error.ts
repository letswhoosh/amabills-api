import mainLayout from './layouts/main';
import config from './config';

interface TemplateInputs {
	title: string;
	message: string;
	contactNumber: string;
	name: string;
	company: string;
	logo: string;
	email: string;
}

const PaymentError = (context: TemplateInputs) => {
	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="100%">
        <mj-text align="center" color=${config.secondaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px">${context.message}</mj-text>
        <mj-text align="center" color=${config.secondaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>${context.name} can be reached on ${context.contactNumber} or ${context.email}</strong></mj-text>
      </mj-column>
    </mj-section>
    </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default PaymentError;
