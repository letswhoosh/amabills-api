import mjml2html from 'mjml';
import moment from 'moment';
import { formatCurrency } from '@app/handleCurrency';

interface InvoiceItem {
	name: string;
	amount: number;
}

interface TemplateInputs {
	title: string;
	name: string;
	date?: Date;
	invoiceItems: InvoiceItem[];
	logo: string;
	company: string;
	link: string;
}

const invoice = (context: TemplateInputs) => {
	const invoiceItems = context.invoiceItems.reduce((acc, val) => {
		const item = `<tr>
    <td style="padding: 0 15px 0 0;">${val.name}</td>
    <td style="padding: 0 0 0 15px;text-align: right">R ${formatCurrency(
		val.amount,
	)}</td>
  </tr>`;
		const currList = acc + item;
		return currList;
	}, '');
	const total = context.invoiceItems.reduce((acc, val) => {
		const curTotal = acc + val.amount;
		return curTotal;
	}, 0);
	const body = `
  <mjml>
  <mj-head>
    <mj-title>${context.company} Invoice</mj-title>
    <mj-font name="Roboto" href="https://fonts.googleapis.com/css?family=Roboto:300,500"></mj-font>
    <mj-attributes>
      <mj-all font-family="Roboto, Helvetica, sans-serif"></mj-all>
      <mj-text font-weight="300" font-size="16px" color="#616161" line-height="24px"></mj-text>
      <mj-section padding="0px"></mj-section>
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-section padding="20px 0">
      <mj-column width="60%">
        <mj-text font-size="10px">${
			context.date || moment().format('DD/MM/YYYY')
		}</mj-text>
      </mj-column>
      <mj-column width="40%">
        <mj-text align="right">${context.company}</mj-text>
      </mj-column>
    </mj-section>
    <mj-section>
      <mj-column width="100%">
        <mj-image href="https://recast.ai?ref=newsletter" src="https://cdn.recast.ai/newsletter/city-01.png"></mj-image>
      </mj-column>
    </mj-section>
    <mj-section>
      <mj-column width="100%">
        <mj-text align="center"></mj-text>
      </mj-column>
    </mj-section>
    <mj-section>
      <mj-column width="45%">
        <mj-text align="center" font-weight="500" padding="0px" font-size="18px">Invoice for 2019/05/19</mj-text>
        <mj-divider border-width="2px" border-color="#616161"></mj-divider>
</mj-divider>
      </mj-column>
    </mj-section>

    <mj-section padding-top="15px">
      <mj-column width="100%">
        <mj-text>
          <p>Hey ${context.name},</p>
          <p>Please view your bill information below: </p>
        </mj-text>
      </mj-column>
    </mj-section>

   <mj-section full-width="full-width">
      <mj-column width="100%">
        <mj-table>
          <tr style="border-bottom:1px solid #ecedee;text-align:left;padding:15px 0;">
            <th style="padding: 0 15px 0 0;">Item</th>
            <th style="padding: 0 0 0 15px; text-align: right">Cost</th>
          </tr>
         ${invoiceItems}
           <tr>
            <td style="padding: 0 15px 0 0;"></td>
             <td style="padding: 0 0 0 15px;text-align: right; border-top: 1px solid rgba(0,0,0,0.7);"><b>R ${formatCurrency(
					total,
				)}</b></td>
          </tr>
        </mj-table>
      </mj-column>
    </mj-section>

    <mj-section>
      <mj-column width="100%">
        <mj-divider border-width="1px" border-color="#E0E0E0"></mj-divider>
      </mj-column>
    </mj-section>
    <mj-section>
      <mj-column width="65%">
        <mj-image align="left" width="50px" href=${context.link} src=${
		context.logo
	}>
        </mj-image>
      </mj-column>
      <mj-column width="35%">
        <mj-table>
          <tr style="list-style: none;line-height:1">
            <td> <a href="https://twitter.com/RecastAI">
                  <img width="25" src="https://cdn.recast.ai/newsletter/twitter.png" />
                </a> </td>
            <td> <a href="https://www.facebook.com/recastAI">
                  <img width="25" src="https://cdn.recast.ai/newsletter/facebook.png" />
                </a> </td>
            <td> <a href="https://medium.com/@RecastAI">
                  <img width="25" src="https://cdn.recast.ai/newsletter/medium.png" />
                </a> </td>
            <td> <a href="https://www.youtube.com/channel/UCA0UZlR8crpgwFiVaSTbVWw">
                  <img width="25" src="https://cdn.recast.ai/newsletter/youtube.png" />
                </a> </td>
            <td> <a href="https://plus.google.com/u/0/+RecastAi">
                  <img width="25" src="https://cdn.recast.ai/newsletter/google%2B.png" />
                </a> </td>
          </tr>
        </mj-table>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
	`;
	const { html } = mjml2html(body);

	return html;
};

export default invoice;
