import mjml2html from 'mjml';
import config from '../config';

interface TemplateInputs {
	body: string;
	title: string;
	logo: string;
	company: string;
}

const mainLayout = ({ logo, title, body, company }: TemplateInputs) => {
	const { html } = mjml2html(`
  <mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0px"></mj-all>
      <mj-class name="preheader" color="#000000" font-size="11px" font-family="Ubuntu, Helvetica, Arial, sans-serif" padding="0px"></mj-class>
    </mj-attributes>
    <mj-style inline="inline">a { text-decoration: none; color: inherit; }</mj-style>
  </mj-head>
  <mj-body background-color=${config.backgroundColor}>
    <mj-section background-color=${config.secondaryColor} padding="10px 0">
      <mj-column width="33%">
      </mj-column>
      <mj-column width="100%">#
        <mj-text align="center" color=${config.defaultTextColor} font-size="20px" font-family="Lato, Helvetica, Arial, sans-serif" padding="18px 0px">${title}</mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding-top="20px">
      <mj-column width="100%">
        <mj-image src="${logo}" alt="tickets" width="120px" padding="10px 25px"></mj-image>
      </mj-column>
    </mj-section>
    ${body}
    <mj-section background-color=${config.secondaryColor} padding="10px">
      <mj-column vertical-align="top" width="100%">
        <mj-text align="center" color=${config.defaultTextColor} font-size="20px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px">Best Regards, <br /><br />The ${company} Team</mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
	`);
	return html;
};

export default mainLayout;
