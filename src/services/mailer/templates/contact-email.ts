import mainLayout from './layouts/main';
import config from './config';

interface TemplateInputs {
	title: string;
	message: string;
	sentBy: string;
	contactNumber: string;
	name: string;
	company: string;
	logo: string;
}

const contact = (context: TemplateInputs) => {
	const body = `
  <mj-wrapper>
    <mj-section background-color="#ffffff">
      <mj-column width="100%">
        <mj-text align="center" color=${config.secondaryColor} font-size="25px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 25px"><strong>Hey There,
            <br />
            </strong></mj-text>
        <mj-text align="center" color=${config.secondaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>${context.message}</strong></mj-text>
        <mj-text align="center" color=${config.secondaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>${context.sentBy}</strong></mj-text>

        <mj-text align="center" color=${config.secondaryColor} font-size="18px" font-family="Lato, Helvetica, Arial, sans-serif" padding="10px 30px"><strong>${context.name} can be reached on ${context.contactNumber}</strong></mj-text>
      </mj-column>
    </mj-section>
    <mj-section background-color="#ffffff" padding="20px 0">
      <mj-column></mj-column>
      <mj-column>
      </mj-column>
    </mj-section>
    </mj-wrapper>
	`;
	return mainLayout({ body, ...context });
};

export default contact;
