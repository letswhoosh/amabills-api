// Initializes the `tj-payments` service on path `/tj-payments`
import { ServiceAddons } from '@feathersjs/feathers';
import { IApplication } from '../../declarations';
import { TjPayments } from './tj-payments.class';
import createModel from '../../models/tj-payments.model';
import hooks from './tj-payments.hooks';
import TjActions from './actions';

// Add this service to the service type index
declare module '../../declarations' {
	interface ServiceTypes {
		'tj-payments': TjPayments & ServiceAddons<any>;
	}
}

export default function (app: IApplication): void {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/tj-payment-key', new TjActions.GetPaymentKey(app));

	app.use('/tj-payments', new TjPayments(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('tj-payments');

	service.hooks(hooks);
}
