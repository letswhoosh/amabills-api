import { IApplication } from '@app/declarations';
import {
	IPaymentCurrency,
	IPaymentRead,
	IPaymentStatus,
	IPaymentWrite,
} from '@app/models/payments/model';
import axios from 'axios';
import qs from 'qs';

/**
 * This method also verifies a users account but does so by sending
 * an email to the user with a code which the user has to input into the
 * app, which is then finally confirmed here, and if all is good, account
 * gets verified.
 */
class GetPaymentKey {
	app: IApplication;

	constructor(app: IApplication) {
		this.app = app;
	}

	async create(data: {
		amount: string;
		userId: string;
		merchantReference: string;
		reference: string;
	}) {
		try {
			const date = new Date();
			const merchantReference = `${data.userId}_${
				date.getMonth() + 1
			}_${date.getDate()}_${date.getMinutes()}_${date.getSeconds()}`;
			const params = {
				merchant_reference: merchantReference,
				amount: data.amount,
				success_url: 'https://amabills.co.za/',
				error_url: 'https://amabills.co.za/',
			};
			const payload = qs.stringify(params);
			const url = 'https://gateway.switch.tj/api/v1/payment-key';
			const result = (
				await axios.post(url, payload, {
					headers: {
						Authorization:
							'Basic V2hvb3NoSW5ub3ZhdGlvbnM6U2hpZnVzZW5zZWlAMTIzNA==',
					},
				})
			).data;
			const payment: IPaymentWrite = {
				userId: data.userId,
				checkoutId: merchantReference,
				amountInCents: parseInt(data.amount),
				description: '',
				status: IPaymentStatus.Processing,
				currency: IPaymentCurrency.ZAR,
				reference: data.reference,
			};
			const paymentCreated: IPaymentRead = await this.app
				.service('payments')
				.create(payment);
			return { ...result, ...paymentCreated };
		} catch (error) {
			return error;
		}
	}
}

export default GetPaymentKey;
