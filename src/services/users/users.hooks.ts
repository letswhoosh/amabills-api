import * as feathersAuthentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
// @ts-ignore Need TS Typing
import hydrate from 'feathers-sequelize/hooks/hydrate';
import { shouldSequelizeModel } from '../utils';
import {
	attachAssociations,
	sendNewUserEmail,
	assignRelationships,
	generatePassword,
} from './users.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;

export default {
	before: {
		all: [],
		find: [authenticate('jwt'), attachAssociations],
		get: [authenticate('jwt'), attachAssociations],
		create: [generatePassword, hashPassword('password')],
		update: [hashPassword('password'), authenticate('jwt')],
		patch: [hashPassword('password'), authenticate('jwt')],
		remove: [authenticate('jwt')],
	},

	after: {
		all: [
			// Make sure the password field is never sent to the client
			// Always must be the last hook
			shouldSequelizeModel,
			protect('password'),
		],
		find: [],
		get: [],
		create: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
			sendNewUserEmail,
		],
		update: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
		],
		patch: [
			// Access sequelize model
			hydrate(),
			assignRelationships,
		],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
