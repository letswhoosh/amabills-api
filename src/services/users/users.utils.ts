/* eslint-disable no-param-reassign */
import { HookContext } from '@feathersjs/feathers';
import Sequelize from 'sequelize';
import R from 'ramda';
import { shouldSequelizeModel, generateCode } from '../utils';
import { IUserRead, TUserInstance } from '@app/models/users.model';
import { BadRequest } from '@feathersjs/errors';
import { IAPIHookContext } from '@app/declarations';

export const attachAssociations = async (context: HookContext) => {
	const sequelize: Sequelize.Sequelize = context.app.get('sequelizeClient');
	const { permissions } = sequelize.models;

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: permissions,
			},
		],
	};

	return context;
};

export const assignRelationships = async (context: HookContext<IUserRead>) => {
	const newUser = (context.result as unknown) as TUserInstance;

	if (newUser == null) {
		throw new BadRequest('User instance is not defined');
	}

	const { permissionIds } = context.data || {};

	if (permissionIds != null && permissionIds.length > 0) {
		await newUser.setPermissions(permissionIds);
	}

	return shouldSequelizeModel(context);
};

export const sendNewUserEmail = async (context: HookContext) => {
	const { app } = context as IAPIHookContext;

	app.service('auth/verify').create({
		email: context.result.email,
		presetPassword: context.data.unhashed,
	});

	if (context.data.unhashed != null) delete context.data.unhashed;

	return context;
};

export const generatePassword = async (context: HookContext) => {
	const { password, sendPassword } = context.data;

	if (password == null || sendPassword === true) {
		const pass = password || generateCode(6);
		context.data.password = pass;
		context.data.unhashed = pass;
	}

	return context;
};
