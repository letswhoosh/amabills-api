// Initializes the `preferences` service on path `/preferences`
import { IApplication } from '@app/declarations';
import { Preferences } from './preferences.class';
import createModel from '@app/models/preferences.model';
import hooks from './preferences.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/preferences', new Preferences(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('preferences');

	service.hooks(hooks);
};
