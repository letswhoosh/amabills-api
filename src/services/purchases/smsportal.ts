import axios from 'axios';
import { HookContext } from '@feathersjs/feathers';
import { IAPIHookContext } from '@app/declarations';

export const authenticate = async (context: HookContext) => {
	const { app } = context as IAPIHookContext;
	try {
		const { baseUrl, base64 } = app.get('smsPortal');
		const { clientId, secret, authUrl, smsUrl } = app.get('sms_portal');
		const apiKey = clientId;
		const apiSecret = secret;
		const accountApiCredentials = apiKey + ':' + apiSecret;

		//@ts-ignore
		const buff = new Buffer.from(accountApiCredentials);
		const base64Credentials = buff.toString('base64');

		const authHeader = 'Basic ' + base64Credentials;
		const result = await axios.get(`${baseUrl}/Authentication`, {
			headers: {
				Authorization: `Bearer ${authHeader}`,
			},
		});
		return { success: true, result };
	} catch (err) {
		console.log('SMS PORTAL AUTHENTICATION ERROR', err);
		return { success: false };
	}
};

interface Message {
	Content: string;
	Destination: string;
}

interface SendParms {
	token: string;
	messages: Message[];
}

export const send = async (
	context: IAPIHookContext,
	{ token, messages }: SendParms,
) => {
	try {
		const { baseUrl } = context.app.get('smsPortal');
		const result = await axios.post(
			`${baseUrl}/bulkmessages`,
			{
				SendOptions: {
					TestMode: process.env.NODE_ENV !== 'production',
				},
				Messages: messages,
			},
			{
				headers: {
					Authorization: `Bearer ${token}`,
				},
			},
		);
		return { success: true, result };
	} catch (err) {
		console.log('SMS PORTAL SEND ERROR', err);
		return { success: false };
	}
};
