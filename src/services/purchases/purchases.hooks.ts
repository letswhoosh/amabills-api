import * as authentication from '@feathersjs/authentication';
// @ts-ignore Need TS Typing
import hydrate from 'feathers-sequelize/hooks/hydrate';
import { shouldSequelizeModel } from '../utils';
import {
	attachAssociations,
	completePayment,
	creditMeterVendor,
	updateLoyaltyPoints,
	triggerPaymentNotifications,
} from './purchases.utils';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
	before: {
		all: [authenticate('jwt')],
		find: [attachAssociations],
		get: [attachAssociations],
		create: [],
		update: [completePayment],
		patch: [completePayment],
		remove: [],
	},

	after: {
		all: [shouldSequelizeModel],
		find: [],
		get: [],
		create: [
			// Access sequelize model
			hydrate(),
		],
		update: [
			triggerPaymentNotifications,
			updateLoyaltyPoints,
			creditMeterVendor,
			// Access sequelize model
			hydrate(),
		],
		patch: [
			triggerPaymentNotifications,
			updateLoyaltyPoints,
			creditMeterVendor,
			// Access sequelize model
			hydrate(),
		],
		remove: [],
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: [],
	},
};
