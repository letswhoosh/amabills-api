/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
import { HookContext, Paginated } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import moment from 'moment';
import { IAPIHookContext } from '@app/declarations';
import { IUserRead } from '@app/models/users.model';
import {
	IPurchaseRead,
	IPurchaseWrite,
	ISlipPrepaid,
} from '@app/models/purchases.model';
import {
	MARGIN_PERCENT,
	SERVICE_FEE_PORTION,
	PROCESSING_FEE_PORTION,
} from '@app/variables';
import MailUtils from '../mailer/mailer.utils';
import { slipTemplate } from '../mailer/templates';
import { authenticate, send } from './smsportal';
import Dinero, { formatCurrency } from '@app/handleCurrency';
import { IMeterRead } from '@app/models/meters.model';

export const attachAssociations = async (context: HookContext) => {
	const { app } = context as IAPIHookContext;
	const sequelize = app.get('sequelizeClient');
	const { users, vouchers } = sequelize.models;

	context.params.sequelize = {
		raw: false,
		include: [
			{
				model: users,
			},
			{
				model: vouchers,
			},
		],
	};

	return context;
};

export const completePayment = async (context: HookContext) => {
	const { data, app, params, id } = context as IAPIHookContext;

	if (id == null) {
		throw new BadRequest('No Purchase ID was provided');
	}

	try {
		const user =
			params && params.user
				? (params.user as IUserRead)
				: data && data.userId
				? await app.service('users').get(data.userId)
				: null;

		if (data.paid === true) {
			if (user == null) {
				throw new BadRequest('No user could be matched to the payment');
			}

			const { walletBalancePaymentInCents } = context.data;

			const purchase = await app.service('purchases').get(id);

			if (walletBalancePaymentInCents > 0 && user.vendorId) {
				await app.service('vendor-transactions').create({
					amountInCents: Math.round(walletBalancePaymentInCents) * -1,
					purchaseId: purchase.id,
					vendorId: user.vendorId,
				});
			}

			context.data = {
				...context.data,
				state: 'paid',
			};
		}

		return context;
	} catch (err) {
		const purchase = await app.service('purchases').get(id);
		context.app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not complete prepaid payment',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: purchase.transactionRef,
		});

		throw err;
	}
};

export const triggerPaymentNotifications = async (context: HookContext) => {
	if (context.data.paid === true) {
		useVoucher(context);
		sendSlip(context, context.result);
		sendSMS(context, context.result);
	}

	return context;
};

export const creditMeterVendor = async (context: HookContext) => {
	const {
		data,
		app,
		params,
		id,
	} = context as IAPIHookContext<IPurchaseWrite>;

	const { user } = params as { user: IUserRead };

	if (data == null) {
		throw new BadRequest('Data was not provided');
	}

	try {
		if (data.paid === true) {
			const processTransaction = async (
				vendorId: number,
				purchaseToProcess: IPurchaseRead,
			) => {
				const { creditCardPaymentInCents } = data;

				const vendor = await app.service('vendors').get(vendorId);

				const amountToPurchaseInCents = Dinero({
					amount: Math.round(creditCardPaymentInCents || 0),
				}).divide(1 + MARGIN_PERCENT);

				const fee = Dinero({
					amount: Math.round(creditCardPaymentInCents || 0),
				}).subtract(amountToPurchaseInCents);

				const serviceFeeInCents = fee.multiply(SERVICE_FEE_PORTION);
				const processingFeeInCents = fee.multiply(
					PROCESSING_FEE_PORTION,
				);

				const margin = serviceFeeInCents.add(processingFeeInCents);
				const amountToCreditInCents = margin.multiply(
					vendor.discountRateInPercent / 100,
				);
				if (amountToCreditInCents.getAmount() !== 0) {
					await app.service('vendor-transactions').create({
						amountInCents: amountToCreditInCents.getAmount(),
						purchaseId: purchaseToProcess.id,
						vendorId,
					});
				}
			};

			const { transactionRef } = context.result;
			const purchase = await getTransaction(context, transactionRef);
			const { data: meters } = (await app.service('meters').find({
				query: { serialNumber: purchase.meterNumber },
			})) as Paginated<IMeterRead>;

			let permissions = [] as string[];
			if (user != null) {
				permissions = user.permissions.map((n) => n.permission);
			}

			if (meters.length > 0) {
				const meter = meters.find(
					(n) => n.vendorId != null,
				) as IMeterRead;
				await processTransaction(meter.vendorId, purchase);
			} else if (
				user &&
				permissions.includes('isVendor') &&
				user.vendorId
			) {
				await processTransaction(user.vendorId, purchase);
			}
		}

		return context;
	} catch (err) {
		app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not credit meter vendor',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: context.result.transactionRef,
		});

		throw err;
	}
};

export const updateLoyaltyPoints = async (context: HookContext) => {
	const { app, params, data } = context as IAPIHookContext<
		{
			addLoyalty: boolean;
		} & IPurchaseWrite
	>;
	const user =
		params && params.user
			? (params.user as IUserRead)
			: data && data.userId
			? await app.service('users').get(data.userId)
			: null;

	if (data == null) {
		throw new BadRequest('Data was not provided');
	}

	const { paid, addLoyalty } = data;

	try {
		if (paid === true && addLoyalty) {
			const PREPAID_BONUS_PERCENTAGE = 5;

			if (user == null) {
				throw new BadRequest('No user could be matched to the payment');
			}

			const processTransaction = async (
				userId: number,
				purchaseToProcess: IPurchaseRead,
			) => {
				const {
					creditCardPaymentInCents,
					walletBalancePaymentInCents,
				} = data;

				if (walletBalancePaymentInCents == null) {
					throw new BadRequest(
						'Wallet balance value was not provided',
					);
				}

				const amountToPurchase = Dinero({
					amount: Math.round(creditCardPaymentInCents || 0),
				}).divide(1 + MARGIN_PERCENT);

				const fee = Dinero({
					amount: Math.round(creditCardPaymentInCents || 0),
				}).subtract(amountToPurchase);

				const serviceFeeInCents = fee.multiply(SERVICE_FEE_PORTION);
				const processingFeeInCents = fee.multiply(
					PROCESSING_FEE_PORTION,
				);

				const margin = serviceFeeInCents.add(processingFeeInCents);

				const amountToCredit = margin.multiply(
					PREPAID_BONUS_PERCENTAGE / 100,
				);

				if (amountToCredit.getAmount() > 0 && user.vendorId == null) {
					await app.service('loyalty-points').create({
						amountInCents: amountToCredit.getAmount(),
						purchaseId: purchaseToProcess.id,
						userId,
					});
				}

				if (walletBalancePaymentInCents > 0 && user.vendorId == null) {
					await app.service('loyalty-points').create({
						amountInCents: walletBalancePaymentInCents * -1,
						purchaseId: purchaseToProcess.id,
						userId,
					});
				}
			};

			const { transactionRef } = context.result;
			const purchase = await getTransaction(context, transactionRef);

			await processTransaction(user.id, purchase);
		}

		return context;
	} catch (err) {
		context.app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not update loyalty points',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: context.result.transactionRef,
		});

		throw err;
	}
};

const getTransaction = async (context: HookContext, transactionRef: string) => {
	const { app } = context as IAPIHookContext;
	const { data: purchases } = (await app
		.service('purchases')
		.find({ query: { transactionRef } })) as Paginated<IPurchaseRead>;

	if (purchases.length < 1) {
		app.service('purchase-errors').create({
			source: 'internal',
			description:
				'Given transaction reference does not match to any purchase',
			dataProvided: { transactionRef },
			transactionRef,
		});
		throw new BadRequest(
			new Error('Transaction ref does not match any purchase'),
		);
	}

	return purchases[0];
};

const useVoucher = async (context: HookContext) => {
	const { app, result } = context as IAPIHookContext<IPurchaseRead>;
	const { voucherId, userId } = result || {};

	if (voucherId != null) {
		try {
			const voucher = await app.service('vouchers').get(voucherId);
			if (voucher != null) {
				await app.service('vouchers').patch(voucher.id, {
					state: 'used',
					usedOn: new Date(),
					issuedTo: userId,
				});
			}
		} catch (err) {
			app.service('purchase-errors').create({
				source: 'internal',
				description: 'Could not update voucher status to used',
				dataProvided: { voucherId },
				error: {
					name: err.name,
					message: err.message,
					stack: err.stack,
				},
				transactionRef: context.result.transactionRef,
			});

			throw err;
		}
	}
};

const sendSMS = async (context: HookContext, purchase: IPurchaseRead) => {
	const { app } = context as IAPIHookContext;

	try {
		const { slipVariables, contactNumber } = purchase;

		if (slipVariables == null) {
			throw new BadRequest('Slip data is not defined');
		}

		if (contactNumber) {
			const messages = [];
			if (
				(slipVariables as ISlipPrepaid).fbeCred &&
				(slipVariables as ISlipPrepaid).fbeCred.token != null &&
				(slipVariables as ISlipPrepaid).fbeCred.token !== ''
			) {
				const fbeString = `amaBills
      ${moment(purchase.createdAt).format('DD-MM-YYYY, HH:mm')}
      Units: ${(slipVariables as ISlipPrepaid).fbeCred.units} kWh
      Token: ${(slipVariables as ISlipPrepaid).fbeCred.token}`;
				messages.push({
					content: fbeString,
					destination: contactNumber.replace('+', ''),
				});
			}

			if (
				(slipVariables as ISlipPrepaid).saleCred &&
				(slipVariables as ISlipPrepaid).saleCred.token != null &&
				(slipVariables as ISlipPrepaid).saleCred.token !== ''
			) {
				const saleCred = `amaBills
    ${moment(purchase.createdAt).format('DD-MM-YYYY, HH:mm')}
    Units: ${(slipVariables as ISlipPrepaid).saleCred.units} kWh
    Token: ${(slipVariables as ISlipPrepaid).saleCred.token}
    ${formatCurrency(purchase.amountInCents)}`;
				messages.push({
					content: saleCred,
					destination: contactNumber.replace('+', ''),
				});
			}

			await app.service('send-sms').create({messages})
		}
	} catch (err) {
		app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not send prepaid sms',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: purchase.transactionRef,
		});

		throw err;
	}
};

const sendSlip = async (context: HookContext, purchase: IPurchaseRead) => {
	const { app } = context as IAPIHookContext;

	try {
		const mailUtils = new MailUtils(app);
		if (purchase.userId == null) {
			throw new BadRequest('Purchase userID is not defined');
		}
		const user = await app.service('users').get(purchase.userId);
		const { noreply, admin } = app.get('emails');
		const { name: company, logo } = app.get('brand');
		const config = {
			to: purchase.email,
			from: {
				name: `${company} Purchase Slip`,
				address: noreply,
			},
			subject: `${company} Slip`,
			html: slipTemplate({
				company,
				logo,
				name: `${user.firstName} ${user.lastName}`,
				title: `${company} Purchase Slip`,
				slip: purchase.slipVariables,
				admin: false,
				purchase,
			}),
		};
		await mailUtils.RetrieveTransport().sendMail(config);
		const adminConfig = {
			to: admin,
			from: {
				name: `${company} Purchase Slip`,
				address: noreply,
			},
			subject: `${company} Slip`,
			html: slipTemplate({
				company,
				logo,
				name: `${user.firstName} ${user.lastName}`,
				title: `${company} Purchase Slip`,
				slip: purchase.slipVariables,
				admin: true,
				purchase,
			}),
		};
		await mailUtils.RetrieveTransport().sendMail(adminConfig);
	} catch (err) {
		console.log('sendSlip Error::', err);
		app.service('purchase-errors').create({
			source: 'internal',
			description: 'Could not send prepaid slip',
			error: {
				name: err.name,
				message: err.message,
				stack: err.stack,
			},
			transactionRef: purchase.transactionRef,
		});
	}
};

/*
-- Test Meters --
1407039368267
1407045683196
1404113048931
1407063650333
1404087926039
1407030986448
1407052178486
*/
