// Initializes the `purchases` service on path `/purchases`
import { IApplication } from '@app/declarations';
import { Purchases } from './purchases.class';
import createModel from '@app/models/purchases.model';
import hooks from './purchases.hooks';

export default (app: IApplication) => {
	const options = {
		Model: createModel(app),
		paginate: app.get('paginate'),
	};

	// Initialize our service with any options it requires
	app.use('/purchases', new Purchases(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('purchases');

	service.hooks(hooks);
};
