import './aliases';
import logger from './logger';
import app from './app';
import localtunnel from 'localtunnel';

export const startLocalTunnel = async () => {
	const tunnel = await localtunnel({
		port: 8080,
		subdomain: 'amabills-dev',
	});

	logger.info(`Public server started on: ${tunnel.url}`);

	tunnel.on('close', () => {
		// tunnels are closed
	});
};

// startLocalTunnel();

const port = app.get('port');
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) =>
	logger.error('Unhandled Rejection at: Promise ', p, reason),
);

server.on('listening', () => {
	logger.info(
		`Loading environment config for '${process.env.NODE_ENV || 'default'}'`,
	);
	logger.info('AmaBills API started on: %s', app.get('apiUrl'));
	logger.info(`Socket.io listening on: ${app.get('websocketPath')}`);
});
