module.exports = {
	up: async (queryInterface) => {
		const vendorPermissions = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from permissions WHERE permission IN ('IsVendor')",
		);
		if (vendorPermissions[0][0] == null) {
			await queryInterface.bulkInsert(
				'permissions',
				[
					{
						name: 'isVendor',
						permission: 'isVendor',
						risk: 3,
						role: 'vendor',
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				],
				{},
			);
		}
	},

	down: async (queryInterface, Sequelize) => {
		const vendorPermissions = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from permissions WHERE permission IN ('isVendor')",
		);
		if (vendorPermissions[0][0] != null) {
			const { Op } = Sequelize;
			await queryInterface.bulkDelete(
				'permissions',
				{ id: { [Op.in]: [vendorPermissions[0][0].id] } },
				{},
			);
		}
	},
};
