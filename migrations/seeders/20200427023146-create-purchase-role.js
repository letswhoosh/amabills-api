module.exports = {
	up: async (queryInterface, Sequelize) => {
		const purchasePermissions = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from permissions WHERE permission IN ('administerAllPurchases')",
		);

		if (purchasePermissions[0][0] == null) {
			await queryInterface.bulkInsert(
				'permissions',
				[
					{
						name: 'Administer All Purchases',
						permission: 'administerAllPurchases',
						risk: 5,
						role: 'super',
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				],
				{},
			);
			const permissions = await queryInterface.sequelize.query(
				// eslint-disable-next-line quotes
				"SELECT id from permissions WHERE permission IN ('administerAllPurchases')",
			);

			const adminUser = await queryInterface.sequelize.query(
				// eslint-disable-next-line quotes
				"SELECT id from users WHERE email IN ('sammy.admin@amabils.co.za')",
			);

			const data = permissions[0].map((n) => ({
				permissionId: n.id,
				userId: adminUser[0][0].id,
				createdAt: new Date(),
				updatedAt: new Date(),
			}));

			await queryInterface.bulkInsert('user_permissions', data, {});
		}
	},

	down: async (queryInterface, Sequelize) => {
		const purchasePermissions = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from permissions WHERE permission IN ('administerAllPurchases')",
		);
		if (purchasePermissions[0][0] != null) {
			const { Op } = Sequelize;
			await queryInterface.bulkDelete(
				'permissions',
				{ id: { [Op.in]: [purchasePermissions[0][0].id] } },
				{},
			);
			await queryInterface.bulkDelete(
				'user_permissions',
				{ permissionId: { [Op.in]: [purchasePermissions[0][0].id] } },
				{},
			);
		}
	},
};
