const bcrypt = require('bcryptjs');

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.bulkInsert(
			'permissions',
			[
				// Super
				{
					name: 'Administer All Permissions',
					permission: 'administerAllPermissions',
					risk: 5,
					role: 'super',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
				{
					name: 'Administer All Users',
					permission: 'administerAllUsers',
					risk: 5,
					role: 'super',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
				{
					name: 'Administer All Subscribers',
					permission: 'administerAllSubscribers',
					risk: 5,
					role: 'super',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
				{
					name: 'Is Admin',
					permission: 'isAdmin',
					risk: 5,
					role: 'super',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
			],
			{},
		);

		const superPermissions = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from permissions WHERE role = 'super'",
		);

		const adminUser = await queryInterface.sequelize.query(
			// eslint-disable-next-line quotes
			"SELECT id from users WHERE email = 'sammy.admin@amabils.co.za'",
		);

		if (adminUser[0][0] == null) {
			await queryInterface.bulkInsert(
				'users',
				[
					{
						firstName: 'Sammy',
						lastName: 'Njau',
						email: 'sammy.admin@amabils.co.za',
						password: bcrypt.hashSync('SeleX#%xy61R', 10),
						contactNumber: '+2713456789',
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				],
				{},
			);

			const adminUserRetry = await queryInterface.sequelize.query(
				// eslint-disable-next-line quotes
				"SELECT id from users WHERE email = 'sammy.admin@amabils.co.za'",
			);

			if (adminUserRetry[0][0] != null) {
				const data = superPermissions[0].map((n) => ({
					permissionId: n.id,
					userId: adminUserRetry[0][0].id,
					createdAt: new Date(),
					updatedAt: new Date(),
				}));
				await queryInterface.bulkInsert('user_permissions', data, {});
			}
		}
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete('permissions', null, {});
		await queryInterface.bulkDelete('user_permissions', null, {});
		await queryInterface.bulkDelete('users', null, {});
	},
};
