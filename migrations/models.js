const Sequelize = require('sequelize');

// module config
const environment = process.env.NODE_ENV || 'development';
const configFileName = environment === 'production' ? 'production' : 'default';
const config = require(`../config/${configFileName}.json`);

// Initialize db with model definitions
// WIP: not entirely sure about this file, since it does not seem to be actively used
// Currently this code does not correctly identify and initialise the models (which is what I was expecting)
const connectionString = config.postgres;
const sequelize = new Sequelize(connectionString, {
	dialect: 'postgres',
	dialectOptions: {
		ssl: environment === 'production',
	},
	logging: false,
	define: {
		freezeTableName: true,
	},
});
const { models } = sequelize;

// The export object must be a dictionary of model names -> models
// It must also include sequelize (instance) and Sequelize (constructor) properties

// eslint-disable-next-line prefer-object-spread
module.exports = Object.assign(
	{
		Sequelize,
		sequelize,
		syncDbWithModels
	},
	models,	
);
