module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.value) {
				await queryInterface.sequelize.query(
					'UPDATE vouchers SET "value" = "value" * 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.value) {
				await queryInterface.sequelize.query(
					'UPDATE vouchers SET "value" = "value" / 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
