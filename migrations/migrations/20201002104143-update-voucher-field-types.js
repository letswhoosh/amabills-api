module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.valueInCents) {
				await queryInterface.changeColumn(
					'vouchers',
					'valueInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.valueInCents) {
				await queryInterface.changeColumn(
					'vouchers',
					'valueInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
