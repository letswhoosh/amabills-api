module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (loyaltyPointsTable.amountInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'amountInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (loyaltyPointsTable.prevBalanceInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'prevBalanceInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (loyaltyPointsTable.currentBalanceInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'currentBalanceInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (loyaltyPointsTable.amountInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'amountInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (loyaltyPointsTable.prevBalanceInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'prevBalanceInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (loyaltyPointsTable.currentBalanceInCents) {
				await queryInterface.changeColumn(
					'loyalty_points',
					'currentBalanceInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
