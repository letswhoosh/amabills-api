module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const metersTable = await queryInterface.describeTable('meters');

			if (metersTable.serialNumber) {
				await queryInterface.changeColumn(
					'meters',
					'serialNumber',
					{
						type: Sequelize.STRING,
						allowNull: false,
						unique: {
							name: 'Duplicate error',
							msg: 'Serial Number is already in use',
						},
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const metersTable = await queryInterface.describeTable('meters');

			if (metersTable.serialNumber) {
				await queryInterface.changeColumn(
					'meters',
					'serialNumber',
					{
						type: Sequelize.STRING,
						allowNull: false,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
