module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const usersTable = await queryInterface.describeTable('users');

			if (!usersTable.vendorId) {
				await queryInterface.addColumn(
					'users',
					'vendorId',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const usersTable = await queryInterface.describeTable('purcahses');

			if (usersTable.vendorId) {
				await queryInterface.removeColumn('users', 'vendorId', {
					transaction,
				});
			}
			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
