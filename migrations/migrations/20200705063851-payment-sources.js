module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (!purchasesTable.creditCardPayment) {
				await queryInterface.addColumn(
					'purchases',
					'creditCardPayment',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (!purchasesTable.voucherPayment) {
				await queryInterface.addColumn(
					'purchases',
					'voucherPayment',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (!purchasesTable.walletBalancePayment) {
				await queryInterface.addColumn(
					'purchases',
					'walletBalancePayment',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.creditCardPayment) {
				await queryInterface.removeColumn(
					'purchases',
					'creditCardPayment',
					{
						transaction,
					},
				);
			}

			if (purchasesTable.voucherPayment) {
				await queryInterface.removeColumn(
					'purchases',
					'voucherPayment',
					{
						transaction,
					},
				);
			}

			if (purchasesTable.walletBalancePayment) {
				await queryInterface.removeColumn(
					'purchases',
					'walletBalancePayment',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
