module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const userTable = await queryInterface.describeTable('users');

			if (!userTable.verificationCode) {
				await queryInterface.addColumn(
					'users',
					'verificationCode',
					{
						type: Sequelize.STRING,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (!userTable.resetToken) {
				await queryInterface.addColumn(
					'users',
					'resetToken',
					{
						type: Sequelize.STRING,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const userTable = await queryInterface.describeTable('users');

			if (userTable.verificationCode) {
				await queryInterface.removeColumn('users', 'verificationCode', {
					transaction,
				});
			}

			if (userTable.resetToken) {
				await queryInterface.removeColumn('users', 'resetToken', {
					transaction,
				});
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
