module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.value) {
				await queryInterface.renameColumn(
					'vouchers',
					'value',
					'valueInCents',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VoucherTable = await queryInterface.describeTable('vouchers');

			if (VoucherTable.valueInCents) {
				await queryInterface.renameColumn(
					'vouchers',
					'valueInCents',
					'value',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
