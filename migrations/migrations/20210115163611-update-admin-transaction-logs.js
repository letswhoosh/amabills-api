'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const adminTransactionLogsTable = await queryInterface.describeTable(
				'admin_transaction_logs',
			);

			if (!adminTransactionLogsTable.valueInCents) {
				await queryInterface.addColumn(
					'admin_transaction_logs',
					'valueInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: false,
					},
					{ transaction },
				);
			}

			if (!adminTransactionLogsTable.purchaseType) {
				await queryInterface.addColumn(
					'admin_transaction_logs',
					'purchaseType',
					{
						type: Sequelize.STRING,
						allowNull: false,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const adminTransactionLogsTable = await queryInterface.describeTable(
				'admin_transaction_logs',
			);

			if (adminTransactionLogsTable.valueInCents) {
				await queryInterface.removeColumn(
					'admin_transaction_logs',
					'valueInCents',
					{
						transaction,
					},
				);
			}

			if (adminTransactionLogsTable.purchaseType) {
				await queryInterface.removeColumn(
					'admin_transaction_logs',
					'purchaseType',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
