module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (!purchasesTable.processingFee) {
				await queryInterface.addColumn(
					'purchases',
					'processingFee',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.margin) {
				await queryInterface.renameColumn(
					'purchases',
					'margin',
					'serviceFee',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purcahses',
			);

			if (purchasesTable.processingFee) {
				await queryInterface.removeColumn(
					'purchases',
					'processingFee',
					{
						transaction,
					},
				);
			}
			await queryInterface.renameColumn(
				'purchases',
				'serviceFee',
				'margin',
				{ transaction },
			);
			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
