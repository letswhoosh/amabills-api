module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const vendorsTable = await queryInterface.describeTable('vendors');

			if (!vendorsTable.discountRate) {
				await queryInterface.addColumn(
					'vendors',
					'discountRate',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: false,
						defaultValue: 0,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const vendorsTable = await queryInterface.describeTable('vendors');

			if (vendorsTable.discountRate) {
				await queryInterface.removeColumn('vendors', 'discountRate', {
					transaction,
				});
			}
			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
