module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.complete) {
				await queryInterface.removeColumn('purchases', 'complete', {
					transaction,
				});
			}

			if (!purchasesTable.state) {
				await queryInterface.addColumn(
					'purchases',
					'state',
					{
						type: Sequelize.STRING,
						allowNull: false,
						defaultValue: 'prevend',
					},
					{ transaction },
				);
			}

			if (!purchasesTable.voucherId) {
				await queryInterface.addColumn(
					'purchases',
					'voucherId',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.state) {
				await queryInterface.removeColumn('purchases', 'state', {
					transaction,
				});
			}

			if (purchasesTable.voucherId) {
				await queryInterface.removeColumn('purchases', 'voucherId', {
					transaction,
				});
			}

			if (!purchasesTable.complete) {
				await queryInterface.addColumn(
					'purchases',
					'complete',
					{
						type: Sequelize.BOOLEAN,
						allowNull: false,
						defaultValue: false,
					},
					{ transaction },
				);
			}
			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
