module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFee) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "processingFee" = "processingFee" * 100',
				);
			}

			if (purchasesTable.serviceFee) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "serviceFee" = "serviceFee" * 100',
				);
			}

			if (purchasesTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "amount" = "amount" * 100',
				);
			}

			if (purchasesTable.vat) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "vat" = "vat" * 100',
				);
			}

			if (purchasesTable.creditCardPayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "creditCardPayment" = "creditCardPayment" * 100',
				);
			}

			if (purchasesTable.voucherPayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "voucherPayment" = "voucherPayment" * 100',
				);
			}

			if (purchasesTable.walletBalancePayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "walletBalancePayment" = "walletBalancePayment" * 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFee) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "processingFee" = "processingFee" / 100',
				);
			}

			if (purchasesTable.serviceFee) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "serviceFee" = "serviceFee" / 100',
				);
			}

			if (purchasesTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "amount" = "amount" / 100',
				);
			}

			if (purchasesTable.vat) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "vat" = "vat" / 100',
				);
			}

			if (purchasesTable.creditCardPayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "creditCardPayment" = "creditCardPayment" / 100',
				);
			}

			if (purchasesTable.voucherPayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "voucherPayment" = "voucherPayment" / 100',
				);
			}

			if (purchasesTable.walletBalancePayment) {
				await queryInterface.sequelize.query(
					'UPDATE purchases SET "walletBalancePayment" = "walletBalancePayment" / 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
