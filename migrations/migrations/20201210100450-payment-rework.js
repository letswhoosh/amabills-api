module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const paymentsTable = await queryInterface.describeTable(
				'payments',
			);

			if (!paymentsTable.successAction) {
				await queryInterface.addColumn(
					'payments',
					'successAction',
					{
						type: Sequelize.JSON,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (!paymentsTable.successEntity) {
				await queryInterface.addColumn(
					'payments',
					'successEntity',
					{
						type: Sequelize.JSON,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const paymentsTable = await queryInterface.describeTable(
				'payments',
			);

			if (paymentsTable.successAction) {
				await queryInterface.removeColumn('payments', 'successAction', {
					transaction,
				});
			}

			if (paymentsTable.successEntity) {
				await queryInterface.removeColumn('payments', 'successEntity', {
					transaction,
				});
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
