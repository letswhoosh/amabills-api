module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (loyaltyPointsTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "amount" = "amount" * 100',
				);
			}

			if (loyaltyPointsTable.prevBalance) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "prevBalance" = "prevBalance" * 100',
				);
			}

			if (loyaltyPointsTable.currentBalance) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "currentBalance" = "currentBalance" * 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (loyaltyPointsTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "amount" = "amount" / 100',
				);
			}

			if (loyaltyPointsTable.prevBalance) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "prevBalance" = "prevBalance" / 100',
				);
			}

			if (loyaltyPointsTable.currentBalance) {
				await queryInterface.sequelize.query(
					'UPDATE loyalty_points SET "currentBalance" = "currentBalance" / 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
