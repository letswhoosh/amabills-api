module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (
				VendorTransactionTable.amount &&
				!VendorTransactionTable.amountInCents
			) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'amount',
					'amountInCents',
					{ transaction },
				);
			}

			if (
				VendorTransactionTable.prevBalance &&
				!VendorTransactionTable.prevBalanceInCents
			) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'prevBalance',
					'prevBalanceInCents',
					{ transaction },
				);
			}

			if (
				VendorTransactionTable.currentBalance &&
				!VendorTransactionTable.currentBalanceInCents
			) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'currentBalance',
					'currentBalanceInCents',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (VendorTransactionTable.amountInCents) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'amountInCents',
					'amount',
					{ transaction },
				);
			}

			if (VendorTransactionTable.prevBalanceInCents) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'prevBalanceInCents',
					'prevBalance',
					{ transaction },
				);
			}

			if (VendorTransactionTable.currentBalanceInCents) {
				await queryInterface.renameColumn(
					'vendor_transactions',
					'currentBalanceInCents',
					'currentBalance',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
