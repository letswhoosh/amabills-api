'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchaseTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchaseTable.meterNumber) {
				await queryInterface.changeColumn(
					'purchases',
					'meterNumber',
					{
						type: Sequelize.STRING,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchaseTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchaseTable.meterNumber) {
				await queryInterface.changeColumn(
					'purchases',
					'meterNumber',
					{
						type: Sequelize.STRING,
						allowNull: false,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
