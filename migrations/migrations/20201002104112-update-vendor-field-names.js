module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTable = await queryInterface.describeTable('vendors');

			if (
				VendorTable.discountRate &&
				!VendorTable.discountRateInPercent
			) {
				await queryInterface.renameColumn(
					'vendors',
					'discountRate',
					'discountRateInPercent',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTable = await queryInterface.describeTable('vendors');

			if (VendorTable.discountRateInPercent) {
				await queryInterface.renameColumn(
					'vendors',
					'discountRateInPercent',
					'discountRate',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
