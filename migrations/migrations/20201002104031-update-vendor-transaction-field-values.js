module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (VendorTransactionTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "amount" = "amount" * 100',
				);
			}

			if (VendorTransactionTable.prevBalance) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "prevBalance" = "prevBalance" * 100',
				);
			}

			if (VendorTransactionTable.currentBalance) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "currentBalance" = "currentBalance" * 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (VendorTransactionTable.amount) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "amount" = "amount" / 100',
				);
			}

			if (VendorTransactionTable.prevBalance) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "prevBalance" = "prevBalance" / 100',
				);
			}

			if (VendorTransactionTable.currentBalance) {
				await queryInterface.sequelize.query(
					'UPDATE vendor_transactions SET "currentBalance" = "currentBalance" / 100',
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
