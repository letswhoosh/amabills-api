module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (!purchasesTable.paid) {
				await queryInterface.addColumn(
					'purchases',
					'paid',
					{
						type: Sequelize.BOOLEAN,
						allowNull: false,
						defaultValue: false,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.paid) {
				await queryInterface.removeColumn('purchases', 'paid', {
					transaction,
				});
			}
			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
