module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (VendorTransactionTable.amountInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'amountInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (VendorTransactionTable.prevBalanceInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'prevBalanceInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (VendorTransactionTable.currentBalanceInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'currentBalanceInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const VendorTransactionTable = await queryInterface.describeTable(
				'vendor_transactions',
			);

			if (VendorTransactionTable.amountInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'amountInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (VendorTransactionTable.prevBalanceInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'prevBalanceInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (VendorTransactionTable.currentBalanceInCents) {
				await queryInterface.changeColumn(
					'vendor_transactions',
					'currentBalanceInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
