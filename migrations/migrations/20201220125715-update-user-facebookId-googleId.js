'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const userTable = await queryInterface.describeTable('users');

			if (!userTable.facebookId) {
				await queryInterface.addColumn(
					'users',
					'facebookId',
					{
						type: Sequelize.STRING,
						allowNull: true,
						unique: {
							name: 'Duplicate error',
							msg: 'facebookId should be unique',
						},
					},
					{ transaction },
				);
			}

			if (!userTable.googleId) {
				await queryInterface.addColumn(
					'users',
					'googleId',
					{
						type: Sequelize.STRING,
						allowNull: true,
						unique: {
							name: 'Duplicate error',
							msg: 'googleId should be unique',
						},
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const userTable = await queryInterface.describeTable('users');

			if (userTable.facebookId) {
				await queryInterface.removeColumn('users', 'facebookId', {
					transaction,
				});
			}

			if (userTable.googleId) {
				await queryInterface.removeColumn('users', 'googleId', {
					transaction,
				});
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
