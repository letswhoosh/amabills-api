module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFeeInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'processingFeeInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.serviceFeeInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'serviceFeeInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.amountInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'amountInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.vatInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'vatInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.creditCardPaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'creditCardPaymentInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.voucherPaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'voucherPaymentInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.walletBalancePaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'walletBalancePaymentInCents',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFeeInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'processingFeeInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.serviceFeeInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'serviceFeeInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.amountInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'amountInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.vatInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'vatInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.creditCardPaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'creditCardPaymentInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.voucherPaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'voucherPaymentInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			if (purchasesTable.walletBalancePaymentInCents) {
				await queryInterface.changeColumn(
					'purchases',
					'walletBalancePaymentInCents',
					{
						type: Sequelize.DECIMAL(10, 2),
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
