module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (
				loyaltyPointsTable.amount &&
				!loyaltyPointsTable.amountInCents
			) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'amount',
					'amountInCents',
					{ transaction },
				);
			}

			if (
				loyaltyPointsTable.prevBalance &&
				!loyaltyPointsTable.prevBalanceInCents
			) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'prevBalance',
					'prevBalanceInCents',
					{ transaction },
				);
			}

			if (
				loyaltyPointsTable.currentBalance &&
				!loyaltyPointsTable.currentBalanceInCents
			) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'currentBalance',
					'currentBalanceInCents',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const loyaltyPointsTable = await queryInterface.describeTable(
				'loyalty_points',
			);

			if (loyaltyPointsTable.amountInCents) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'amountInCents',
					'amount',
					{ transaction },
				);
			}

			if (loyaltyPointsTable.prevBalanceInCents) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'prevBalanceInCents',
					'prevBalance',
					{ transaction },
				);
			}

			if (loyaltyPointsTable.currentBalanceInCents) {
				await queryInterface.renameColumn(
					'loyalty_points',
					'currentBalanceInCents',
					'currentBalance',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
