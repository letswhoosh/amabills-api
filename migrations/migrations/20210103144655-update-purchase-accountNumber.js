'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchaseTable = await queryInterface.describeTable(
				'purchases',
			);

			if (!purchaseTable.accountNumber) {
				await queryInterface.addColumn(
					'purchases',
					'accountNumber',
					{
						type: Sequelize.STRING,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchaseTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchaseTable.accountNumber) {
				await queryInterface.removeColumn(
					'purchases',
					'accountNumber',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
