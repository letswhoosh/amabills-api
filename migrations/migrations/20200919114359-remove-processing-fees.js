module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFee) {
				await queryInterface.removeColumn(
					'purchases',
					'processingFee',
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (!purchasesTable.v) {
				await queryInterface.addColumn(
					'purchases',
					'processingFee',
					{
						type: Sequelize.INTEGER,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
