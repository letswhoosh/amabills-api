'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const adminTransactionLogsTable = await queryInterface.describeTable(
				'admin_transaction_logs',
			);

			if (adminTransactionLogsTable.purchaseType) {
				await queryInterface.changeColumn(
					'admin_transaction_logs',
					'purchaseType',
					{
						type: Sequelize.STRING,
						allowNull: false,
						validate: {
							isIn: {
								args: ['bills', 'prepaid'],
								msg: `Type of purchase is not valid`,
							},
						},
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const adminTransactionLogsTable = await queryInterface.describeTable(
				'admin_transaction_logs',
			);

			if (adminTransactionLogsTable.purchaseType) {
				await queryInterface.changeColumn(
					'admin_transaction_logs',
					'purchaseType',
					{
						type: Sequelize.STRING,
						allowNull: false,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
