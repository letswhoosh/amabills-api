module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.c && !purchasesTable.processingFeeInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'processingFee',
					'processingFeeInCents',
					{ transaction },
				);
			}

			if (
				purchasesTable.serviceFee &&
				!purchasesTable.serviceFeeInCents
			) {
				await queryInterface.renameColumn(
					'purchases',
					'serviceFee',
					'serviceFeeInCents',
					{ transaction },
				);
			}

			if (purchasesTable.amount && !purchasesTable.amountInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'amount',
					'amountInCents',
					{ transaction },
				);
			}

			if (purchasesTable.vat && !purchasesTable.vatInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'vat',
					'vatInCents',
					{
						transaction,
					},
				);
			}

			if (
				purchasesTable.creditCardPayment &&
				!purchasesTable.creditCardPaymentInCents
			) {
				await queryInterface.renameColumn(
					'purchases',
					'creditCardPayment',
					'creditCardPaymentInCents',
					{
						transaction,
					},
				);
			}

			if (
				purchasesTable.voucherPayment &&
				!purchasesTable.voucherPaymentInCents
			) {
				await queryInterface.renameColumn(
					'purchases',
					'voucherPayment',
					'voucherPaymentInCents',
					{
						transaction,
					},
				);
			}

			if (
				purchasesTable.walletBalancePayment &&
				!purchasesTable.walletBalancePaymentInCents
			) {
				await queryInterface.renameColumn(
					'purchases',
					'walletBalancePayment',
					'walletBalancePaymentInCents',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const purchasesTable = await queryInterface.describeTable(
				'purchases',
			);

			if (purchasesTable.processingFeeInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'processingFeeInCents',
					'processingFee',
					{ transaction },
				);
			}

			if (purchasesTable.serviceFeeInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'serviceFeeInCents',
					'serviceFee',
					{ transaction },
				);
			}

			if (purchasesTable.amountInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'amountInCents',
					'amount',
					{ transaction },
				);
			}

			if (purchasesTable.vatInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'vatInCents',
					'vat',
					{
						transaction,
					},
				);
			}

			if (purchasesTable.creditCardPaymentInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'creditCardPaymentInCents',
					'creditCardPayment',
					{
						transaction,
					},
				);
			}

			if (purchasesTable.voucherPaymentInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'voucherPaymentInCents',
					'voucherPayment',
					{
						transaction,
					},
				);
			}

			if (purchasesTable.walletBalancePaymentInCents) {
				await queryInterface.renameColumn(
					'purchases',
					'walletBalancePaymentInCents',
					'walletBalancePayment',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
