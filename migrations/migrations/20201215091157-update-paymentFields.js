'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const paymentsTable = await queryInterface.describeTable(
				'payments',
			);

			if (!paymentsTable.paymentComposition) {
				await queryInterface.addColumn(
					'payments',
					'paymentComposition',
					{
						type: Sequelize.JSON,
						allowNull: true,
					},
					{ transaction },
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},

	down: async (queryInterface, Sequelize) => {
		const transaction = await queryInterface.sequelize.transaction();
		try {
			const paymentsTable = await queryInterface.describeTable(
				'payments',
			);

			if (paymentsTable.paymentComposition) {
				await queryInterface.removeColumn(
					'payments',
					'paymentComposition',
					{
						transaction,
					},
				);
			}

			await transaction.commit();
		} catch (err) {
			await transaction.rollback();
			throw err;
		}
	},
};
