const environment = process.env.NODE_ENV || 'development';
const configFileName = environment === 'development' ? 'default' : environment;
const config = require(`../config/${configFileName}.json`);

const ssl = ['production', 'staging'].includes(environment || 'default');

const dialect = 'postgres';
const dialectOptions = ssl
	? {
			ssl: {
				rejectUnauthorized: false,
			},
	  }
	: null;

module.exports = {
	[environment]: {
		dialect,
		url: process.env.DATABASE_URL || config.postgres,
		migrationStorageTableName: '_migrations',
		seederStorage: 'sequelize',
		seederStorageTableName: '_seeds',
		ssl,
		dialectOptions,
	},
};
