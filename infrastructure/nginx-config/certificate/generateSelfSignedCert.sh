#!/bin/bash

openssl genrsa -aes128 -out server.key 2048
openssl rsa -in server.key -out server.key
openssl req -new -days 365 -key server.key -out server.csr

# self signed cert valid for 365days
openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 365
