# Docker & Docker Compose pointers

## getting started with docker

Refer to the docker docs to get started with using (docker)[https://docs.docker.com/get-started/]

(Docker compose)[https://docs.docker.com/compose/] is a helpful way of packaging and automating the deployment of docker containers. Please refer to the docker-compose documentation for more info on how it works.

## To start with

Before you start testing and using these scripts, you'll need an image built of the UI app. Be default these scripts are expecting to find an image either locally or hosted on docker hub.

For this sample app, the image has not been uploaded to docker hub so you'll need to compile the image locally in order to test everything.

### Compile UI app image

To start with, clone the (sample app repo)[https://bitbucket.org/symbionix/symbionix-sample-app/src/master/] using `git clone <Http/ssh repo_url>`

Then navigate into this new folder and compile the docker image with this command:

```
docker build -t sample-api .
```

The API will automatically be compiled by the docker-compose script, so no need to manually compile the image for the sample api.

### Using the docker compose scripts

Now we can start using the docker-compose script.

For a more ecomprehensive intro to available docker compose commands please refer to the official documentation. But the core basics you'll need include:

To spin up the dependencies required to test the api & view the logs in real time use:

```
docker-compose up
```

Note: with this first approach, once you exit from the terminal the docker containers will shutdown.

To spin up the environment in background mode (i.e. the command will execute and end while leaving the system active in the background) use:

```
docker-compose up -d
```

To rebuild the API docker image before spinning up the environment use:

```
docker-compose up -d --build
```

To spin up a full setup of the sample-api, sample-app, postgres and nginx to simulate current production configuration, use:

```
docker-compose -f docker-compose.full.yml -d up
```
