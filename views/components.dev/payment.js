const { redirectUrl, apiPath, userId } = window;

const updatePaymentStatus = ({ step, status }) =>
	fetch(
		`${apiPath}/payments/send-status-update?step=${step}&status=${status}&userId=${userId}`,
	);

const PaymentForm = () => {
	const [ready, setReady] = React.useState(false);
	const form = React.createRef();

	const updateStatus = React.useCallback(() => {
		if (window.shopReady !== true) {
			updatePaymentStatus({ step: 7, status: 'Loading form...', userId });
			// eslint-disable-next-line no-console
			console.log('Form Loaded...');
			window.shopReady = true;
		}
	}, [ready]);

	React.useEffect(() => {
		console.log('Loading Form');
		updatePaymentStatus({ step: 6, status: 'Retrieving form...', userId });
		if (form.current != null && window.shopReady !== true) {
			form.current.addEventListener(
				'DOMNodeInserted',
				updateStatus,
				false,
			);
			setReady(true);
		}
		return () => {
			setReady(false);
			form.current.removeEventListener('DOMNodeInserted', updateStatus);
		};
	}, []);

	return (
		<main className="container">
			<div className="content">
				{ready === false ? (
					<div>
						<div className="text-center">
							<div className="loading loading-lg" />
							<p>Setting up payment form...</p>
						</div>
					</div>
				) : null}

				<div ref={form}>
					{ready === true ? (
						<div>
							<div
								class="container"
								style={{ width: '40em', maxWidth: '90vw' }}
							>
								<div class="columns">
									<div
										class="column col-6"
										style={{
											display: 'flex',
											alignItems: 'center',
										}}
									>
										<img
											style={{
												width: '40px',
												height: '40px',
											}}
											alt="secure"
											src="https://res.cloudinary.com/symbionix/image/upload/v1580993412/securepay.png"
										/>
									</div>
									<div
										class="column col-6"
										style={{ textAlign: 'end' }}
									>
										<img
											style={{
												width: 'auto',
												height: '55px',
											}}
											alt="Whoosh Payments"
											src="https://res.cloudinary.com/symbionix/image/upload/v1586651035/whoosh.png"
										/>
									</div>
								</div>
							</div>
							<div style={{ marginTop: '18px' }}>
								<form
									action={redirectUrl}
									className="paymentWidgets"
									data-brands="VISA MASTER AMEX"
								/>
							</div>
						</div>
					) : null}
				</div>
			</div>
		</main>
	);
};

const domContainer = document.querySelector('#payment-form');
ReactDOM.render(<PaymentForm />, domContainer);
