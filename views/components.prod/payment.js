var _slicedToArray = (function () {
	function sliceIterator(arr, i) {
		var _arr = [];
		var _n = true;
		var _d = false;
		var _e = undefined;
		try {
			for (
				var _i = arr[Symbol.iterator](), _s;
				!(_n = (_s = _i.next()).done);
				_n = true
			) {
				_arr.push(_s.value);
				if (i && _arr.length === i) break;
			}
		} catch (err) {
			_d = true;
			_e = err;
		} finally {
			try {
				if (!_n && _i['return']) _i['return']();
			} finally {
				if (_d) throw _e;
			}
		}
		return _arr;
	}
	return function (arr, i) {
		if (Array.isArray(arr)) {
			return arr;
		} else if (Symbol.iterator in Object(arr)) {
			return sliceIterator(arr, i);
		} else {
			throw new TypeError(
				'Invalid attempt to destructure non-iterable instance',
			);
		}
	};
})();

var _window = window,
	redirectUrl = _window.redirectUrl,
	apiPath = _window.apiPath,
	userId = _window.userId;

var updatePaymentStatus = function updatePaymentStatus(_ref) {
	var step = _ref.step,
		status = _ref.status;
	return fetch(
		apiPath +
			'/payments/send-status-update?step=' +
			step +
			'&status=' +
			status +
			'&userId=' +
			userId,
	);
};

var PaymentForm = function PaymentForm() {
	var _React$useState = React.useState(false),
		_React$useState2 = _slicedToArray(_React$useState, 2),
		ready = _React$useState2[0],
		setReady = _React$useState2[1];

	var form = React.createRef();

	var updateStatus = React.useCallback(
		function () {
			if (window.shopReady !== true) {
				updatePaymentStatus({
					step: 7,
					status: 'Loading form...',
					userId: userId,
				});
				// eslint-disable-next-line no-console
				console.log('Form Loaded...');
				window.shopReady = true;
			}
		},
		[ready],
	);

	React.useEffect(function () {
		console.log('Loading Form');
		updatePaymentStatus({
			step: 6,
			status: 'Retrieving form...',
			userId: userId,
		});
		if (form.current != null && window.shopReady !== true) {
			form.current.addEventListener(
				'DOMNodeInserted',
				updateStatus,
				false,
			);
			setReady(true);
		}
		return function () {
			setReady(false);
			form.current.removeEventListener('DOMNodeInserted', updateStatus);
		};
	}, []);

	return React.createElement(
		'main',
		{ className: 'container' },
		React.createElement(
			'div',
			{ className: 'content' },
			ready === false
				? React.createElement(
						'div',
						null,
						React.createElement(
							'div',
							{ className: 'text-center' },
							React.createElement('div', {
								className: 'loading loading-lg',
							}),
							React.createElement(
								'p',
								null,
								'Setting up payment form...',
							),
						),
				  )
				: null,
			React.createElement(
				'div',
				{ ref: form },
				ready === true
					? React.createElement(
							'div',
							null,
							React.createElement(
								'div',
								{
									class: 'container',
									style: { width: '40em', maxWidth: '90vw' },
								},
								React.createElement(
									'div',
									{ class: 'columns' },
									React.createElement(
										'div',
										{
											class: 'column col-6',
											style: {
												display: 'flex',
												alignItems: 'center',
											},
										},
										React.createElement('img', {
											style: {
												width: '40px',
												height: '40px',
											},
											alt: 'secure',
											src:
												'https://res.cloudinary.com/symbionix/image/upload/v1580993412/securepay.png',
										}),
									),
									React.createElement(
										'div',
										{
											class: 'column col-6',
											style: { textAlign: 'end' },
										},
										React.createElement('img', {
											style: {
												width: 'auto',
												height: '55px',
											},
											alt: 'Whoosh Payments',
											src:
												'https://res.cloudinary.com/symbionix/image/upload/v1586651035/whoosh.png',
										}),
									),
								),
							),
							React.createElement(
								'div',
								{ style: { marginTop: '18px' } },
								React.createElement('form', {
									action: redirectUrl,
									className: 'paymentWidgets',
									'data-brands': 'VISA MASTER AMEX',
								}),
							),
					  )
					: null,
			),
		),
	);
};

var domContainer = document.querySelector('#payment-form');
ReactDOM.render(React.createElement(PaymentForm, null), domContainer);
