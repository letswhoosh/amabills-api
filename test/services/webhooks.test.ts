import app from '../../src/app';

describe('\'webhooks\' service', () => {
  it('registered the service', () => {
    const service = app.service('webhooks');
    expect(service).toBeTruthy();
  });
});
